\documentclass[a4paper,12pt]{article}

\usepackage{amsmath, amssymb, bm}
\usepackage{cite}

\title{Extensions to stochastic gradient evidence approximations}
\date{\today}

\def\Z{\mathcal{Z}}
\def\bmtheta{\bm{\theta}}
\def\data{\mathcal{D}}

\begin{document}
\maketitle

\section{Annealing}

Annealed importance sampling~\cite{ais} samples from a sequence of thermal
annealed distributions $\propto f_t(\bmtheta) := L(\bmtheta)^{\beta_t}
p(\bmtheta)$. The thermal parameter is monotonically increasing with $0 =
\beta_1 < \ldots < \beta_T = 1$, \cite{sandwich} recommends using a
sigmoid scheduling scheme
%
\begin{equation*}
  \beta_t = \sigma(a(\frac{2t}{T} - 1)),
\end{equation*}
%
for some user parameter $a$. This helps improve stability over a linear
scheduling scheme.

The particles are updated from each intermediate distribution to the next
through some MCMC operator, and the importance weights are updated as follows
%
\begin{equation*}
  w \gets w \frac{f_t(\bmtheta_{t-1})}{f_{t-1}(\bmtheta_{t-1})}
  = w L(\bmtheta_{t-1})^{\beta_t - \beta_{t-1}}
\end{equation*}
%
At the end of the chain, $w$ is an unbiased estimator of $\int L(\bmtheta)
p(\bmtheta) \, d\bmtheta$. Since the importance weights do not involve the
functional form of $p(\bmtheta)$, we can easily use this scheme to estimate
predictive distributions by letting $p(\bmtheta)$ be the posterior and using
SGHMC as the transition operator.

Instead of using a preset annealing schedule as above, it is possible to use
adaptive annealing based on effective sample size. This may not be necessary
during Bayesian updating once a large number of observations have already been
seen, but may provide better stability when estimating $p(y_n|y_{<n})$ when $n$
is small.

\section{Sequential Nested Sampling}

NS would work equally well to estimate predictive probabilities just by sampling
from the posterior using SGHMC and climbing up $p(y_n|\bmtheta)$. This can be
done with larger batch/chunk sizes due to the robustness of NS. The number of
steps required scales with the information in the data set. Information gain is
not additive over the data; i.e. the information in two data points is not
necessarily the equal to the information in the first data point plus the
information in the second data point when conditioned on the first. Because of
this, sequential nested sampling could have to perform more steps or fewer steps
than regular nested sampling. However the sequential version would not need to
evaluate the full likelihood function, so each step will be roughly constant in
the data set size (linear in the chunk size), while in vanilla nested sampling
each step is linear in the data set size.

\section{Latent Variable Models}

Latent variable models (LVMs) are models which associate an unobservable
variable $z_n$ to each data point $y_n$. There may be some temporal structure in
the latent variables such as in hidden Markov models. The data points are
usually assumed to be i.i.d. when conditioned on the latent variables and the
model parameters. If we assume the latent variables are Markov then the joint
distribution is
%
\begin{equation*}
  p(\bmtheta,\{z_n\}_n,\{y_n\}_n) =
  p(\bmtheta)\prod_{n}p(z_n|z_{n-1},\bmtheta)p(y_n|z_n,\bmtheta)
\end{equation*}
%
Often the latent variables are assumed to be i.i.d. when conditioned on the
model parameters $\bmtheta$, eg: latent Dirichlet allocation and variational
autoencoders. The models treated in \cite{sandwich} are all of this form. If
this is the case the joint distribution simplifies to 
%
\begin{equation*}
  p(\bmtheta,\{z_n\}_n,\{y_n\}_n) =
  p(\bmtheta)\prod_{n}p(z_n|\bmtheta)p(y_n|z_n,\bmtheta).
\end{equation*}
%
Sequential Monte Carlo methods which tackle inference in these models typically
have to provide a proposal distribution $q(z_n|y_{n}, \bmtheta)$ from which to sample
the next latent variable during Bayesian updating. Specifying a poor proposal
distribution can cause the estimates to have very high variance.

In LVMs, the term ``marginal likelihood'' may refer to the Bayesian evidence,
which marginalizes out the latent variables and the model parameters
%
\begin{equation*}
  \Z = \idotsint p(\bmtheta,\{z_n\}_n,\{y_n\}_n)\, dz_1\cdots dz_Nd\bmtheta,
\end{equation*}
%
which simplifies to 
%
\begin{equation*}
  \Z = \int p(\bmtheta) \left(\prod_n\int
  p(z_n|\bmtheta)p(y_n|z,\bmtheta)\,dz\right)\,d\bmtheta
\end{equation*}
%
in the case of conditionally independent latent variables.
Marginal likelihood may also refer to the likelihood of the parameters once the
latent variables have been marginalized out:
%
\begin{equation*}
  p(\{y_n\}_n|\bmtheta) = \prod_n\int
  p(z_n|\bmtheta)p(y_n|z,\bmtheta)\,dz
\end{equation*}
%
In this case scaling to large data sets is trivial and does not require any
stochastic gradient approximations because of the independence of the latent
variables when conditioned on the model parameters.

\section{Nested Sampling for Validation Probabilities}

It is common in machine learning to split up the data set into a training set
and a test set. The model parameters are inferred using the training set and the
test set is used as a measure of the models performance. For this it is
desirable to estimate the conditional evidence of the test set given the
training set
%
\begin{equation*}
  p(\data_{\mbox{test}}|\data_{\mbox{train}}) = \int
  p(\data_{\mbox{test}}|\bmtheta)p(\bmtheta|\data_{\mbox{train}})\,d\bmtheta
\end{equation*}
%
Nested sampling could be used for this purpose, but would require efficient
sampling from the constrained posterior distribution. Using our implementation
of constrained SGHMC, this should be possible. Furthermore, as a by product of
running NS this way, one would obtain importance weights for the posterior
samples which incorporate the information in the test set. This means that, once
the model has been tested on the test set, the deployed model would have also
been trained on the test set indirectly through the importance weights.

\section{Squeezing With Harmonic Nested Sampling}

In \cite{sandwich} the authors note that nested
sampling over-estimates the log-ML too often to be considered a stochastic
lower-bound. This seems likely due to the fact that when MCMC operators are used
to produce samples from the constrained prior, such as by reflecting off of
constraint boundaries, they tend to under-sample near the boundaries. If this
can be mitigated then one could use NS to obtain a stochastic upper-bound by
estimating
%
\begin{equation*}
  \frac{1}{\Z} = E_{p(\bmtheta|\data)}\left[\frac{1}{L(\bmtheta)}\right]
\end{equation*}
%
By sampling under the constrained posterior using SGHMC (starting from a sample
generated by a forward NS run) under increasing $1/L(\bmtheta)$.

This method will not be very scaleable but should provide a tight stochastic
upper-bound. Furthermore, using NS should mitigate all the problems that the
standard HME has, such as infinite variance.

If the stochastic upper-bound/lower-bound guarantees cannot be met, then this is
probably not worth exploring.

\bibliographystyle{plain}
\bibliography{ref}
\end{document}
