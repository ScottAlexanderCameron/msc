\section{Notation and Conventions}
\label{sec:notation}

Throughout this thesis, we will denote parameters by $\theta$, observations by
$y$ or similar, a data set by $\data = \{y_n\}_{n=1}^N$ and most probabilities by
$p$.
%
We sometimes write $x$ for a generic variable without attaching the
specific meaning of a parameter or observation.
%
We will not differentiate between probabilities or probability densities but
rather by their arguments, except when this convention may cause ambiguity.

We mainly consider parametric models with conditionally independent
data. In these models the joint probability distribution factorizes into the
prior times a product of individual-data likelihoods,
%
\begin{equation}
  p(\data,\theta) = p(\theta) \prod_{n} p(y_n|\theta).
\end{equation}
%
This restriction ensures that estimates of the log-likelihood using mini-batches
sampled i.i.d.\ from the data set are unbiased, and are approximately normally
distributed due to the central limit theorem.
%
The restriction to conditionally independent data can be weakened to
conditionally Markov data or autoregressive models since a log-likelihood of the
form
%
\begin{equation}
  \log{p(\data|\theta)} = \sum_n \log{p}(y_n|y_{n-1},\cdots,y_{n-k}, \theta)
\end{equation}
%
would also yield to a central limit theorem for fixed $k$.
%
This work is therefore applicable to many models in machine learning, with the
notable exception of Gaussian processes.
%
The reason for this is that the probability of a data set under a
non-degenerate Gaussian process likelihood cannot be written in the above form
for any fixed value $k$.

We will further assume that the parameters $\theta$ are continuous variables and
that the prior probability density function and the likelihood are continuous
and differentiable.
%
This allows us to use algorithms such as Hamiltonian Monte Carlo and stochastic
gradient Hamiltonian Monte Carlo.
%
Discrete parameters or hyper-parameters can be used in certain cases, in which
case Gibbs sampling could be used to update them between the continuous
parameter updates.
%
In order for the mini-batching to still be beneficial, it would be required that
the Gibbs update for the discrete parameters does not depend on the entire data
set.

We do not use boldface for vectors as is the common convention, because
many equations, such as those in Section~\ref{sec:hamiltonian}, apply to more
general structures such as Riemannian manifolds.
%
Familiarity with differential equations and vector calculus is assumed, and we
make use of differential operators in matrix equations. For example, given a
matrix valued function $A(x)$ and scalar function $f(x)$, we could denote a
scalar function $g(x) = \nabla^T A(x) \nabla f(x)$, which would be interpreted
as
%
\begin{equation}
  g(x) = \sum_{j,k} \frac{\partial}{\partial x_j} A_{j,k}(x)
  \frac{\partial}{\partial x_k} f(x).
\end{equation}
%
When there are multiple vector-valued variables involved, such as $x$ and $v$
then we may write $\nabla_x$ and $\nabla_v$ to mean the gradient with respect to
$x$ and gradient with respect to $v$ respectively.
%
$\nabla_{x,v}$ would then mean gradient with respect to the vector $r = (x,
v)^T$, the direct sum of $x$ and $v$.
%
For a vector-valued function, the divergence $\nabla {\cdot} f(x)$ is the inner
product of the differential operator and the vector function; in matrix notation
it can equivalently be written $\nabla^T f(x)$.
%
The Laplacian operator is defined to be $\nabla^2 := \nabla{\cdot}\nabla$.
