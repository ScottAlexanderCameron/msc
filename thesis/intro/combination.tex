\subsection{Model Combination and Selection}

Calculation of the evidence $p(\data|\M)$ is crucial both in
experimental science and in machine learning, but plays a different
role in each.
% 
In science, we often have some physically interpretable parameters in our
models.
%
These parameters could be the mass of a proton, the temperature of the
sun, or the structure of molecules in cell walls.  %
%
In these scenarios, we often want to find one model that best describes the
observed phenomena.
%
In these cases the evidence of a model is sometimes used as a selection
criterion \citetext{\citealp[chapter~17]{vdlinden};
\citealp[chapter~12]{bayesian-reasoning}};
%
If one model has a significantly higher evidence than another, it is clearly a
much better description of our observations.
%
One nice feature of using evidence in this way, is that it automatically
incorporates Occam's razor; models which are overly complex will have a lower
evidence than those that are simpler but still sufficient to describe the data
\citep[chapter~3]{vdlinden}.  

In a machine learning context, on the other hand, the goal is not always to find
the model with an optimal parameterization for the physical quantities. 
%
Rather, machine learning often seeks the best possible predictions for future
observations $y'$, given present data $\data$.
%
Given multiple models, we may therefore combine the predictions through a
weighted sum of posterior predictive distributions over the models
%
\begin{equation}
  \label{mip}
  p(y'|\data) = \sum_k p(y'|\data,\M_k)\, p(\M_k|\data).
\end{equation}
% 
The combination of models itself follows from elementary rules of probability
theory. 
%
It weights the prediction $p(y'|\data,\M_k)$ of each model by its ability, as
quantified by the respective model posterior $p(\M_k|\data)$, to describe the
data already available.


Each model's individual posterior predictive $p(y'|\data,\M_k)$ can in turn be
written in terms of that model's posterior $p(\theta_k|\data,\M_k)$ and the
appropriate conditional distribution $p(y'|\theta_k,\data,\M_k)$ for $y'$,
%
\begin{equation}
  \label{ppd}
  p(y'|\data,\M_k)
  = \int p(y'|\theta_k,\data,\M_k)\, p(\theta_k|\data,\M_k) \, d\theta_k.
\end{equation}
%
If the parameters are discrete, the integral is replaced by a sum.


The model posterior $p(\M_k|\data)$ in Equation~(\ref{mip}) can be written in terms
of the evidence $p(\data|\M_k)$ by means of Bayes' rule,
%
\begin{equation}
  p(\M_k|\data)
  = \frac{p(\data|\M_k)\,p(\M_k)}{\sum_j p(\data|\M_j)\,p(\M_j)}.
\end{equation}
%
Given equal and constant model priors, the combined model predictions can be
written in terms of the individual predictions and model evidences
$\Z_k:=p(\data|\M_k)$.
%
\begin{equation}
  p(y'|\data) =
  \frac{\sum_k p(y'|\data,\M_k)\, \Z_k}{\sum_k \Z_k}.
\end{equation}
