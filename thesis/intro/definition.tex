\section{Bayesian Evidence}

\subsection{Definition and Interpretation}

Bayesian modeling presents the task of answering questions about data
as posterior inference.
%
Given observations $\data$ and a model $\M$ with parameters
$\theta$, the posterior distribution is given by Bayes' rule
%
\begin{equation}
  \underbrace{p(\theta|\data,\M)}_{\mbox{\tiny posterior}} = 
  \frac{
    \overbrace{p(\data|\theta,\M)}^{\mbox{\tiny likelihood}}
    \overbrace{p(\theta|\M)}^{\mbox{\tiny prior}}
    }{
    \underbrace{p(\data|\M)}_{\mbox{\tiny evidence}}
  }.
\end{equation}
%
The evidence (or marginal likelihood) $\Z := p(\data|\M)$ is the probability (or
density) of the observations under the model assumptions.
%
We adopt the convention that probabilities/densities are represented by the
letter $p$, and distinguish between each probability distribution by the symbol
in the argument.
%
We do not further distinguish between probabilities and probability densities
unless the context is ambiguous.

Many questions that need to be answered within the assumptions of the model
usually only require the posterior distribution up to a normalizing constant,
%
\begin{equation}
  p(\theta|\data,\M) \propto p(\data|\theta,\M) p(\theta|\M),
\end{equation}
%
and so the evidence is often neglected.
%
However, if the model is unable to adequately describe the data, the posterior
distribution may give ridiculous predictions since the model assumptions are
taken to be ground truth; they appear only on the right hand side of the
conditional.
%
We can allow the model assumptions to be probabilistic in nature by specifying a
prior probability distribution over a family of models
$\{\M_k\}_{k\in\mathcal{I}}$.
%
The posterior distribution over this family
%
\begin{equation}
  p(\M_k|\data) \propto 
  \underbrace{p(\data|\M_k)}_{\mbox{\tiny model evidence}}
  p(\M_k),
\end{equation}
%
is proportional to the model evidences and so 
%
for this reason, a model's evidence can be seen as a quantitative measure of how
well that model describes the data set: the larger the evidence, the more likely
it is that that model was the one which generated the data.

In practice, it is common to propose only a finite number of models, and a
uniform prior is often assumed. In this case the posterior distribution over
models is given by a constant multiplied by the model evidences.
%
\begin{equation}
  p(\M_k|\data) \propto p(\data|\M_k).
\end{equation}
%
By specifying a finite set of models, we are able to account for some model
uncertainty; however our particular choice of models still encodes certain
assumptions about the relationships between observations which may not be
correct.

The specification of at least two competing models is essential to the iterative
and perpetually provisional knowledge framework of science. 
%
No model is ever final, and no model is ever ``absolutely correct''.

