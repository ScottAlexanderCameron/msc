\section{Online Estimation and Data Streaming}
\label{sec:online}

Many practical inference problems arise in situations where new data is
continually made available.
%
In these problems, the data may or may not be a time series with some inherent
dynamical structure which should be modeled.
%
Some examples of these kinds of data are stock prices, monthly weather data such
as average temperatures and rainfall, and general user trends on websites.

In an online setting, recalculating the full evidence for every new batch of
data, based on the all of the previous observations may be prohibitively
inefficient.
%
In data streaming applications, data may be arriving frequently enough that
recomputing quantities from scratch every time could be slow enough to render
the model useless if the data arrival rate exceeds the computation rate.
%
It is therefore desirable to be able to calculate the evidence in a manner which
efficiently updates previous estimates in such a way that the marginal time
complexity is constant in the data set size.
%
Processing of particle collision data at the Large Hadron Collider, or the filtering
of radio frequency interference at the Square Kilometer Array are two examples
of online applications with extreme speed requirements \citep{lhc, ska}


In such online problems, inference may be formulated in terms of Bayesian
updating.
%
The initial data is used to update the prior to the first posterior distribution and
when new data arrives, the previously calculated posterior is then treated as
the prior giving a new posterior which incorporates all of the data so far.
%
For a data set $\data = \{y_n\}_{n=1}^N$, and assuming the data are independent
when conditioned on the model parameters, the $n$\textsuperscript{th} posterior
can be calculated recursively
%
\begin{equation}
  p(\theta|y_{<n+1}) = \frac{p(y_{n}|\theta)
  p(\theta|y_{<n})}{p(y_{n}|y_{<n})},
\end{equation}
%
with $y_{<n}$ shorthand for the list $(y_1,y_2,\ldots,y_{n-1})$.
The denominator $p(y_{n}|y_{<n})$, which could be called the
conditional evidence, is the posterior predictive distribution under
the previous posterior $p(\theta|y_{<n})$. 


In such online applications, the data may exhibit non-stationarities
to an extent that the model is be unable to describe.
%
For models which assume conditionally independent data this could be due to any
change in the underlying data generating process.
%
For models which explicitly model the time dynamics through some parameters
$\theta$, such as autoregressive models, this can arise if the optimal values
of $\theta$ to describe the process at one period of time differ significantly
from the optimal values of $\theta$ to describe the process at a later time.
%
Such changes can occur at discrete point in time, which could be modeled by
change points, or they could occur continuously.
%
When such extra-model non-stationarities arise, we would expect the evidence of
the model to decrease, since it is not adequate to describe such behaviour.
%
In this case the evidence can be used for change point detection or to assess
the extent to which the model is able to capture the incremental
non-stationarity of the data.
