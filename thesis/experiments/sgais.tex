\section{Results For SGAIS}
\label{sec:sgais-exp}

The results given in this section appeared in our paper in the journal
\textit{Entropy} \citep{sgais}.
%
All models and parameter values remain the same as in
Sections~\ref{sec:methods}~and~\ref{sec:models}.

\subsection{Accuracy and Speed}
\unskip
\subsubsection{Linear Regression}
Results are shown in Figure~\ref{fig:lin}.
%
The final discrepancy of SGAIS on one million data points was only
about 0.1\%.
%
For this model, our method achieved a speedup over NS by about a factor of 3.3,
and a speedup over AIS by a factor of 24.9 on one million observations.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/lin-logz.png}
    }
    \caption{}
    \label{fig:lin-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/lin-time.png}
    }
    \caption{}
    \label{fig:lin-time}
  \end{subfigure}
  \caption[Linear regression]{%
    Linear regression model.(\subref{fig:lin-logz}) shows the accuracy of SGAIS
    estimator compared to NS, AIS, and the exact evidence.
    (\subref{fig:lin-time}) shows the run-time of each method. \texttt{ns} is
    nested sampling, \texttt{ais} is annealed importance sampling, and
    \texttt{sgais} is our stochastic gradient annealed importance sampling
    approach.
  }
  \label{fig:lin}
\end{figure}

\subsubsection{Logistic Regression}
Figure~\ref{fig:log} shows the log-evidence estimates and run-time of each
algorithm for the logistic regression model.
%
For the largest data set, NS and SGAIS produced estimates that
differed by roughly 0.6\%, which is negligible.
%
SGAIS was a factor 10.4 faster than the nested sampler on
one million observations for this model.
%
AIS was not run for larger data set sizes because each subsequent run would take
more than 4000 seconds.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/log-logz.png}
    }
    \caption{}
    \label{fig:log-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/log-time.png}
    }
    \caption{}
    \label{fig:log-time}
  \end{subfigure}
  \caption[Logistic regression]{%
    Logistic regression model. (\subref{fig:log-logz}) shows SGAIS compared to
    NS and AIS.  (\subref{fig:log-time}) shows the run-time of each method. 
  }
  \label{fig:log}
\end{figure}

\subsubsection{Gaussian Mixture Model}
Figure~\ref{fig:gmm} shows the log-evidence estimates and run-time of each
algorithm for the Gaussian mixture model.
%
The estimates produced by NS and SGAIS differed on the largest data set by
roughly 0.1\%. For this model, SGAIS was about a factor of 4.9
faster than the nested sampler for one million observations.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-logz.png}
    }
    \caption{}
    \label{fig:gmm-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-time.png}
    }
    \caption{}
    \label{fig:gmm-time}
  \end{subfigure}
  \caption[Gaussian mixture model]{%
    Gaussian mixture model. (\subref{fig:gmm-logz}) shows SGAIS compared to NS
    and AIS. (\subref{fig:gmm-time}) shows the run-time of each method.
  }
  \label{fig:gmm}
\end{figure}

In all the above experiments, SGAIS converges to the same
result as NS with a negligible error for large $N$.
%
The speedup of SGAIS over NS was reduced in comparison to the sequential
estimator results in Section~\ref{sec:seq-exp}; however, SGAIS appears to be
significantly more reliable, producing accurate estimates for all data set
sizes, falling within the discrepancy band bounded by AIS and NS even for
small $N$.

Furthermore, SGAIS produces evidence estimates for all values of $N$ in a single
run, while NS and AIS only produce estimates for a single value of $N$.
%
The times required to generate the above plots for NS and AIS are therefore the
total area under the curves shown in Figures~\ref{fig:lin-time},
\ref{fig:log-time} and \ref{fig:gmm-time}.


\subsection{Non-stationarity Detection}
The log-evidence estimates shown in Figure~\ref{fig:dist-shift-logz} display
sharp changes at 1000 and 10,000 observations for the non-shuffled data.
%
The cusps in the resulting plot clearly identify the position of the change-points,
without \mbox{a priori} assuming the existence or number of change-points.
%
The numbers of annealing steps shown in~\ref{fig:dist-shift-steps} exhibit
spikes at the change-points, and remain high once more clusters are added to the
data than the model can describe.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift.png}
    }
    \caption{}
    \label{fig:dist-shift-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift-steps.png}
    }
    \caption{}
    \label{fig:dist-shift-steps}
  \end{subfigure}
  \caption[Non-stationarity detection]{%
    Evidence estimation under distribution shift.
    (\subref{fig:dist-shift-logz}) shows the evidence estimates for Gaussian
    mixture models with different numbers of mixture components.  The solid
    lines are for the non-stationary data in its original order, while the dashed
    lines are for the shuffled and therefore stationary data.
    (\subref{fig:dist-shift-steps}) shows the number of annealing steps for the
    online evidence estimates for the in-order data.
  }
\end{figure}
%
The agreement of the final evidence estimates between the shuffled and
non-shuffled data suggests that these estimates can be trusted.
%
The difference between the online and shuffled estimates is small enough to
be able to distinguish between the three models.
%
The 5- and 7-component models seem to describe the total data set better than the
3-component model, but the 5- and 7-component models have similar values for
their log-evidence, presumably due to the overlapping clusters in the data set.

\subsection{Sensitivity to Algorithm Parameters}

To evaluate the robustness of SGAIS, we also tested its dependence on parameters
with reasonably predictable influences.
%


\subsubsection{Number of Particles}
Increasing the number of particles, $M$, results in higher accuracy and a longer
running time without much effect on the number of annealing steps. 
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-M-logz.png}
    }
    \caption{}
    \label{fig:m-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-M-time.png}
    }
    \caption{}
    \label{fig:m-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-M-steps.png}
    }
    \caption{}
    \label{fig:m-steps}
  \end{subfigure}
  \caption[Sensitivity to number of particles]{%
    Sensitivity to the number of particles ($M$).
    (\subref{fig:m-logz}) shows the log-evidence estimates, (\subref{fig:m-time})
    shows the run-time, and (\subref{fig:m-steps}) plots the number of annealing
    steps for each chunk of data against the data set size until that chunk.
  }
\end{figure}


\subsubsection{Burn-in Steps}
A smaller number of burn-in SGHMC steps per intermediate distribution typically
resulted in lower accuracy and a shorter run-time.
%
A smaller number of burn-in steps also resulted in more annealing steps due to
the slower equilibration.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-burnin-logz.png}
    }
    \caption{}
    \label{fig:burnin-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-burnin-time.png}
    }
    \caption{}
    \label{fig:burnin-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-burnin-steps.png}
    }
    \caption{}
    \label{fig:burnin-steps}
  \end{subfigure}
  \caption[Sensitivity to number of burn-in steps]{%
    Sensitivity to the number of SGHMC burn-in steps.
    (\subref{fig:burnin-logz}) shows the log-evidence estimates,
    (\subref{fig:burnin-time}) shows the run-time, and
    (\subref{fig:burnin-steps}) plots the number of annealing steps.
  }
\end{figure}


\subsubsection{Mini-batch Size}
Larger mini-batch sizes typically result in higher accuracy but more computation
per SGHMC step. 
%
Larger mini-batch sizes result in fewer Bayesian updating steps but require more
annealing steps per new chunk of data.
%
Mini-batch size would typically be chosen based on the hardware capabilities of
the platform and the type of data under consideration.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-batch-size-logz.png}
    }
    \caption{}
    \label{fig:minibatch-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-batch-size-time.png}
    }
    \caption{}
    \label{fig:minibatch-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-batch-size-steps.png}
    }
    \caption{}
    \label{fig:minibatch-steps}
  \end{subfigure}
  \caption[Sensitivity to mini-batch size]{%
    Sensitivity to the mini-batch size.
    (\subref{fig:minibatch-logz}) shows the log-evidence estimates,
    (\subref{fig:minibatch-time}) shows the run-time, and
    (\subref{fig:minibatch-steps}) plots the number of annealing steps.
  }
\end{figure}


\subsubsection{Target ESS}
As expected, a higher target $\ESS$ tends to result in more annealing
steps---see Figure~\ref{fig:ess-logz}.
%
Most of the annealing work is done in the early stages of Bayesian updating.
%
Note that since we used 10 particles, a target $\ESS$ of $0.1 M = 1$ requires no
annealing steps because $\ESS$ is bounded below by 1.
%
No annealing results in high variance during the early stages of Bayesian
updating, and adaptively annealing helps to reduce that variance, with only a
small impact on the run-time.
%
This illustrates the importance of the adaptive annealing schedule in our
approach.
%
Figures~\ref{fig:ess-logz} and \ref{fig:ess-time} indicate that our approach
converges to the log-evidence with acceptable accuracy within a reasonable
time for a target $\ESS$ larger than 1.
%
Even a small target $\ESS$ was good enough to match vanilla AIS, on average.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-ESS-logz.png}
    }
    \caption{}
    \label{fig:ess-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-ESS-time.png}
    }
    \caption{}
    \label{fig:ess-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-ESS-steps.png}
    }
    \caption{}
    \label{fig:ess-steps}
  \end{subfigure}
  \caption[Sensitivity to target ESS]{%
    Sensitivity to the target effective sample size ($\ESS$).
    (\subref{fig:ess-logz}) shows the log-evidence estimates,
    (\subref{fig:ess-time}) shows the run-time, and (\subref{fig:ess-steps})
    plots the number of annealing steps.
  }
\end{figure}

\subsubsection{Learning Rate}
Interestingly, smaller values of the learning rate tend to result in less
accurate log-evidence estimates over a longer time --- see
Figure~\ref{fig:lr-logz} and \ref{fig:lr-time}.
%
We suspect this to be because a smaller learning rate does not allow the
particle to move as far each step, resulting in a slower equilibration and
requiring more annealing steps per observation.
%
This effect can be seen in Figure~\ref{fig:lr-steps}; the smaller learning rates
appear to result in a larger number of annealing steps per observation.
%
To further verify this, we investigated the interaction between the learning
rate and the number of burn-in steps.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-lr-logz.png}
    }
    \caption{}
    \label{fig:lr-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-lr-time.png}
    }
    \caption{}
    \label{fig:lr-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-lr-steps.png}
    }
    \caption{}
    \label{fig:lr-steps}
  \end{subfigure}
  \caption[Sensitivity to learning rate]{%
    Sensitivity to the learning rate. Reported learning rates are
    per-observation learning rates, that is, $\eta = \mathrm{lr}/N$.
    (\subref{fig:lr-logz}) shows the log-evidence estimates,
    (\subref{fig:lr-time}) shows the run-time, and (\subref{fig:lr-steps}) shows
    the number of annealing steps. For a learning rate of 0.01, the run-time
    shown in (\subref{fig:lr-time}) displays a change in gradient near $10^5$
    observations. This is the result of a reduced number of annealing steps but
    is not visible in (\subref{fig:lr-steps}) since we only show the number of
    annealing steps for up to $5\times10^4$ observations.
  }
\end{figure}

\subsubsection{Learning Rate and Burn-in}
We investigate the interaction between the number of SGHMC steps taken per
intermediate distribution and the learning rate by varying the learning rate,
while keeping the product of the learning rate and the number of SGHMC steps
constant.
%
While fewer burn-in steps (larger learning rate) tends to make the algorithm
faster, a smaller learning rate results in higher accuracy in the log-evidence
estimates, as seen in Figure~\ref{fig:lr-burnin-logz}.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-lr-burnin-logz.png}
    }
    \caption{}
    \label{fig:lr-burnin-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/gmm-sensitivity-lr-burnin-time.png}
    }
    \caption{}
    \label{fig:lr-burnin-time}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \ifoptiondraft{}{
      \includegraphics[width=0.5\textwidth]{exp-figs/gmm-sensitivity-lr-burnin-steps.png}
    }
    \caption{}
    \label{fig:lr-burnin-steps}
  \end{subfigure}
  \caption[Sensitivity to learning rate and number of steps]{%
    Sensitivity to the learning rate while keeping the product of the learning
    rate and the number of stochastic gradient Hamiltonian Monte
    Carlo (SGHMC) steps constant. Reported learning rates are
    per-observation learning rates, that is, $\eta = \mathrm{lr}/N$.
    (\subref{fig:lr-burnin-logz}) shows the log-evidence estimates,
    (\subref{fig:lr-burnin-time}) shows the run time, and
    (\subref{fig:lr-burnin-steps}) shows the number of annealing steps.
  }
\end{figure}
%
The decrease in accuracy with a larger learning rate is presumably due to the
discretization error in the Euler--Maruyama integrator.
%
This conjecture is supported by Figure~\ref{fig:lr-burnin-steps}: a larger learning rate
requires a larger number of annealing steps to reach the target $\ESS$.
%
Figure~\ref{fig:lr-burnin-logz} indicates that a per-observation learning rate
of 1.0 can be used and still result in estimates of acceptable accuracy on data
sets of one million observations. 
%
For a learning rate of 1.0, SGAIS achieved a speedup over NS by a factor of 83.
