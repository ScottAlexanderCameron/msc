\section{Sequential Estimation with SG-MCMC}
\label{sec:seq-exp}

Here we show the results of our sequential approach which appeared in
\citet{maxent-paper}.
%
Our experiments here compare the approach described in Section~\ref{sec:seq} to
NS for the three models given in Section~\ref{sec:models}.
%
In some of the figures below, the sequential sampler initially underestimates
the log-evidence. 
%
We believe this is due to higher variance of the initial predictive estimates
when $n$ is small, and so we also give a hybrid result which replaces the
initial terms in the log-evidence estimator with an NS estimate of the evidence for
the first 100 data points.
 
\subsection{Linear~Regression}
For the linear regression model, the exact evidence is available analytically
and is shown in Figure~\ref{fig:seq-lin-logz} for comparison.
%
Both algorithms are able to produce accurate results for this model for all data
set sizes.
%
The final error of the sequential sampler on one million data points is only
about $10^{-4}N$ (roughly 0.01\%). 
%
For this model, our method was faster than NS by about a factor of 3 on one
million~observations.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-lin-logz.png}
    }
    \caption{}
    \label{fig:seq-lin-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-lin-time.png}
    }
    \caption{}
    \label{fig:seq-lin-time}
  \end{subfigure}
  \caption[Linear regression]{%
    Linear regression model. (\subref{fig:seq-lin-logz}) shows the accuracy of
    the sequential evidence estimator (\texttt{seq}) compared to nested sampling
    (\texttt{ns}) and the exact evidence as well as the hybrid approach
    (\texttt{seq-hybrid}). (\subref{fig:seq-lin-time}) shows their run-times.  
  }
  \label{fig:seq-lin}
\end{figure}

\subsection{Logistic~Regression}
Results for the logistic regression model are shown in Figures~\ref{fig:seq-log}.
%
For the largest data set, NS and our sequential sampler produced estimates which
differed by $3\times 10^{-4}N$ (roughly 0.7\%), which is negligible.
%
Our sequential sampler was almost a factor 17 faster than the nested sampler on
one million observations for this model.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-log-logz.png}
    }
    \caption{}
    \label{fig:seq-log-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-log-time.png}
    }
    \caption{}
    \label{fig:seq-log-time}
  \end{subfigure}
  \caption[Logistic regression]{%
    Logistic regression model. (\subref{fig:seq-log-logz}) shows the sequential
    evidence estimator compared to nested sampling and
    (\subref{fig:seq-log-time}) shows their run-times.
  }
  \label{fig:seq-log}
\end{figure}


\subsection{Gaussian Mixture~Model}
The posterior distribution for this model is multimodal. Some modes are due to
permutation symmetries; these modes do not have to be  explored since each one
contains the same information. 
%
There are also some local modes which do not necessarily capture meaningful
information about the data; for example, fitting a single Gaussian to the whole
data set may be a local optimum of the likelihood function, but a poor one. 
%
If an MCMC walker finds one of these modes it can get trapped. 
%
However, we found that by Bayesian updating, the MCMC walkers tend to leave the
poor local modes early on, before they become extremely peaked. 
%
This is similar to how annealing can help prevent MCMC and optimization
algorithms from getting trapped in poor local optima. 

Results for the Gaussian mixture model are shown in Figure~\ref{fig:seq-gmm}.
%
The estimates produced by NS and our sequential sampler differed on the largest
data set by $2\times 10^{-3}N$ (roughly 0.06\%). 
%
For this model our sequential sampler was about a factor 11 faster than the
nested sampler on one million observations.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-gmm-logz.png}
    }
    \caption{}
    \label{fig:seq-gmm-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/seq-gmm-time.png}
    }
    \caption{}
    \label{fig:seq-gmm-time}
  \end{subfigure}
  \caption[Gaussian mixture model]{%
    Gaussian mixture model. (\subref{fig:seq-gmm-logz}) shows the sequential
    evidence estimator compared to nested sampling and
    (\subref{fig:seq-gmm-time}) shows their run-times.
  }
 \label{fig:seq-gmm}
\end{figure}

In all the experiments our sequential sampler seems to converge to the same
result as NS within a negligible error for large data sets.
%
The initial disagreement between NS and our sequential sampler on the first few
thousand data points, as seen in Figure~\ref{fig:seq-log-logz} and
Figure~\ref{fig:seq-gmm-logz}, may safely be attributed to high variance
in the early stages of Bayesian updating, since the proposed hybrid approach,
replacing early terms in the sequential estimator by estimates based on NS,
matches NS closely for all data set sizes.

This discrepancy is a clear shortfall of our approach; it served to
motivate the introduction of annealing steps, leading to the SGAIS
for which results are given in the next section.
