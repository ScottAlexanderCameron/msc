\section{Methodology}
\label{sec:methods}

To evaluate the accuracy and runtime performance of our proposed approach, we
estimated the evidence for three simple models on simulated data sets in an online
fashion.
%
We further evaluated the robustness of SGAIS under various choices of algorithm
parameter values.

As already set out in Section~\ref{sec:difficulty} The log-evidence typically
grows linearly in the number of data points.
%
For this reason, it is natural to measure errors in $\log\Z/N$ rather than
$\log\Z$.
%
For each model in Section~\ref{sec:models}, we measured the runtime performance
of SGAIS compared to NS and AIS for various data set sizes up to one million
observations.
%
Each of these ``interim'' data sets is taken as the first $N$ observations of the largest
data set.

\subsection{Default Parameters}
We use mini-batches of size 500 with the SGHMC parameters set to
%
$\eta = 0.1/N$, $\alpha = 0.2$, and $\hat{\beta} = 0$. Predictive distributions
are approximated using $M = 10$ particles and 20 burn-in steps for each
intermediate distribution.
%
We use a target $\ESS$ of 5 for adaptive annealing.
%
Rather than Bayesian updating by adding a single observation at a time, we add
chunks of data at a time that are the same size as the mini-batches.
%
These parameter choices are not necessarily the optimal choice for the models
below.
%
Instead, we chose the SGHMC parameters based approximately on those suggested
in~\citet{sghmc}, and we chose the number of particles and target $\ESS$ to
hopefully be sufficient for adaptive annealing but small enough to result in a
short running time.

\subsection{NS and AIS}
As our reference standards of accuracy, we implemented NS and AIS.
%
Both NS and AIS implementations used SGHMC as their MCMC kernel.
%
For AIS, we used the same parameters as SGAIS, except that
each MCMC step uses the whole data set instead of mini-batches.
%
We implemented NS with 20 SGHMC steps to sample from the constrained prior.
%
NS still requires the full data set to check the constraints and so cannot take
advantage of mini-batching.
%
For SGHMC used with NS, we used parameters $\eta = 10^{-3}$, $\alpha = 0.1$, and
$\hat{\beta} = 0$ because there is no gradient noise when sampling from the
prior. 
%
Results reported are for two particles; more particles result in similar but
slower behaviour. 
%
We allow NS to run until the additive terms are less than 1\% of the current
$\hat\Z$ estimate.
%
This is a popular stopping criterion and is also used in~\citet{sandwich}. 
%
%More information about our implementations is given in Appendix~\ref{ap:ns}.
%\todo{correctness of ns/ais or self evident?}
%
Since we found AIS to be much slower than NS, we only ran AIS on data sets small
enough to finish within 4000 seconds.

The results from our conference paper, shown in Section~\ref{sec:seq-exp} only
compare to NS, since we had not yet implemented any annealing algorithms, while
the results in Section~\ref{sec:sgais-exp} compare SGAIS to both NS and AIS.
%
We consider discrepancies between results from different algorithms to be
acceptable if they are of the same magnitude as discrepancies between NS and
AIS.
%
Since all these estimators are likely to underestimate the evidence, it is
generally safe to assume the largest evidence estimate is the most accurate.

\subsection{Sensitivity Analysis}
We investigated the sensitivity of SGAIS to the following parameters:
number of particles $M$, the target $\ESS$, the number of burn-in steps for each
intermediate distribution, the learning rate $\eta$, and the mini-batch size $|B|$.
%
Each test was done by varying one parameter while keeping the others fixed at
their default values.

\subsection{Run-time Environment}
Our experiments were executed on a laptop with an Intel i7 CPU and 8GB of RAM,
running Arch Linux; kernel release 5.1.7. 
%
For fair comparison, all code was single-threaded. Multithreading gives a
considerable speedup when calculating the likelihood on large data sets but can
introduce subtle complexities that are difficult to control and quantify in tests of
run-time performance.

\subsection{Non-stationarity Detection}
During online estimation, changes in the data-generating distribution
should typically be detectable in the evidence estimates.
%
To investigate this, we generated simulated data with varying numbers of
``clusters'' of data points based on different simulation parameter values. 
%
Histograms of the simulated data are shown in
Figure~\ref{fig:simulated-dist-shift-data}. 
%
The first 1000 observations were generated from 3 Gaussian distributions, the
next 9000 observations were generated from 5 Gaussian distributions, including
the 3 used to generate the first observations, and the remaining 90,000
observations were generated from 7 Gaussian distributions, including the
previous 5.
%
Some of the clusters overlap, so it is not immediately obvious from the
histograms how many clusters  there actually are.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.24\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift-1k.png}
    }
    \caption{}
    \label{fig:dist-1k}
  \end{subfigure}
  \begin{subfigure}{0.24\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift-9k.png}
    }
    \caption{}
    \label{fig:dist-9k}
  \end{subfigure}
  \begin{subfigure}{0.24\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift-90k.png}
    }
    \caption{}
    \label{fig:dist-90k}
  \end{subfigure}
  \begin{subfigure}{0.24\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{exp-figs/dist-shift-100k.png}
    }
    \caption{}
    \label{fig:dist-100k}
  \end{subfigure}
  \caption[Non-stationary data]{
    Histograms of non-stationary simulated data. (\subref{fig:dist-1k}) shows
    the first 1000 observations, (\subref{fig:dist-9k}) shows the next 9000
    observations, (\subref{fig:dist-90k}) shows the last $90,000$ observations,
    and (\subref{fig:dist-100k}) shows the total data set. In~each of the three
    time phases, the data-generating distribution produces data with more
    clusters than before.
  }
  \label{fig:simulated-dist-shift-data}
\end{figure}

We evaluated the effect of this type of distribution shift on SGAIS by
estimating the evidence online for Gaussian mixture models --- see
Section~\ref{sec:models} --- with 3, 5, and 7 mixture components.
%
For comparison, we then shuffled the data to enforce stationarity and estimated
the evidence for these three models on the shuffled data.
%
If the final evidence estimates for the in-order and shuffled data differ
significantly then this may indicate that the particles are getting trapped in
local modes before the change-points occur.


