\section{Stochastic Gradient Annealed Importance Sampling}
\label{sec:sgais}

Although we were able to get acceptable results using using the evidence
estimator discussed in the previous section, there were some clear shortcomings
regarding the robustness of the approach.
%
These shortcomings will be revisited in further detail in
Section~\ref{sec:seq-exp}.
%
Here, we present an algorithm we call stochastic gradient annealed
importance sampling~(SGAIS), which addresses these shortcomings.
%
SGAIS is the key novel contribution presented in this thesis.

The main shortcomings of the evidence estimator described in the previous
section are due to the high variance of the predictive probability estimates.
%
Earlier predictive probability estimates exhibit high variance because the
posterior is still quite diffuse and so individual observations still contain
significant additional information.
%
High variance can also ensue when some observations seem unlikely in the model,
possibly due to some non-stationarity in the data which the model may not be
capable of adequately describing.
%
We can improve the quality of the final evidence estimator by instead using a
more accurate approach to calculating predictive probabilities than simple
averaging.
%
For this we use AIS.
%
This can be done by starting with $M$ particles drawn approximately from
$\theta_i^{(n-1)}\sim p(\theta|y_{<n})$, presumably from a previous run of AIS
with importance weights which we will label as $w_i^{(n-1)}$.
%
We then treat $p(\theta|y_{<n})$ as the prior and anneal the likelihood of new
observations $p(y_n|\theta)$. 
%
The resulting importance weights, call them $\tilde w_i^{(n)}$, depend on the
initial starting point of the particle.
%
If the particle had been initially sampled exactly from the posterior
$p(\theta|y_{<n})$ instead of approximately, then the weights $\tilde w_i^{(n)}$
would be unbiased estimates of the posterior predictive.
%
We can instead reweight these importance weights by the corresponding importance
weights for each particle produced by the previous AIS run.
%
By using the unnormalized importance weights, we can estimate the `unnormalized'
posterior predictive, i.e.\ the probability of the total data set seen so far.
%
Defining $w_i^{(n)} := \tilde w_i^{(n)} \, w_i^{(n-1)}$ we have the estimator
%
\begin{equation}
  \hat{p}(y_{\le n}) := \frac{1}{M} \sum_i w_i^{(n)}.
\end{equation}
%
The final evidence estimator is then $\hat\Z = \frac{1}{M} \sum_i w_i^{(N)}$.
%
This estimator is unbiased.
%
Define $h(\theta_i) = \E{\tilde w_i^{(n)}\middle|\theta_i}$, where the expectation
is taken with respect to the particle trajectory over one run of AIS starting at
$\theta_i$.
%
From the unbiasedness of AIS, $\E[p(\theta|y_{<n})]{h(\theta)}=p(y_n|y_{<n})$.
%
By linearity of expectation
%
\begin{align}
  \E{\hat{p}(y_{\le n})}
  &= \E{\frac{1}{M} \sum_i
  \E{\tilde w_i^{(n)}\middle|\theta_i^{(n-1)}}\,w_i^{(n-1)}},\\
  &= \E{\frac{1}{M} \sum_i h(\theta_i^{(n-1)})\,w_i^{(n-1)}},\\
  &= \E[p(\theta|y_{<n})]{h(\theta)}\,p(y_{<n}),\label{eq:sgais:unbiased}\\
  &= p(y_n|y_{<n}) p(y_{<n}),\\
  &= p(y_{\le n}).
\end{align}
%
The third line (\ref{eq:sgais:unbiased}) follows from the unbiasedness of AIS for
unnormalized expectations.
%
Thus, we no longer have to explicitly multiply the predictive probability
estimates; we can just update the importance weights from the previous AIS run,
and the resulting estimator will automatically take all the previous
predictive probabilities into account.

This can be viewed as modifying AIS to use the sequence of
intermediate distributions
%
\begin{equation}
  f_n^{(\lambda)}(\theta) = p(y_n|\theta)^{\lambda}
  \left[\prod_{k<n} p(y_k|\theta) \right] p(\theta),
\end{equation}
%
where for each $n$ we adaptively interpolate $\lambda$ between zero and one.
%
The resulting importance weight updates can be calculated without iterating over
the entire data set
%
\begin{equation}
  w_i^{(t)} \gets
  w_i^{(t-1)}\frac{f_n^{(\lambda_t)}(\theta_i^{(t-1)})}{f_n^{(\lambda_{t-1})}(\theta_i^{(t-1)})}
  = w_i^{(t-1)} \, p(y_n|\theta_i^{(t-1)})^{\lambda_t - \lambda_{t-1}}.
\end{equation}
%
Each MCMC transition then uses an SGHMC kernel with the mini-batch stochastic
potential energy estimate
%
\begin{equation}
  \label{eq:sgais-potential}
  \hat{U}_n^{(\lambda)}(\theta) =
  -\lambda\log{p}(y_n|\theta)
  -\frac{n-1}{|B|}\sum_{y \in B} \log{p}(y|\theta)
  -\log{p(\theta)}.
\end{equation}

For combined Bayesian updating with annealing, it is important that the
annealing schedules be chosen adaptively since individual observations typically
contain significant information in the early stages of Bayesian updating when
the posterior is still quite diffuse, but typically only contain a small amount
of additional information later on once the posterior has become quite
constrained.
%
We select the annealing schedule adaptively as described in
Section~\ref{sec:ais} where the ESS is calculated as
%
\begin{equation}
  \ESS(\Delta) = \frac{\left(\sum_i \omega_i(\Delta)\right)^2}{\sum_i
  \omega_i(\Delta)^2},
  \qquad
  \omega_i(\Delta) = p(y_n|\theta_i^{t-1})^{\Delta},
\end{equation}
%
where $\Delta := \lambda_t - \lambda_{t-1}$.

SGAIS is summarized in Algorithm~\ref{alg:sgais}.
%
Here one may include the possibility of resampling steps, similar to SIR, in
which case the particles $\theta_i$ would be resampled proportionally to their
importance weights $w_i$, thereafter uniformly setting the importance weights to
their mean $w_i \gets \frac{1}{M} \sum_j w_j$.
%
\begin{algorithm}[htb]
  \caption{Stochastic Gradient Annealed Importance Sampling}
  \label{alg:sgais}
  \textbf{Input:} Data $\data=\{y_n\}_{n=1}^N$, number of particles $M$,
  pdfs $p(y|\theta),p(\theta)$, target ESS: $\ESS^*$ \\
  \textbf{Output:} $\hat{\Z}$ evidence estimator
  \begin{algorithmic}[1]
    \State $\forall_i$: sample $\theta_i \sim p(\theta)$
    \State $\forall_i$: $w_i \gets 1$
    \For{$n = 1,\ldots,N$}
      \State $\lambda \gets 0$
      \While{$\lambda < 1$} 
        \State $\Delta \gets \argmin_{\Delta}[\ESS(\Delta) - \ESS^*]$
        \Comment{$\Delta \in (0, 1-\lambda]$}
        \State $\lambda \gets \lambda + \Delta$
        \State $\forall_i$: $w_i \gets w_i \, p(y_n|\theta_i)^\Delta$
        \State optionally resample particles
        \State $\forall_i$: $\theta_i \gets \Call{Sghmc}{\theta_i,
        \hat{U}_n^{(\lambda)}}$
        \Comment{$\hat{U}_n^{(\lambda)}$ defined in
        Equation~\ref{eq:sgais-potential}}
      \EndWhile
    \EndFor
    \State \Return $\hat{\Z} = \frac{1}{M}\sum_i w_i$
  \end{algorithmic}
\end{algorithm}
