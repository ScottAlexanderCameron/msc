\section{Sequential Evidence Estimation with SG-MCMC}
\label{sec:seq}

Recall that evidence can be factorized into a product of predictive
distributions
%
\begin{equation}
  \Z = \prod_n p(y_n|y_{<n}),
\end{equation}
%
where each predictive distributions takes the form of an expectation
value of the current data point $y_n$ with respect to the posterior of
all previous data,
%
\begin{equation}
  p(y_n|y_{<n}) = \E[p(\theta|y_{<n})]{p(y_n|\theta)}.
\end{equation}
%
This suggests that, given an estimator for predictive distributions
$\hat{p}(y_n|y_{<n})$, the evidence could be estimated as
%
\begin{equation}
  \label{eq:seq-logz}
  \hat\Z := \prod \hat{p}(y_n|y_{<n}).
\end{equation}
%
Indeed this is the approach taken by SIR (Algorithm~\ref{alg:sir}).

One detail that we did not fully address in our conference paper is that the
estimator $\hat\Z$ here will not necessarily be unbiased when the estimators of
the predictive probabilities are correlated.
%
However if the predictive estimators are consistent then $\hat\Z$ will also be
consistent since the bias will tend to zero as the variance of the predictive
estimators tend to zero.

The main computational difficulty with this formulation is to estimate the
predictive distributions in a way that scales favourably with the data set size.
%
We can achieve linear time complexity in the resulting evidence estimation
algorithm if we can guarantee constant time complexity for estimating the
predictive distributions.
%
In our conference paper, we considered the simple case of estimating predictive
distributions with MCMC samples.
%
\begin{equation}
  \label{eq:seq-pred}
  \hat{p}(y_n|y_{<n}) := \frac{1}{M} \sum_{i=1}^M p(y_n|\theta_i),
\end{equation}
%
where the particles $\theta_i$ are drawn approximately from the posterior
$p(\theta|y_{<n})$ using SGHMC.
%
Since we expect each successive posterior $p(\theta|y_{<n})$ to be similar to
its relative prior $p(\theta|y_{<n-1})$, the particles can generally be reused
and only a short burn-in time should be required to converge to the next
posterior.
%
This approach is quite similar in nature to SIR except for two key differences. 
%
Firstly, there is no resampling step, but the importance weights are still
averaged upon each iteration; and secondly, we use SGHMC instead of some other
MCMC method.

Averaging $p(y_n|\theta_i)$ at each step results in some bias in the final
evidence estimator since the predictive distributions are correlated,
but results in a lower variance then if no averaging were done.
%
Since we are interested in scaling to large data sets, trading some bias
for reduced variance seems worthwhile.
%
This bias would be removed if resampling had been incorporated, but we did not
consider this in the conference paper.

If MH based kernels such as HMC were used instead of SGHMC, then each step of
the algorithm would require iteration over all $n$ previous observations just to
generate a new state for each particle.
%
This results in an algorithm which has at least quadratic time complexity in the
data set size.
%
Since SGHMC uses mini-batching, allowing the particles to burn in for a fixed
number of iterations for each new observation results in a predictive
probability estimator that has constant time complexity in the data set size,
%
i.e.\ the time required to compute $\hat{p}(y_n|y_{<n})$ does not
depend on $n$.
%
The resulting evidence estimator can then be computed in time linear in the data
set size, which is a considerable advantage.
%
Linear time complexity is the best that one could hope for, since calculating
the probability of a data set in any reliable way necessitates visiting each
data point at least once.

In order to ensure that the new data is taken into account during the SGHMC
steps, we used and energy function explicitly incorporating the new data in
addition to the mini-batches sampled from previous data. The potential
%
\begin{equation}
  U_n(\theta) = -\log{p}(y_n|\theta) - \frac{n-1}{|B|}\sum_{y\in
  B}\log{p}(y|\theta) - \log{p}(\theta),
\end{equation}
%
is used to generate samples approximately from the $n$\textsuperscript{th}
posterior $p(\theta|y_{\le n})$, where the mini-batch is sampled i.i.d.\ with
replacement from the previously seen data $B \subset \{y_k\mid k<n\}$.

In practice, Bayesian updating by a single observation at a time is not particularly
efficient. As $n$ gets large, we expect that each new observation will generally
contain very little new information and so by processing a number of
observations at a time, for example in chunks the same size as a typical
mini-batch, we can take advantage of data parallelism while requiring fewer MCMC
steps in total.
%
One difficulty we encountered is that the estimator in
Equation~\ref{eq:seq-pred} tends to have high variance when $n$ is small. This
high variance is worsened for high-dimensional parameter spaces and Bayesian
updating with many observations at a time.
%
The result is that when $n$ is small, we may need to estimate predictive
probabilities for a small number of observations at a time and only increase the
number of observations processed at a time once $n$ is large enough that new
observations contain very little new information.
%
For the results reported in our conference paper, we used a Bayesian updating
schedule which initially only added 20 observations at a time, increasing
linearly at a rate of 20 until a maximum of 500.
%
It is possible the schedule may need to
be hand-tuned on a per-application basis and does not necessarily scale well to
complex models.

Since we are interested in estimating evidence for large data sets, high
variance in the initial predictive estimates may be acceptable if the final
evidence estimate is still accurate.
%
In this case the high variance in the initial terms in
Equation~\ref{eq:seq-logz} may be carefully corrected.
%
To this end we further considered a hybrid approach to estimating evidence which
replaces the initial high variance terms in Equation~\ref{eq:seq-logz} with an
estimate provided by nested sampling.
%
This estimate can be computed accurately on a small number of data points,
after which the stochastic gradient method can be applied to efficiently scale
up to large data sets.
