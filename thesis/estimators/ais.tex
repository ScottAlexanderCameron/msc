\section{Annealed Importance Sampling}
\label{sec:ais}

We noted before that directly averaging the likelihood over prior samples yields
a high variance estimator which is completely unreliable.
%
There are many methods to reduce the variance of Monte Carlo estimators, one of
which is importance sampling.
%
Importance sampling is a simple technique which can easily be used to augment
sampling algorithms. 
%
The basic principle of importance sampling relies on the equality
%
\begin{equation}
  \E[p(\theta)]{h(\theta)} =
  \E[q(\theta)]{h(\theta)\frac{p(\theta)}{q(\theta)}}
\end{equation}
%
for any distribution $q$ whose support covers the support of
$p$.
%
We can therefore replace any unbiased estimator for $H := \E[p(\theta)]{h(\theta)}$,
where $h$ and $p$ are allowed to be arbitrary, with another estimator simply by
replacing $p(\theta)$ with $q(\theta)$ and $h(\theta)$ with
$h(\theta)p(\theta)/q(\theta)$.
%
The resulting estimator will be unbiased but it will generally have lower or
higher variance than the original estimator.
%
This idea is very general and can be applied to MCMC sampling, providing the
same guarantees as direct MCMC, or any other unbiased estimator.
%
While there exists a host of research on importance sampling techniques,  much
of it is beyond the scope of this thesis.
%
Corresponding to the simple estimator of $\E[p(\theta)]{h(\theta)}$ based on
direct sampling
%
\begin{equation}
  \hat{H}_{\mathrm{DS}} := \frac{1}{M} \sum_{i=1}^M h(\theta_i),
  \qquad
  \theta_i \overset{\smbox{i.i.d.}}{\sim} p(\theta),
\end{equation}
%
given a proposal distribution $q$, also called the importance distribution, we
obtain the simple importance sampling estimator
%
\begin{equation}
  \label{eq:importance-sampling}
  \hat{H}_{\mathrm{IS}} := \frac{1}{M} \sum_{i=1}^M
  h(\theta_i)\,w_i,
  \qquad
  \theta_i \overset{\smbox{i.i.d.}}{\sim} q(\theta),
\end{equation}
%
where $w_i = p(\theta_i)/q(\theta_i)$ are the importance weights, whose
expectation with respect to $q$ is one.
%
The difference in the variance of the direct sampling estimator and the
importance sampling estimator is
%
\begin{equation}
  \frac{1}{M}\E[p(\theta)]{h^2(\theta)
  \left(1-\frac{p(\theta)}{q(\theta)}\right)}.
\end{equation}
%
This quantity is positive if the importance sampler has a lower variance than
the original estimator, and it is negative if the importance sampler has a
higher variance.
%
If $q(\theta)$ is small in regions where $p$ places considerable mass, then the
ratio $p(\theta)/q(\theta)$ will contribute significantly to the above
expectation resulting in a higher variance for the importance sampling estimator
than the original.
%
However if $q$ places considerable mass in regions where $p(\theta)$ is small,
then the variance of the importance sampling estimator will not be penalized as
heavily.
%
For this reason, it is usually recommended that $q$ be a distribution with
heavier tails than $p$.
%
Naturally, the variance of the estimator will depend strongly on the function
$h$. 
%
Proposal distributions which place considerable mass in regions which contribute
most to the expectation have lower variance than those that do not.

Theoretically, the minimum variance importance sampling estimator samples from
the  proposal distribution would be
%
\begin{equation}
  q_{\mathrm{opt}}(\theta) =
  \frac{h(\theta)p(\theta)}{\E[p(\theta)]{h(\theta)}}.
\end{equation}
%
The corresponding simple importance sampling estimator is then
%
\begin{equation}
  \hat{H}_{\mathrm{opt}} := \frac{1}{M} \sum_{i=1}^M \E[p(\theta)]{h(\theta)},
  \qquad
  \theta_i \overset{\smbox{i.i.d.}}{\sim} q_{\mathrm{opt}}(\theta).
\end{equation}
%
This estimator simply adds a number of constants and has zero variance.
%
It also requires knowledge of the exact value of the expectation which we are
trying to estimate in the first place and is therefore not very practical.
%
In practice, for simple importance sampling,
we also need to be able to generate samples from the proposal distribution $q$
and so this limits our choice to simple common distributions.
%
The proposal distribution needs to overlap well with $h(\theta)p(\theta)$ to be
effective, but in high dimensions with a limited choice of common distributions,
this tends to be difficult or impossible to achieve.

Importance sampling can also be iterated with a sequence of importance
distributions. For two distributions $q$ and $r$ such that the support of $q$
covers the support of $p$ and the support of $r$ covers the support of $q$,
%
\begin{equation}
  \E[p(\theta)]{h(\theta)} =
  \E[q(\theta)]{h(\theta)\frac{p(\theta)}{q(\theta)}} =
  \E[r(\theta)]{h(\theta)\frac{p(\theta)}{q(\theta)}\frac{q(\theta)}{r(\theta)}}.
\end{equation}
%
This provides no benefit for the simple importance sampling estimator as the
intermediate distributions in the fractions simply cancel. It can, however, be
used in more complex algorithms such as sequential importance sampling and
annealed importance sampling~(AIS).
%
AIS~\citep{ais} estimates expectations, by iterated approximate sampling from a
sequence of distributions, bridging from a distribution which is easy to sample
from to some desired distribution.
%
As before, each distribution in the sequence must have a support which covers
the support of the next distribution, and in order to be effective, each
distribution in the sequence should be more diffuse than the next but without
being too dissimilar.

While the theory of AIS applies to any sequence of intermediate distributions
satisfying the support requirement, it is almost always used with a sequence of
distributions which are geometrically interpolated between a simple
distribution and the desired distribution.
%
AIS does not require the normalization constant of the desired distribution and
provides an unbiased estimator of the ratio of the normalizing constants of the
desired distribution and the initial importance distribution.
%
Since we want to estimate Bayesian evidence, this will be our main interest in
AIS.
%
For Bayesian inference problems, the most natural initial distribution is the
prior, the desired final distribution is the posterior, and the sequence of
unnormalized geometrically interpolated distributions is
%
\begin{equation}
  f_t(\theta) = p(\data|\theta)^{\lambda_t} p(\theta),
\end{equation}
%
where $(\lambda_t)_{t=0}^T$ is any increasing sequence with $\lambda_0 = 0$ and
$\lambda_T = 1$.
%
The sequence of $\lambda$'s is called the annealing schedule.
%
In the above equation, $\lambda$ plays a role similar to the inverse temperature
in the Boltzmann distribution discussed in Section~\ref{sec:hamiltonian}; hence
the name ``annealing''.
%
The annealing schedule controls the number of intermediate distributions as well
as the extent to which each consecutive importance distribution differs from
the last, and so the accuracy and computational efficiency depend very strongly
on the choice of annealing schedule.
%
For now we will assume that the schedule is fixed; later we will discuss how to select the
annealing schedule adaptively.

AIS proceeds by initially generating $M$ particles $\theta_{1:M}^{(0)}$ --- not
necessarily independently --- from the distribution $f_0$, which for our
purposes is the prior, and then for each intermediate distribution in the
sequence, updating the importance weights and then moving the particles by
applying some Markov kernel which leaves the current distribution invariant.
%
The application of a Markov kernel to update the particles allows iterated
importance sampling without the computation being redundant as it is in the
simple importance sampling estimator.
%
The importance weights are all initialized to $1$ and at each time-step $t$ are
updated based on the current particles $\theta_i^{(t-1)}$ as follows:
%
\begin{equation}
  \label{eq:ais-weights}
  w_i^{(t)} = w_i^{(t-1)} \, \frac{f_t(\theta_i^{(t-1)})}{f_{t-1}(\theta_i^{(t-1)})}
  = w_i^{(t-1)} \, p(\data|\theta_i^{(t-1)})^{\lambda_t-\lambda_{t-1}}.
\end{equation}
%
The particles are then updated by sampling $\theta_i^{(t)}$ from a Markov
kernel, based at $\theta_i^{(t-1)}$, which has $f_t$ as its stationary
distribution.
%
After each time step $t$, the expectation of the importance weights is the
normalization constant of $f_t$, and so they can be used to estimate the
unnormalized expectation of a test function $h$ over $f_t$ in a similar manner
to Equation~\ref{eq:importance-sampling}
%
\begin{equation}
  \E{h(\theta_i^{(t-1)})\,w_i^{(t)}} = \int
  h(\theta)\,p(\data|\theta)^{\lambda_t}p(\theta) \, d\theta.
\end{equation}
%
Therefore at each time-step, we have the unbiased estimator
%
\begin{equation}
  \hat{H}^{(t)} := \frac{1}{M} \sum_{i=1}^M
  h(\theta_i^{(t-1)}) \, w_i^{(t)}
  \quad
  \xrightarrow{M\to\infty}
  \quad
  \int h(\theta)\,p(\data|\theta)^{\lambda_t}p(\theta) \, d\theta.
\end{equation}
%
Proof that this estimator is unbiased is based on considering the extended state
space of trajectories $\theta_i^{1:T}$, and treating the sampling procedure as
a simple importance sampler on this space.
See \citet{ais} for the detailed proof.
%
The AIS evidence estimator is the special case $\hat{\Z} = \hat{H}^{(T)}$ for
the test function $h(\theta)=1$.
%
Biased but consistent estimators of posterior expectations of a test function
$h$ can be obtained by simply dividing the unnormalized estimator by the
evidence estimator.
%
This can be used for estimating posterior predictive probabilities of a test
value $y'$ by using $h(\theta) = p(y'|\theta)$.

At each time step, the Markov transition must exhibit $f_t$ as its stationary
distribution. We can efficiently update the particles using HMC with the
following potential energy function
%
\begin{equation}
  \label{eq:ais-potential}
  U^{(\lambda)}(\theta) = -\lambda\log{p}(\data|\theta) - \log{p}(\theta).
\end{equation}
%
By using HMC instead of some other simple MH algorithm or MALA, the particle is
allowed to travel further and more closely converge to $f_t$.
%
SGHMC could also be used, however the importance weight updates in
Equation~\ref{eq:ais-weights} still require iterating over the whole data set so
in this form AIS cannot take full advantage of mini-batching.

By choosing a geometric interpolation of the prior and posterior for the
intermediate distributions, we guarantee that the importance distributions will
satisfy the support requirement, and we can be fairly certain that each
intermediate distribution is more diffuse than the next.
%
Therefore to reduce the variance of the estimators, we should choose the
annealing schedule such that each intermediate distribution is as close as
possible to the next.
%
A common way to do this is to calculate the empirical effective sample
size~(ESS) \mbox{\citep{ess}}, and adaptively choose each subsequent $\lambda$ in the
sequence such that the ESS is approximately equal to some user-specified target
\citep{adaptive-smc, adaptive-smc-convergence}.
%
For importance sampling the ESS is given by
%
\begin{equation}
  \ESS = \frac{\left(\sum_i w_i\right)^2}{\sum_i w_i^2}.
\end{equation}
%
This is a kind of heuristic which can be interpreted loosely as the number of
samples from the target distribution which would be required for a naive Monte
Carlo estimator to have similar variance to the importance sampled estimator.
%
This interpretation is not always strictly valid, since $1\le\ESS\le M$ when
there are $M$ particles, and directly sampling the target may have a much larger
variance or a much lower variance than an importance sampled estimator.
%
However for AIS it can simply be thought of as the effective number of particles
for calculating expectations with respect to distribution $f_t$ when the
particles are drawn from $f_{t-1}$.
%
For the importance weights in AIS, the ESS will depend on the annealing
schedule. Defining $\Delta = \lambda_t - \lambda_{t-1}$,
%
\begin{equation}
  \ESS(\Delta) = \frac{\left(\sum_i \omega_i(\Delta)\right)^2}{\sum_i
  \omega_i(\Delta)^2},
\end{equation}
%
where
%
\begin{equation}
  \omega_i(\Delta) = p(\data|\theta_i^{t-1})^{\Delta}.
\end{equation}
%
The annealing schedule can be chosen adaptively by solving for $\lambda_t$ in
the equation $\ESS(\lambda_t-\lambda_{t-1})=\ESS^*$ given some target $\ESS^*$.
%
ESS is monotonic in $\Delta$ so a simple bisection search can be used.

\begin{algorithm}[htb]
  \caption{Annealed Importance Sampling}
  \label{alg:ais}
  \textbf{Input:} Data $\data$, number of particles $M$,
  pdfs $p(\data|\theta),p(\theta)$, target ESS: $\ESS^*$ \\
  \textbf{Output:} $\hat{\Z}$ evidence estimator
  \begin{algorithmic}[1]
    \State $\forall_i$: sample $\theta_i \sim p(\theta)$
    \State $\forall_i$: $w_i \gets 1$
    \State $\lambda \gets 0$
    \While{$\lambda < 1$} 
      \State $\Delta \gets \argmin_{\Delta}[\ESS(\Delta) - \ESS^*]$
      \Comment{$\Delta \in (0, 1-\lambda]$}
      \State $\lambda \gets \lambda + \Delta$
      \State $\forall_i$: $w_i \gets w_i \, p(\data|\theta_i)^\Delta$
      \State $\forall_i$: $\theta_i \gets \Call{Hmc}{\theta_i, U^{(\lambda)}}$
      \Comment{$U^{(\lambda)}$ defined in Equation~\ref{eq:ais-potential}}
    \EndWhile
    \State \Return $\hat{\Z} = \frac{1}{M}\sum_i w_i$
  \end{algorithmic}
\end{algorithm}
%
AIS is summarized in Algorithm~\ref{alg:ais} where HMC is used to update the
particles.
%
One potential benefit of AIS in the context of equilibrium statistical physics
simulations is that it provides an estimate of the partition function
$\Z(\lambda)$ and expectations of interest for each value of $\lambda$ in the
annealing schedule, from which interesting properties of the system can be
extracted from the intermediate values, such as critical points and phase
diagrams.
%
For evidence estimation, on the other hand, this is probably unnecessary since
the intermediate distributions have no real meaning to us.


In systems which exhibit phase changes and critical transition, a long
burn-in time might be required for particles distributed approximately according
to $f_{t-1}$ to reach $f_t$, which may result in high variance of the estimator
near to the critical point.
%
Adaptively annealing helps to reduce these effects but care should still be
taken when applying AIS to complex systems.
%
Although these phase changes and critical transition occur frequently in physics
models, they are also known to occur in Bayesian models.
