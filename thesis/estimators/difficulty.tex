\section{What Makes Evidence Estimation Difficult?}
\label{sec:difficulty}

Before considering the more successful algorithms for evidence estimation, we
briefly outline the structural difficulties of doing so and show by example how
two simple approaches fail miserably to provide reliable evidence estimates.

\subsection{Evidence and Entropy}

Evidence is the integral of the likelihood with respect to the prior
%
\begin{equation}
  \Z := p(\data) = \int p(\data|\theta) p(\theta) \, d\theta.
\end{equation}
%
It is the probability that a model assigns to the sequence of values
$\data$ which were observed.

Now consider the true data generating distribution; the data set is generated
according to this distribution through some physical process.
%
This distribution is unobservable and we generally propose some model which we
hope is flexible enough that, in the large data limit, its posterior predictive
distribution matches closely with the true data generating distribution for each
new observation.
%
We will assume that the data is independently and identically distributed.
%
If this is not the case, we can shuffle and resample the data so that it is,
otherwise the following argument can be generalized in certain cases.
%
For any reasonably sized data set, the sequence of observations will, with
overwhelming probability, lie in the typical set \citep[chapter 4]{mackay}.
%
The typical set consists of those outcome sequences where the relative frequency
of any particular outcome in the sequence is approximately equal to the
probability of that outcome under the generating distribution.
%
For example, for binary outcomes, if the generating process has a probability of
0.25 of producing a zero, and a probability of 0.75 of producing a one, then the
typical set is the collection of sequences for which approximately one quarter of the elements
are zero and three quarters are one.
%
Each sequence in the typical set has probability (density; in the case of
continuous data) on the order of
%
\begin{equation}
  p_{\mbox{\tiny true}}(\data) \approx e^{-NH},
\end{equation}
%
where $H$ is the entropy of the data generating distribution if the data is
discrete and $H$ is the differential entropy with respect to the Lebesgue
measure in the case of continuous data:%
%
\footnote{%
  The entropy is taken with respect to the Lebesgue measure in the continuous case
  because the probability density $p_{\mbox{\tiny true}}(\data)$ is defined in
  terms of the product Lebesgue measure. We could replace this by another
  measure in both the entropy definition and the probability density definition,
  provided we use the same measure.
}
%
\begin{equation}
  H = -\sum_y p(y) \log{p(y)}
  \qquad \mbox{or}
  \qquad
  H = -\int p(y) \log{p(y)} \, dy.
\end{equation}
%
The probability of observing any particular sequence of outcomes, decreases
exponentially with the number of observations, and since the true data
generating distribution is a better description of the data than any
model we might come up with, we expect that the evidences of our models would be
upper bounded by this extremely tiny probability.
%
Probabilities are non-negative, so for any model, we expect the evidence to obey the
following inequality,
%
\begin{equation}
  0 \le \Z \le e^{-NH}.
\end{equation}

If the uncertainty of the evidence estimator is larger than or comparable to
the estimator itself, then it is practically useless.
%
Estimating extremely small, but positive quantities poses a challenge for Monte
Carlo integration.
%
This same problem arises in rare event simulations \citep{multilevel-splitting},
where one typically wants to estimate extremely tiny probabilities of events
lying in the tails of distributions.

\subsection{Likelihood-Weighted Prior Sampling}

For certain simple models, the evidence can be calculated analytically, but for
almost all interesting or realistic models, the evidence is analytically
intractable.
%
In this case one has to resort to numerical methods.
%
In one or two dimensions quadrature can be used, but the time complexity of
quadrature algorithms increases exponentially with the number of dimensions,
rendering them infeasible for large problems.
%
Instead, for high dimensional integration, one typically resorts to Monte Carlo
methods.
%
To use Monte Carlo integration, the evidence is expressed as an expectation, for
example
%
\begin{equation}
  \Z = \E[p(\theta)]{p(\data|\theta)},
\end{equation}
%
and an estimator is found which is consistent and preferably unbiased.
%
The simplest example of an evidence estimator is likelihood weighted prior
sampling
%
\begin{equation}
  \label{eq:lw-estimator}
  \hat{\Z} := \frac{1}{M} \sum_{i=1}^M p(\data|\theta_i),
  \qquad
  \theta_i \overset{\smbox{i.i.d.}}{\sim} p(\theta).
\end{equation}
%
Despite being unbiased, the value of $M$ required for the estimator to converge
on realistic models is typically so large that the Monte Carlo simulation would
not complete in any reasonable amount of time.


In typical Bayesian inference problems, the prior is quite diffuse to allow the
model to learn from the data without introducing unjustified biases,
while the likelihood is usually very peaked:
%
usually only a very small volume of the prior is covered by any significant
likelihood values.
%
This means that a sample from the prior will typically not coincide
with a region of significant likelihood and results in a distribution
for the estimator $\hat{Z}$ which has a very long tail, with the bulk
of the distribution lying far below the actual evidence value.
%
The long tail of this distribution is the reason for the estimator still being
unbiased despite the high probability to greatly underestimate.

As an example, Figure~\ref{fig:example-prior-like} shows the likelihood
and prior for a simple Gaussian--Gaussian model with 100 data points. 
%
\begin{figure}[htbp]
  \centering
  \ifoptiondraft{}{
    \includegraphics[width=0.5\textwidth]{ex-like-prior.png}
  }
  \caption[Prior and likelihood for a Gaussian--Gaussian model]{%
    The prior and likelihood for a
    Gaussian--Gaussian model with 100 data points. The prior volume over the
    shaded region is the evidence.
  }
  \label{fig:example-prior-like}
\end{figure}
%
The data was generated by sampling a Gaussian with mean
$\mu_{\mathrm{true}}{=}2$ and unit variance.
%
The model applied to this data can be summarised as
%
\begin{equation}
  p(\mu) = \Norm(\mu|0, 1),
  \qquad
  p(y_n|\mu) =  \Norm(y_n|\mu, 1) \quad \text{i.i.d.\ for all } n.
\end{equation}

Figure~\ref{fig:lw-estimator} shows the corresponding histogram of the
estimator in Equation~\ref{eq:lw-estimator}.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{hist-lw-estimator.png}
    }
    \caption{}
    \label{fig:lw-estimator}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{hist-lw-log.png}
    }
    \caption{}
    \label{fig:lw-log-estimator}
  \end{subfigure}
  \caption[Likelihood weighted prior sampling]{%
    (\subref{fig:lw-estimator}) shows a normalized histogram with 1000 bins of
    the evidence estimated by directly averaging the likelihood over $M=10$
    independent prior samples.
    (\subref{fig:lw-log-estimator}) shows the same histogram for the log of the
    estimator.
    The dashed orange lines are the exact evidence.
  }
\end{figure}
%
The exact evidence for this model is of the order of $10^{-64}$, an
extremely small number,\footnote{%
  To put that into perspective, the radius of a proton measured in parsecs is
  $2.84\times10^{-32}$ and the mass of an electron measured in solar masses is
  $4.58\times10^{-61}$ (according to Wolfram Alpha).
}
but the estimators tend to be so much smaller than even this value,
that they default numerically to an exact zero as illustrated in 
Figure~\ref{fig:lw-estimator}.


Evidences and their estimators tend to be extremely small numbers. Since this is
the case we will usually be interested in log-evidence or log-evidence per data
point rather than the evidence itself.
%
This, however, introduces a new bias, since unbiased evidence-estimators result
in biased log-evidence estimators. Bias may not matter if the estimator is
consistent and we are only interested in evidence ratios (Bayes Factors) rather
than absolute values. However, for use in weighted model combination, we would
typically prefer unbiased evidence estimators, not unbiased estimators of the
logarithm.

The histogram in Figure~\ref{fig:lw-estimator} is for the simple estimator in
Equation~\ref{eq:lw-estimator} using only $M=10$ samples. Naturally we expect
that the accuracy will improve if we increase the number of samples; however,
the distribution of this estimator is so skewed that the central limit theorem
approximation will be poor until $M$ is extremely large.
%
The above example is possibly one of the simplest models of all, and 100 data
points is not a large number by modern standards, so naturally if simple prior
sampling with Equation~\ref{eq:lw-estimator} is inaccurate for this model, we
cannot expect it to work well on complex models with many parameters and for
many data points.

\subsection{Harmonic Mean}

No discussion of evidence estimators would be complete without mentioning the
harmonic mean estimator.
%
The idea behind estimating evidence with harmonic averaging is based on the
following equation
%
\begin{equation}
  \E[p(\theta|\data)]{\frac{1}{p(\data|\theta)}} = 
  \int \frac{1}{p(\data|\theta)}\frac{p(\data|\theta)p(\theta)}{\Z} \, d\theta =
  \frac{1}{\Z}.
\end{equation}
%
This equation only applies when the posterior has the same support as the prior
and therefore requires that the likelihood be non-zero everywhere.
%
This equation suggests the following estimator
%
\begin{equation}
  \hat{\Z}_{\mathrm{HME}} := \left(
    \frac{1}{M} \sum_{i=1}^M \frac{1}{p(\data|\theta_i)}
  \right)^{-1},
\end{equation}
%
where $\theta_i$ are drawn from the posterior distribution, probably using some
MCMC method.
%
This estimator is biased; its reciprocal is an unbiased estimator for the
reciprocal of the evidence.

In practice it tends to be very inaccurate and often has infinite variance.
%
For this reason it has been criticized as the ``worst Monte Carlo algorithm
ever''~\citep{hme-worst}.
%
The main flaw of the harmonic mean estimator can be put down to the fact
that the samples are drawn from the posterior, which is insensitive to the prior
and therefore also the evidence.

A histogram of the evidence estimates produced by the harmonic mean by exactly
sampling the posterior of the Gaussian--Gaussian model can be seen in
Figure~\ref{fig:hme}.
%
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{hist-hme.png}
    }
    \caption{}
    \label{fig:hme}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{hist-hme-log.png}
    }
    \caption{}
    \label{fig:hme-reciprocal}
  \end{subfigure}
  \caption[Hamonic mean estimator histogram]{%
    (\subref{fig:hme}) shows a normalized histogram with 1000 bins of the
    harmonic mean estimator for the Gaussian--Gaussian model using $M=10$
    posterior samples.
    (\subref{fig:hme-reciprocal}) shows the same histogram for the log of
    the harmonic mean estimator.
    The dashed orange lines are the exact evidence.
  }
\end{figure}
%
For this simple example the harmonic mean typically overestimates the evidence
by at least two orders of magnitude.
%
This histogram was generated by exactly sampling the posterior; however, for
realistic models this is usually not possible and instead MCMC algorithms are
used to generate approximate samples.
%
Since this results in samples which are approximate and correlated, one would
expect that the accuracy of the harmonic mean estimator will be made worse
by doing this.
%
MCMC algorithms tend to have difficulty moving between modes; therefore
if MCMC is used to approximately sample from a multi-modal posterior then the
distribution of the harmonic mean estimator can change drastically.
%
This tends to be less of a problem for other evidence estimators but can be
fatal for the harmonic mean.

\subsection{Sandwiching and Uncertainty Estimation}

Evidence estimators more generally tend to consistently underestimate or
consistently overestimate~\citep{sandwich}.
%
Evidence estimators are positive real random variables and so we can apply
Markov's inequality
%
\begin{equation}
  \mathrm{Pr}\left[\hat{\Z} \ge \E{\hat{\Z}}\,e^{\alpha}\right] = 
  \mathrm{Pr}\left[\log\hat{\Z} \ge \log\E{\hat{\Z}} + \alpha\right]
  \le e^{-\alpha}.
\end{equation}
%
If the estimator is unbiased then we can be certain that it will not
overestimate the exact evidence by more than a few nats.
%
The same inequality can be applied to the reciprocal of the harmonic mean
estimator to show that it will almost never underestimate the exact evidence by
more than a few nats (assuming exact posterior sampling is used).
%
For unbiased estimators, the bulk of the distribution lying far below the exact
evidence means that the estimator must have a large variance.
%
Furthermore, since the distributions of unbiased evidence estimators tend to be
skewed in this way, estimating the evidence many times and then calculating
the sample variance tends to give no indication of the accuracy of the
estimator and only an indication of the typical values that the estimator
produces which may be very different from the exact evidence.

\citet{sandwich} propose an approach called bidirectional Monte Carlo, which
produces two evidence estimators which can be based on either annealed
importance sampling or sequential Monte Carlo.
%
The first is unbiased and so is very unlikely to overestimate.
%
They call this estimator a stochastic lower bound.
%
The second estimator is biased and based on reversing annealed importance
sampling or a harmonic mean variant of sequential Monte Carlo.
%
The second estimator is very unlikely to underestimate, and so they call it a
stochastic upper bound.
%
The difference between these two estimators gives a fair and prudent
approximation of their accuracy.

Unfortunately the stochastic upper bound requires an exact posterior sample in
order to theoretically guarantee a low probability of underestimating.
%
This means that it cannot be used to quantify accuracy on most realistic models
with real data.
%
However it can be used on simulated data sets.
%
If one samples the parameters from the prior, and then samples data points using
these parameters, the initially sampled parameters will be an exact posterior
sample given the generated data.

\citet{sandwich} recommend this as a method of testing the typical accuracy of
other evidence estimators by comparing their estimates to the bidirectional
Monte Carlo bounds on artificially generated data for models of interest.
%
Unfortunately, if the generated data is radically different from data being
modeled, then the accuracy of the estimator of interest may be much lower on the
real data than on the simulated data.
%
This approach has been successfully applied to variational autoencoders
\citep{inference-suboptimality}.
