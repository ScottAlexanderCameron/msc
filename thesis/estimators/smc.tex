\section{Sequential Monte Carlo}
\label{sec:smc}

The framework of sequential Monte Carlo~(SMC) extends the basic ideas from the
previous section and as such AIS falls into this class.
%
Many SMC algorithms are conventionally called particle filters and are
specifically targeted towards online inference applications.
%
Although the framework of SMC applies equally well to many different model
classes, these algorithms are often used in latent variable models with an
inherent dynamical structure.
%
Many of these models make a Markov assumption about the latent variables $\{z_n\}$ and
factorize the joint distribution
%
\begin{equation}
  p(\{y_n\}, \{z_n\}, \theta) = p(\theta)\prod_n
  p(y_n|z_n,\theta)p(z_n|z_{n-1},\theta),
\end{equation}
%
The key difference between the
model parameters $\theta$ and the latent variables is that the number of
parameters stays constant as the data set size increases, while the number of
latent variables grows with the number of data points.
%
The distribution of the latent variables and the parameters can both be inferred
through Bayes' theorem, so from a purely probabilistic perspective there is no
fundamental difference in the way they are treated.
%
NS and AIS do not assume any specific structure of the likelihood function and
so they can trivially be used with these kinds of models by augmenting
the parameter space with the latent variables.
%
However, in an online context where new observations are periodically added to
the data set, NS and AIS have no way to incorporate the extra dimensionality
required to include the new observations and must perform the entire calculation
again from scratch.

Particle filtering algorithms are particularly convenient for these kinds of
scenarios because they naturally incorporate the dynamics of the model.
%
The general framework of SMC, although specifically aimed at these sorts of
problems, works equally well for parametric models which do not have latent
variables.
%
In this work we will mainly consider the case where there are no latent
variables thus avoiding many of the intricacies that arise in latent variable
models.
%
For an in-depth introduction to SMC, see \citet{smc} and \citet{smc-practice}.

As in AIS, the key idea behind other SMC algorithms is to use a sequence of
intermediate distributions to transition from a simple distribution to a complex
target distribution which is of interest.
%
The choice of sequence may be guided by the structure of the
problem at hand or chosen simply because it is convenient.
%
For AIS, the sequence of distributions is chosen to be a geometric interpolation
of the simple and the desired distribution.
%
In SMC terminology, this is referred to as thermal tempering.
%
For physics simulations, the intermediate distributions have some physical
meaning but for Bayesian inference they are not necessarily relevant to the
problem.


Another common sequence of distributions is given by Bayesian updating. 
%
Assuming conditionally independent data, this can be written
%
\begin{equation}
  f_n(\theta) = p(\theta) \prod_{k\le n} p(y_k|\theta),
\end{equation}
%
or
%
\begin{equation}
  f_n(\theta, z_{1:n}) = p(\theta) \prod_{k\le n}
  p(y_k|z_k,\theta)p(z_k|z_{k-1},\theta),
\end{equation}
%
for Markov latent variable models.
%
Such updating of distributions is particularly meaningful in Bayesian inference
problems and enables online estimation.
%
In SMC terminology, this approach is called data tempering.
%
Although similar in nature to thermal tempering, data tempering limits the
extent to which the intermittent distributions could be chosen adaptively since
the Bayesian updating process is inherently discrete.

Data tempering corresponds to calculating (potentially correlated) importance
sampling estimates of predictive distributions $p(y_n|y_{<n})$ and factoring the
evidence as
%
\begin{equation}
  \Z = \prod_n p(y_n|y_{<n}).
\end{equation}
%
One potential benefit of this formulation is that it reduces the difficult
problem of estimating one integral of an extremely peaked function to the
problem of estimating many integrals of smoother functions.
%
%
Note that simply multiplying correlated estimates of predictive probabilities
does not necessarily result in an unbiased evidence estimator.
%
The sequential importance sampling and sequential importance resampling evidence
estimators described below are unbiased, however our approach in
Section~\ref{sec:seq} will not be.

Bayesian updating can be done one, or many observations at a time. 
%
One
observation at a time is typically more computationally intensive since more
operations will need to be done per data point and we cannot easily take
advantage of data parallelism; however, Bayesian updating many observations at a
time can result in a higher variance of the resulting estimators,
since the distributions in the sequence will become more dissimilar.

Sequential importance sampling is typically used for posterior inference and
evidence estimation in the context of latent
variable models with data tempering, in which case each new latent variable is
sampled from some importance distribution $q_n$ which may depend on all the
previous latent variables and observations.
%
The unnormalized importance weights may be calculated for the
$i$\textsuperscript{th} particle as
%
\begin{equation}
  w_i^{(n)} = w_i^{(n-1)} \,
  \frac{p(y_n|z_n^{(i)})\,p(z_n^{(i)}|z_{n-1}^{(i)})}{q_n(z_n^{(i)})}.
\end{equation}
%
Here we have suppressed dependence on the model parameters for readability.
%
The posterior expectation of a test function $h$ can be approximated using the
samples weighed by the normalized importance weights
%
\begin{equation}
  \frac{\sum_i h(\theta_i,z_{1:N}^{(i)}) \, w_i^{(N)}}{\sum_i w_i^{(N)}},
\end{equation}
%
and the evidence can be approximated by the mean of the importance weights
%
\begin{equation}
  \hat\Z := \frac{1}{M} \sum_i w_i^{(N)}
\end{equation}

Many generalizations of this idea exist, depending on the kind of model and the
dependencies between the latent variables. In any of these cases, the choice of
importance distribution $q_n$ plays a critical role in the quality of the
estimator.
%
While a simple implementation might use the prior transition distribution
$q_n(z_n)=p(z_n|z_{n-1})$, this may result in high variance. 
%
In a more sophisticated approach, $q_n$ could be parameterized and optimized
variationally, as done in variational sequential Monte Carlo~\citep{vsmc}.

One problem that typically arises with sequential importance sampling is 
degeneracy in the importance weights. 
%
This happens when the normalized importance weight for one particle is
approximately one while the rest are zero. This results in an effective sample
size of one and high variance of the Monte Carlo estimator.
%
In an attempt to reduce this degeneracy, the particles can at each step be
resampled with replacement from the current live particles with probability
proportional to their importance weights.
%
Resampling in this way results in particles which are distributed according to
the posterior transition distribution $p(z_n|z_{n-1},y_{\le n})$ in the limit of
an infinite number of particles, but does not always improve the result when
there are a small number of particles.
%
Furthermore, while resampling reduces weight degeneracy, it instead results in
particle degeneracy, since it kills off some of the live particles while
duplicating others.
%
To some extent this can be combated by applying a Markov transition to the
resampled particles to allow them to move around; however, resampling can
still result in mode collapse and so it should be used with care.
%
The resulting algorithm is usually called sequential importance
resampling~(SIR).

For parametric models without latent variables, we can apply SIR with data
tempering by initially sampling $M$ particles from the prior and then, for each
observation, calculating the importance weights corresponding to Bayesian
updating, i.e.\ the ratio of the relative posterior to the relative prior,
resampling the particles proportionally to these importance weights and then
applying a Markov kernel which leaves the current distribution invariant.
%
The importance weights at each step have a particularly attractive
form,\footnote{The importance weights do not need to be defined recursively for
SIR because the particles are weighted equally after resampling.}
%
\begin{equation}
  w_i^{(n)} = p(y_n|\theta_i^{(n-1)}).
\end{equation}
%
Each importance weight update only depends on the current data point $y_n$, and
does not require iterating over the entire data set. 
%
The particles are resampled proportionally to $w_i^{(n)}$ after which each
particle is updated through a Markov transition which has $p(\theta|y_{\le n})$
as its stationary distribution.
%
We can estimate the posterior predictive densities for each observation
%
\begin{equation}
  \hat{p}(y_n|y_{<n}) = \frac{1}{M} \sum_i w_i^{(n)},
\end{equation}
%
the product of which can be used to estimate the evidence
%
\begin{equation}
  \hat\Z := \prod_n \hat{p}(y_n|y_{<n}).
\end{equation}
%
This estimator is unbiased \citep{smc}.

Instead of resampling the particles after each observation, one can specify some
resampling criterion and only resample when this criterion is met. 
%
One possible criterion could be to only resample after the ESS drops below some
user-specified target.
%
In this case the importance weights for each observation are cumulatively
multiplied between resampling steps.

For an HMC kernel, the particles might be sampled approximately from the
$n$\textsuperscript{th} tempered distribution using the following potential
energy function:
%
\begin{equation}
  \label{eq:sir-energy}
  U_n(\theta) = -\sum_{k\le n} \log{p}(y_k|\theta) - \log{p}(\theta)
\end{equation}
%
SIR with HMC for models without latent variables is summarized in
Algorithm~\ref{alg:sir}.
%
We leave open the choice of resampling criterion.
%
\begin{algorithm}[htb]
  \caption{Sequential Importance Resampling}
  \label{alg:sir}
  \textbf{Input:} Data $\data=\{y_n\}_{n=1}^N$, number of particles $M$,
  pdfs $p(y|\theta),p(\theta)$\\
  \textbf{Output:} $\hat{\Z}$ evidence estimator
  \begin{algorithmic}[1]
    \State $\forall_i$: sample $\theta_i \sim p(\theta)$
    \State $\forall_i$: $w_i \gets 1$
    \For{$n = 1,\ldots,N$}
      \State $\forall_i$: $w_i \gets w_i \, p(y_n|\theta_i)$
      \If{resampling criterion met}
        \State resample $\theta_i$ proportionally to $w_i$
        \State $\forall_i$: $w_i \gets \frac{1}{M} \sum_j w_j$
      \EndIf
      \State $\forall_i$: $\theta_i \gets \Call{Hmc}{\theta_i, U_n}$
      \Comment{$U_n$ defined in Equation~\ref{eq:sir-energy}}
    \EndFor
    \State \Return $\hat{\Z} = \frac{1}{M}\sum_i w_i$
  \end{algorithmic}
\end{algorithm}

