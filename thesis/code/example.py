#! /usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rng
import scipy.stats as stats

plt.rcParams.update({'font.size': 13, 'figure.autolayout': True})
rng.seed(1234)

data = rng.normal(2, 1, size=100)


def logmeanexp(x, axis=0):
    logmax = np.max(x, axis=axis)
    return np.log(np.mean(np.exp(
        x - np.expand_dims(logmax, axis=axis)),
        axis=axis)) + logmax


def true_logz():
    N = len(data)
    mean = np.mean(data)
    mean_sqr = np.mean(data * data)
    logz = -1/2 * N * mean_sqr + 1/2 * (N*mean)**2 / (N+1)
    logz += -N/2 * np.log(2*np.pi) - 1/2 * np.log(N + 1)
    return logz


def log_like(mu):
    log_p = stats.norm.logpdf(data, np.expand_dims(mu, axis=-1), 1)
    return np.sum(log_p, axis=-1).reshape(mu.shape)


def prior(mu):
    return stats.norm.pdf(mu, 0, 1)


def post_mean_std():
    N = len(data)
    sum_x = np.sum(data)
    mean = sum_x / (N + 1)
    var = 1/(N + 1)
    return mean, np.sqrt(var)


def plot_prior_like():
    mu = np.linspace(-5, 5, 1000)
    fig, ax1 = plt.subplots()
    ax1.set_xlabel(r'$\mu$')
    ax1.set_ylabel(r'$p(\mu)$', color='blue')
    ax1.plot(mu, prior(mu), color='blue')
    ax1.tick_params(axis='y', labelcolor='blue')
    ax1.set_ylim(bottom=0)

    like = np.exp(log_like(mu))
    ax2 = ax1.twinx()
    ax2.set_ylabel(r'$p(\mathcal{D}|\mu)$', color='green')
    ax2.plot(mu, like, color='green')
    ax2.fill_between(mu, 0, like, color='green', alpha=0.3)
    ax2.tick_params(axis='y', labelcolor='green')
    ax2.set_ylim(bottom=0)
    plt.savefig('images/ex-like-prior.png', dpi=300)
    plt.close(fig)


def hist_lwe(T=1000, M=10):
    z = []
    for i in range(1000):
        mu = rng.normal(0, 1, size=(T, M))
        like = log_like(mu)
        z += list(logmeanexp(like, axis=1))
        if i % 100 == 0:
            print(i)
    logz = true_logz()
    true_z = np.exp(logz)

    z = np.array(z)

    fig = plt.figure()
    plt.xlabel(r'$\hat{\mathcal{Z}}$')
    bins, _, _ = plt.hist(np.exp(z), bins=250, density=True)

    plt.plot([true_z, true_z], [0, np.max(bins)], linestyle='--')
    plt.savefig('images/hist-lw-estimator.png', dpi=300)
    plt.close(fig)

    fig = plt.figure()
    plt.xlabel(r'$\log\hat{\mathcal{Z}}$')
    bins, _, _ = plt.hist(z, bins=250, density=True)

    plt.plot([logz, logz], [0, np.max(bins)], linestyle='--')
    plt.savefig('images/hist-lw-log.png', dpi=300)
    plt.close(fig)


def hist_hme(T=1000, M=10):
    mean, std = post_mean_std()
    z = []
    for i in range(1000):
        mu = rng.normal(mean, std, size=(T, M))
        like = log_like(mu)
        z += list(-logmeanexp(-like, axis=1))
        if i % 100 == 0:
            print(i)
    logz = true_logz()
    true_z = np.exp(logz)

    z = np.array(z)

    fig = plt.figure()
    plt.xlabel(r'$\hat{\mathcal{Z}}$')
    bins, _, _ = plt.hist(np.exp(z), bins=1000, density=True)

    plt.plot([true_z, true_z], [0, np.max(bins)], linestyle='--')
    plt.savefig('images/hist-hme.png', dpi=300)
    plt.close(fig)

    fig = plt.figure()
    plt.xlabel(r'$\log\hat{\mathcal{Z}}$')
    bins, _, _ = plt.hist(z, bins=1000, density=True)

    plt.plot([logz, logz], [0, np.max(bins)], linestyle='--')
    plt.savefig('images/hist-hme-log.png', dpi=300)
    plt.close(fig)


#plot_prior_like()
#hist_lwe()
hist_hme()
