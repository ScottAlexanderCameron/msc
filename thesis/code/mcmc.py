import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn

from torch.distributions import Normal
from torch.optim import SGD

plt.rcParams.update({'font.size': 12, 'figure.autolayout': True})


class P(nn.Module):
    def forward(self, x):
        return Normal(torch.zeros_like(x), torch.tensor([1, 0.3], dtype=x.dtype)).log_prob(x).sum(dim=-1)


def rwmh(x0, T, eps=0.1):
    logp = P()
    xs = [x0.numpy()] * T
    x = x0

    for i in range(1, T):
        xnew = torch.normal(x, eps)
        if torch.rand(1).log() < logp(xnew) - logp(x):
            x = xnew
        xs[i] = x.numpy()

    return np.array(xs)


def hmc(x0, T, eps=0.02, L=10):
    logp = P()
    xs = [x0.numpy()] * T
    x = x0

    for i in range(1, T):
        v = torch.randn_like(x0)
        vnew = v
        xnew = nn.Parameter(x.clone())
        U = -logp(xnew)
        U.backward()
        # half step
        vnew -= eps/2 * xnew.grad
        for t in range(L):
            xnew.data.add_(eps,  vnew)
            xnew.grad.zero_()
            if t < L - 1:
                Unew = -logp(xnew)
                Unew.backward()
                vnew -= eps * xnew.grad
        # half step
        Unew = -logp(xnew)
        Unew.backward()
        vnew -= eps/2 * xnew.grad

        Enew = Unew + (vnew * vnew).sum()/2
        Eold = U + (v * v).sum()/2

        if torch.rand(1).log() < Eold - Enew:
            x = xnew.data.detach()
        xs[i] = x.numpy()

    return np.array(xs)


def sgld(x0, T, lr=0.02):
    return sghmc(x0, T, lr, alpha=1)


def sghmc(x0, T, lr=0.02, alpha=0.2):
    logp = P()
    xs = [x0.numpy()] * T
    x = nn.Parameter(x0.clone())
    sgd = SGD([x], lr=lr, momentum=1 - alpha)

    for i in range(1, T):
        sgd.zero_grad()

        loss = -logp(x) + (x * torch.normal(torch.zeros_like(x), np.sqrt(2 * alpha / lr))).sum()
        loss.backward()

        sgd.step()

        xs[i] = x.detach().clone().numpy()

    return np.array(xs)


def plot_traj(xs, fname):
    X, Y = np.meshgrid(np.linspace(-3, 3, 1000), np.linspace(-2.5, 4.5, 1000))
    Z = P()(torch.tensor(
        np.concatenate((X.reshape(-1, 1), Y.reshape(-1, 1)), axis=1))) \
        .exp() \
        .numpy() \
        .reshape(X.shape)

    fig = plt.figure()
    plt.contourf(X, Y, Z, cmap=plt.cm.PuBu_r)
    plt.plot(xs[:, 0], xs[:, 1], 'r--')
    plt.savefig(f'{fname}.png', dpi=300)
    plt.close(fig)


def main():
    x0 = torch.tensor([1., 4.])
    T = 150

    torch.manual_seed(1234)
    xs = rwmh(x0, T)
    plot_traj(xs, 'rwmh')

    torch.manual_seed(1234)
    xs = hmc(x0, T)
    plot_traj(xs, 'hmc')

    torch.manual_seed(1234)
    xs = sgld(x0, T)
    plot_traj(xs, 'sgld')

    torch.manual_seed(1234)
    xs = sghmc(x0, T)
    plot_traj(xs, 'sghmc')


if __name__ == '__main__':
    main()
