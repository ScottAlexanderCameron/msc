\section{Stochastic Gradient Markov Chain Monte Carlo}
\label{sec:sghmc}
 
Just as HMC is inspired by Hamiltonian dynamics, we can develop stochastic
gradient MCMC~(SG-MCMC) algorithms taking inspiration from Langevin dynamics.
%
Both first order and second order Langevin dynamics admit the Boltzmann
distribution as a stationary distribution under certain conditions, so we can
simulate either dynamics to produce samples from a desired distribution by
setting $U(x) = -\log{p}(x)$.
%
First order dynamics will then converge to $p(x)$ if a diffusion constant of one
is used, and second order dynamics will converge to $p(x)$ if the diffusion
matrix is equal to the friction coefficient.
%
We can use SG-MCMC algorithms for efficient large-scale Bayesian inference by
estimating the potential energy using mini-batches sampled uniformly from the
data set
%
\begin{equation}
  \hat{U}(\theta) := -\frac{|\data|}{|B|}\sum_{y\in B} \log{p}(y|\theta) -
  \log{p}(\theta).
\end{equation}
%
with $|\data|$ and $|B|$ the number of samples in the entire data set
and the batch respectively. 
%
For mini-batches of sufficient size, the gradients of the potential energy estimate
will be approximately Gaussian distributed.

As with HMC, we need an integrator to simulate the dynamics. HMC relies on
leapfrog integration to preserve certain favourable properties and on MH
rejection steps to correct for discretization error.
%
Small step sizes for HMC result in fewer rejections, but result in more
likelihood gradient calculations for the same distance travelled.
%
In this case the MH corrections still guarantee convergence even for large step
sizes.
%
For SG-MCMC algorithms, we can use the Euler--Maruyama
integration scheme \citep[chapter~13]{numeric-sde}. For an SDE of the form
%
\begin{equation}
  dx = \mu(x)\,dt + \sigma(x)\,dW(t),
\end{equation}
%
the Euler--Maruyama integrator with step-size $\epsilon$ performs the 
update rule
%
\begin{equation}
  x_t = x_{t-1} + \mu(x_{t-1})\,\epsilon + \sigma(x_{t-1})\,\xi_t,
\end{equation}
%
where $\xi_t$ is a normally distributed random vector with variance $\epsilon$.
%
Discretization error will still be a concern, however we can control for it by
carefully decreasing the step size.
%
With the use of mini-batches, estimating likelihood gradients is computationally
inexpensive in comparison to an MH correction which requires iterating over the
entire data set.
%
Therefore using a very small step size does not result in computational
inefficiency as it would with HMC.

Euler--Maruyama integration with step size $\eta$ for first order Langevin
dynamics has the following update rule
%
\begin{equation}
  x_t = x_{t-1} - \eta \nabla U(x_{t-1}) + \xi_t, \qquad \xi_t \sim \Norm(0, 2\eta).
\end{equation}
%
This equation is the same as the update rule for (stochastic) gradient
descent, except for the added noise $\xi_t$.
%
Therefore, following common practice in machine learning, we call $\eta$ the
learning rate.
%
As it turns out, performing a single leapfrog step of HMC, without an MH
correction is equivalent to the following update step
%
\begin{equation}
  x_t = x_{t-1} - \frac{\epsilon^2}{2}\nabla U(x_{t-1}) + \epsilon\,v,
  \qquad v \sim \Norm(0, 1).
\end{equation}
%
These updates are identical and we can identify $\eta = \frac{1}{2}\epsilon^2$. 
For this reason HMC with a single leapfrog step is also called the Metropolis
adjusted Langevin algorithm~(MALA).

Although we can calculate the MH acceptance probability based on a single
leapfrog step if we have access to the exact gradients, it is no longer possible
if we use stochastic approximations of the gradients based on mini-batches,
since we can no longer calculate the probability of the reverse transition.
%
The variance of the gradient term is proportional to $\eta^2$, while the
variance of the injection noise $\xi$ is proportional to $\eta$.
%
Therefore using a small learning rate results in the gradient noise being
dominated by the injection noise, and so it can typically be ignored for a small
enough learning rate.
%
We can also account for the gradient noise by estimating its variance and
subtracting that estimate from the variance of the injection noise.

To guarantee convergence we can use a learning rate schedule following the
Robbins--Monro conditions \citep{sgld}
%
\begin{equation}
  \sum_{t=1}^\infty \eta_{t} = \infty,
  \qquad
  \mbox{and}
  \qquad
  \sum_{t=1}^\infty \eta_{t}^2 < \infty;
\end{equation}
%
however, using a small learning rate $\eta\sim\bigO{\frac{1}{|\data|}}$ is
usually sufficient in practice.

Simulating first order Langevin dynamics with stochastic gradient estimates is
called stochastic gradient Langevin dynamics~(SGLD) and was introduced
by~\citet{sgld}. SGLD is summarized in Algorithm~\ref{alg:sgld}.
%
The parameter $\hat\beta$ in Algorithm~\ref{alg:sgld} is to control for the
stochastic gradient variance. It is meant to be an estimate of the variance of
the stochastic gradients, which may be calculated adaptively or user-specified.
%
\begin{algorithm}[htbp]
  \caption{Stochastic Gradient Langevin Dynamics}
  \label{alg:sgld}
  \textbf{Input:} unbiased potential energy estimate $\hat{U}(x)$, learning rate
  $\eta$, noise estimate $\hat\beta$, initial state $x_0$ \\
  \textbf{Output:} samples $x_{1:M}$
  \begin{algorithmic}[1]
    \For{$i = 1,\cdots,M$}
    \State sample $\xi \sim \Norm(0, 2(1-\hat\beta\eta)\eta)$
    \State $x_i \gets x_i - \eta \nabla\hat{U}(x_i) + \xi$
    \EndFor
  \end{algorithmic}
\end{algorithm}

One potential downside of SGLD is that it no longer uses the auxiliary momenta
which were introduced in HMC, because it is based on first order Langevin
dynamics, which corresponds to the infinite friction limit.
%
While following gradients converges far quicker than a simple random walk, SGLD
still tends to behave similarly to a random walk once it has converged,
resulting in high autocorrelation of samples.
%
We can reintroduce momentum with second order Langevin simulations to avoid this
behaviour.

Euler--Maruyama discretization for second order Langevin dynamics gives the
update rule
%
\begin{align}
  \xi_t &\sim \Norm(0, 2\gamma\epsilon), \\
  v_t &= v_{t-1} - \epsilon\,\gamma\,v_{t-1} - \epsilon\,\nabla U(x_{t-1}) +
  \xi_t,\\
  x_t &= x_{t-1} + \epsilon\,v_{t}.
\end{align}
%
The resulting algorithm is called stochastic gradient Hamiltonian Monte
Carlo (SGHMC) and is summarized in Algorithm~\ref{alg:sghmc}. SGHMC was
introduced by \citet{sghmc}.
%
To guarantee convergence we can again use the Robbins--Monro step-size
conditions for $\epsilon$.
%
Since $v$ is an auxiliary variable and is not needed for MH corrections as it
is in HMC, it is somewhat neater to relabel $v\gets \epsilon\,v$, and redefine the
learning rate $\eta = \epsilon^2$ and momentum decay $\alpha =
\gamma\,\epsilon$.
%
\begin{align}
  \xi_t &\sim \Norm(0, 2\alpha\eta), \\
  v_t &= (1 - \alpha) v_{t-1} - \eta \nabla U(x_{t-1}) + \xi_t, \\
  x_t &= x_{t-1} + v_t.
\end{align}
%
The above update rule is exactly the update rule for (stochastic) gradient descent
with momentum \citep{sutskever,sghmc}, except for the injection noise $\xi_t$.
%
One benefit of this is that SGLD and SGHMC can be used with existing stochastic
gradient descent implementations just by adding $x_t\cdot\xi_t$ to the loss
function (potential energy).
%
Setting $\alpha$ to one gives the same update equation as SGLD, i.e. the
infinite friction limit.

Just as with SGLD, the parameter $\hat\beta$ in Algorithm~\ref{alg:sghmc} 
controls the stochastic gradient variance. It is meant to be an estimate of
the variance of the stochastic gradients, which may be calculated adaptively or
user-specified.
%
We note that here $\hat\beta$ is used slightly differently than in the original
version of SGHMC. In our case, $\hat\beta$ is meant to be an estimate of $\Var{\nabla
U}$, while \citet{sghmc} introduce it such that $\hat\beta\eta$ is an estimate
of $\Var{\eta\nabla U}$.
%
\begin{algorithm}[htbp]
  \caption{Stochastic Gradient Hamiltonian Monte Carlo}
  \label{alg:sghmc}
  \textbf{Input:} unbiased potential energy estimate $\hat{U}(x)$, learning rate
  $\eta$, momentum decay $\alpha$, noise estimate $\hat\beta$, initial state
  $x_0$ \\
  \textbf{Output:} samples $x_{1:M}$
  \begin{algorithmic}[1]
    \State sample $v \sim \Norm(0,\eta)$
    \For{$i = 1,\cdots,M$}
    \State optionally resample momenta $v \sim \Norm(0, \eta)$
    \State sample $\xi \sim \Norm(0, 2(\alpha - \hat\beta\eta)\eta)$
    \State $v \gets v - \alpha v - \eta \nabla\hat{U}(x_i) + \xi$
    \State $x_i \gets x_i + v$
    \EndFor
  \end{algorithmic}
\end{algorithm}
%
Similarly to HMC, various extensions to SGHMC exist, such as Riemannian manifold
SGHMC which adaptively estimates the local potential energy curvature and
stochastic gradient Nos\'e--Hoover dynamics which couples the system to an
external heat source to improve convergence~\citep{sgrhmc}.

\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{sgld.png}
    }
    \caption{}
    \label{fig:sgld}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \ifoptiondraft{}{
      \includegraphics[width=\textwidth]{sghmc.png}
    }
    \caption{}
    \label{fig:sghmc}
  \end{subfigure}
  \caption[Comparison of SGLD to SGHMC]{%
    (\subref{fig:sgld}) shows a trajectory generated by stochastic gradient
    Langevin dynamics (Algorithm~\ref{alg:sgld}) and (\subref{fig:sghmc}) shows a
    trajectory generated by stochastic gradient Hamiltonian Monte Carlo
    (Algorithm~\ref{alg:sghmc}).
  }
  \label{fig:sgld-sghmc-comp}
\end{figure}
%
Figure~\ref{fig:sgld-sghmc-comp} shows a comparison of trajectories generated
with SGLD and SGHMC.
%
Since SGLD uses gradients, it quickly descends to the minimum of the potential
energy, after which it diffuses around exhibiting behaviour similar to a random
walk.
%
SGHMC on the other hand uses momentum, and so it has smoother trajectories and
travels further distances, covering larger areas of the distribution.
%
See \citet{sghmc, bohamiann, sgrhmc} for a more in-depth analysis of SGHMC and
its parameters.
