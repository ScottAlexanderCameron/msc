\section{Hamiltonian Dynamics}
\label{sec:hamiltonian}

The random walk proposal distribution of Algorithm~\ref{alg:rwmh}
results in slow convergence to the stationary distribution due to high
correlations between successive samples $x_i$.
%
Furthermore, this high autocorrelation implies that each new sample contains
little new information, even after the chain has converged to the desired
stationary distribution after the so-called burn-in phase.
%
In order to combat this, more sophisticated MCMC kernels need to be introduced.
%
This section will attempt to shed some light on Hamiltonian dynamics with the
purpose of using Hamiltonian simulations as MCMC proposals.
%
A reader with a strong background in physics may safely skip this
section and move directly to Section~\ref{sec:hmc}

The formalism of Hamiltonian dynamics describes the physics of classical
objects.
%
Hamiltonian dynamics is equivalent to Newtonian mechanics if there are no
dissipative forces; however, it is a far more elegant construction, naturally
describing conservation laws, in more general coordinate systems than just
Euclidean.
%
The full machinery of Hamiltonian dynamics and its construction in classical
mechanics is beyond the scope of this thesis; we will merely attempt to describe the
basics of Hamiltonian dynamics and highlight some important properties.
%
See \citet{mechanics} for a rigorous mathematical introduction to the topic.

We may parameterize the configuration of a classical system by coordinates
$x_i$.
%
Classical systems have associated conjugate momenta, which we will denote here
as $v_i$.
%
These are not necessarily velocities, however momenta are dual to velocities and
so this notation is not completely inappropriate.
%
We choose $x$ and $v$ here instead of the more conventional $q$ and $p$ in order
to avoid confusion with probabilities.
%
Together, the coordinates and momenta uniquely and completely specify the state
of the system.
%
The joint space of coordinates and momenta is called the
phase space.\footnote{The phase space is the cotangent bundle of the
configuration manifold.}
%
Classical systems have an associated Hamiltonian function $H(\{x_i\}, \{v_i\})$
which is the generator of the dynamics of the system.
%
Hamilton's equations of motion describe the system's trajectory through
phase space given some initial conditions. 
%
Hamilton's equations of motion are
%
\begin{equation}
  \label{eq:motion-equations}
  \frac{dx_i}{dt} = \frac{\partial H}{\partial v_i}
  \qquad \mbox{and} \qquad
  \frac{dv_i}{dt} = -\frac{\partial H}{\partial x_i}.
\end{equation}
%
The Hamiltonian is the total energy of the system, which can often be described
as the sum of kinetic and potential energies which depend solely on $x$ and on
$v$ respectively,
%
\begin{equation}
  H(x,v,t) = K(v) + U(x,t).
\end{equation}
%
For the usual quadratic kinetic energy $K(v) = \frac{1}{2m} v^2$,
these equations of motion reduce to Newton's second law
%
\begin{equation}
  m \frac{d^2x}{dt^2} = - \nabla U(x,t).
\end{equation}
% 
If the potential is time-independent, $U(x,t) = U(x)$, the Hamiltonian
does not depend explicitly on time and thereby becomes a constant of motion,
%
\begin{align}
  \frac{dH}{dt} &= \sum_i \left(
    \frac{\partial H}{\partial x_i}\frac{dx_i}{dt}
    +
    \frac{\partial H}{\partial v_i}\frac{dv_i}{dt}
  \right) + \frac{\partial H}{\partial t}\\
  &= \sum_i \left(
    \frac{\partial H}{\partial x_i}\frac{\partial H}{\partial v_i}
    -
    \frac{\partial H}{\partial v_i}\frac{\partial H}{\partial x_i}
  \right) + 0\\
  &= 0.
\end{align}
%
This is the first important property of Hamiltonian dynamics. For use in MCMC,
exact conservation in time of the Hamiltonian will result in a\linebreak
Metropolis--Hastings acceptance probability of one.
%
Since dynamics are simulated numerically, the Hamiltonian will not be
conserved exactly; however, the discrepancy will typically be small.

A second property of Hamiltonian dynamics is that it conserves
volumes in phase space.
%
The conservation of volume means that the Jacobian determinant of transforming a
phase space point through Hamiltonian dynamics for any amount of time is one.
%
This property, known as \textit{Liouville's theorem} in the context of
Hamiltonian dynamics \citep[chapter 3]{mechanics}, is a fundamental theorem in
classical mechanics and statistical physics.
% 
In the context of the Metropolis--Hastings algorithm it is also an
important property.
%
Since Hamiltonian dynamics is deterministic, the conditional probability of
moving from one phase space configuration to another is given by a Dirac delta
function, multiplied with a Jacobian factor.
%
Since we already know that the Jacobian is unity, this greatly simplifies
calculation of the MH acceptance ratio.


Thirdly, if the kinetic energy is symmetric, $K(v) = K(-v)$, then the dynamics
can be reversed just by negating the momentum.
%
Assuming that the system is initially in state $(x, v)$, and evolves under
Hamilton's equations of motion for some time to arrive at $(x', v')$, then it
can be shown that starting with initial condition $(x', -v')$ and evolving the
system for the same amount of time, the system would arrive at $(x, -v)$.
%
This can be seen by noting that a simultaneous time reversal $t\to-t$ and parity
transformation $v\to-v$ leaves Equation~\ref{eq:motion-equations} invariant.


Hamilton's equations of motion define a flow through phase space which transports
configurations of the system continuously.
%
We may imagine an ensemble of systems or, phrased differently, a distribution
over possible initial conditions, which follow these dynamics.
%
We would expect to be able to calculate the dynamics of the distribution of
this ensemble of systems over time from these equations of motion.
%
It turns out that the distribution of configurations of a system whose dynamics
are given by an ordinary differential equation follows a partial differential
equation called \textit{Liouville's equation}, which is closely related to
Liouville's theorem.
%
We will not give a formal proof here, but describe the form of the equation
intuitively as follows.
%
Consider a system which follows dynamics governed by a first-order ordinary
differential equation in terms of a velocity vector field $b(x,t)$,
%
\begin{equation}
  \frac{dx}{dt} = b(x, t).
\end{equation}
%
Higher-order ODEs can be written in this form by transforming them into coupled
first-order ODEs.
%
Let $p(x; t)$ be a distribution over configurations of the system at time $t$.
This can be thought of as a particle density if the particles are not
interacting, or as a probability density in $x$ describing our uncertainty over
the current configuration.
%
There is a probability/particle \emph{current} which is the product of the
density and the velocity field
%
\begin{equation}
  j := p(x; t)\, \frac{dx}{dt} = p(x; t)\, b(x, t).
\end{equation}
%
The current is a vector field which gives the net density and direction of
the flow of probability or particles away from each point in the space.
%
The density and current naturally satisfy a continuity equation 
%
\begin{equation}
  \label{eq:liouville}
  \frac{\partial p}{\partial t} = - \nabla\cdot j = - \nabla\cdot (p\,b).
\end{equation}
%
This continuity equation is Liouville's equation.
%
Intuitively, it states that the rate at which the density increases at any point
is equal to the net rate of particles entering that point minus the rate of
particles leaving that point.
%
Continuity equations arise whenever systems exhibit continuous motion or
continuous symmetries.
%
They are a kind of conservation law, in this case conservation of number of
particles or conservation of total probability.
%
Noether's theorem usually manifests itself as a continuity equation.
%
For Hamilton's equations of motions, by substituting
Equation~\ref{eq:motion-equations} into Equation~\ref{eq:liouville}, one arrives
at Liouville's equation for Hamiltonian dynamics
%
\begin{equation}
  \frac{\partial p}{\partial t} = 
  \nabla_v\cdot(p\nabla_xH) - \nabla_x\cdot(p\nabla_vH).
\end{equation}
%
It is easily shown by simple substitution that it yields a family of stationary
solutions in the form of a \textit{Boltzmann distribution}
%
\begin{equation}
  p(x,v) = \frac{1}{Z} e^{-\beta H(x,v)}
\end{equation}
%
where the normalization constant $Z$ is called the partition function. The
positive constant $\beta$ is a ``Lagrange multiplier'' whose value depends on
the system constraints.

In physics, $\beta$ is interpreted as an inverse temperature and the ensemble of
points $(x,v)$ governed by Hamilton's equations and the Boltzmann distribution
is known as the \emph{canonical ensemble}. In the context of MCMC,
these need not have a physical interpretation at all, however it is still a
useful analogy.

Since Hamiltonian dynamics conserves the Hamiltonian, any initial energy
distribution is also stationary, and so closed Hamiltonian systems do not
necessarily converge to the canonical ensemble.
%
However the system can converge to the canonical ensemble if energy is allowed
to enter and exit the system through a so called ``heat bath''.
%
If the energy entering and exiting the system is correctly controlled then the
system may exhibit a unique stationary distribution with a well defined
temperature.

For the separable Hamiltonian $H = K(v) + U(x)$, the positions and momenta become
statistically independent with marginals given by
%
\begin{equation}
  p(x) = \frac{1}{Z_x} e^{-\beta U(x)},
  \qquad 
  \mbox{and}
  \qquad 
  p(v) = \frac{1}{Z_v} e^{-\beta K(v)}.
\end{equation}
