\section{Markov Chain Monte Carlo}

\subsection{Basic Principals of MCMC}
\subsection{Hamiltonian Monte Carlo}

The random walk proposal of Algorithm~\ref{alg:rwmh} results in slow convergence
to the stationary distribution due to high autocorrelation.
%
Furthermore, the high autocorrelation results in each new sample containing
almost no new information, even after the chain has converged to the desired
distribution.
%
In order to combat this, more sophisticated MCMC kernels need to be introduced.
%
This property is what Radford Neal calls `slow mixing' in motivation for the use
of Hamiltonian Monte Carlo, sometimes called hybrid Monte
Carlo~\citep[chapter~5]{mcmc-handbook}.\todo{this needs a reword}
%
Hamiltonian Monte Carlo~(HMC) is considered the gold standard of MCMC algorithms
due to its fast convergence and scalability to large and complex models.
%
The theory behind HMC is based on Hamiltonian dynamics of a point particle with
position $x$ and momenta $v$.\footnote{More common notation is to use $q$ for
position and $p$ for momenta. We use an alternate notation to avoid confusion
with probabilities.}
%
The position of the particle will represent the random variable which we want to
sample, and the momenta are auxiliary variables which allow the system to
exhibit faster `mixing'.
%
HMC satisfies detailed balance and is reversible.
%
The Hamiltonian is given by the total energy of the system $H = K(v) + U(x)$, and the
equations of motion are
%
\begin{equation*}
  \frac{dx}{dt} = \nabla_v H, \qquad
  \frac{dv}{dt} = -\nabla_x H.
\end{equation*}
%
For dynamics described by ordinary differential equations, such Hamilton's
equations of motion, the probability density of the state of the particle at a
given time is determined by a partial differential equation called Liouville's
equation\footnote{Liouville's theorem is a fundamental theorem in classical
mechanics and builds the foundation of statistical mechanics.}
%
\begin{equation*}
  \frac{\partial p(x,v; t)}{\partial t} =
  \nabla_v\cdot(p\nabla_xH) - \nabla_x\cdot(p\nabla_vH).
\end{equation*}
%
A stationary solution to Liouville's equation is given by the Boltzmann
distribution for a system at equilibrium.
%
\begin{equation*}
  p_\infty(x,v) = \frac{1}{Z} e^{-\beta H(x,v)}.
\end{equation*}
%
Here $\beta$ is an arbitrary positive constant which can be interpreted as an
inverse temperature.
%
The most common scenario has that the Hamiltonian is a sum of the kinetic
energy, which only depends on momentum, and potential energy, which only depends
on position, in which case the above stationary distribution factorizes;
momentum and position become independent.
%
\begin{equation*}
  p_\infty(x,v) = \frac{1}{Z} e^{-\beta K(v)}e^{-\beta U(x)}.
\end{equation*}

Hamiltonian dynamics, without any external influences, conserve energy.
%
This means that if we want to simulate Hamiltonian trajectories, unless we
occasionally add or remove energy from the system, some points of the phase
space will not be reached.
%
HMC allows the system to change energy by periodically resampling the momenta
from the corresponding marginal stationary distribution with temperature one
%
\begin{equation*}
  v \sim \frac{1}{Z_v}e^{-K(v)}.
\end{equation*}
%
The resampling of momenta allows one to control the temperature parameter.
%
Forgetting the values of the momenta when taking averages with respect to the
position effectively marginalizes out the momenta, yielding expectations with
respect to the marginal stationary distribution of the position.
%
\begin{equation*}
  \frac{1}{M} \sum_{i=1}^M h(x_i)
  \quad \xrightarrow{M\to\infty} \quad
  \frac{1}{Z_x}\int h(x) e^{-U(x)} \, dx.
\end{equation*}
%
From the above equation it can be seen that if we want to simulate from the
distribution $p(x)$ then a potential energy $U(x) = -\log{p}(x)$ should be used
for the simulation.
%
We still have a choice of kinetic energy. The independence between momentum and
position ensures that for any choice of kinetic energy, the dynamics will
converge to the desired distribution; however, the performance of the algorithm
will depend on the choice of kinetic energy.
%
The most common choice is a quadratic kinetic energy with a mass matrix $M$,
%
\begin{equation*}
  K(v) = \frac{1}{2} v^TM^{-1}v.
\end{equation*}
%
This corresponds to a Gaussian stationary distribution for the momenta.
%
The mass matrix is often chosen to be the identity matrix, but allowing it to be
non-diagonal can be beneficial when there are strong correlations between
variables.

Hamiltonian dynamics, with occasional momentum resampling yields a Markov chain
which converges to a desired stationary distribution specified by the potential
energy.
%
However, it is not possible to simulate the exact dynamics in most cases.
%
Instead the equations of motion need to be discretized and solved numerically.
%
HMC integrates the equations of motion using the leapfrog method due to its
favourable characteristics~\citep[see][chapter~5]{mcmc-handbook}.
%
Due to the numerical error in the discretization, energy in not conserved
exactly and so an MH-rejection step is usually done for each sample in order to
guarantee detailed balance, however for a small enough step size the HM step is
usually not necessary.
%
HMC is outlined in Algorithm~\ref{alg:hmc} with a quadratic kinetic energy and
an identity mass matrix.

\begin{algorithm}[htbp]
  \caption{Hamiltonian Monte Carlo}
  \label{alg:hmc}
  \textbf{Input:} potential energy $U(x)$, step size $\epsilon$, number
  of leapfrog steps $L$, initial state $x_0$ \\
  \textbf{Output:} samples $x_{1:M}$
  \begin{algorithmic}[1]
    \For{$i = 1,\cdots,M$}
      \State sample $v \sim \Norm(0,1)$
      \State set $x' \gets x_{i-1}$
      \State set $v' \gets v - \frac{\epsilon}{2}\nabla U(x)$\Comment{half step}
      \For{$t = 1,\cdots,L-1$}
        \State set $x' \gets x' + \epsilon v$
        \State set $v' \gets v' - \epsilon\nabla U(x)$
      \EndFor
      \State set $x' \gets x' + \epsilon v$
      \State set $v' \gets v' - \frac{\epsilon}{2}\nabla U(x)$\Comment{half step}
      \State sample $u \sim U(0,1)$
      \If{$\log(u) < H(x,v) - H(x',v')$}
        \Comment{HM-accept/reject}
        \State set $x_i \gets x'$
      \Else
        \State set $x_i \gets x_{i-1}$
      \EndIf
    \EndFor
  \end{algorithmic}
\end{algorithm}

In the case of posterior inference the negative log joint is used as a potential
energy $U(\theta) = -\log{p}(\data, \theta)$.
%
The dominant contribution to the computation time comes from the taking the
a sum of terms for each data point, both for the gradient calculation and,
potentially, for the MH-rejection step.
%
Unfortunately this mean that for large data sets HMC is usually too slow to be
practical.
