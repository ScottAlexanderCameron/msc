\section{Langevin Dynamics}
\label{sec:langevin}

The main difficulty with scaling Bayesian inference to large data sets using
HMC is the requirement of iterating through the whole data set every time the
potential energy, or its gradient, is required. 
%
Maximum likelihood estimation and variational inference are still feasible on
large data sets with the help of stochastic optimization.
%
\citet{robbins-monro} presented a method for finding roots of a function
given only noisy, but unbiased, estimates of the function values.
%
Under certain conditions the algorithm can be guaranteed to converge. One of
these conditions is that the step sizes $\epsilon_t$ follow a sequence which
satisfies
%
\begin{equation}
  \sum_{t=1}^\infty \epsilon_t = \infty,
  \qquad
  \mbox{and}
  \qquad
  \sum_{t=1}^\infty \epsilon_t^2 < \infty.
\end{equation}
%
Informally the first equation states that the total simulation time should be
infinite, and therefore the root can be reached, no matter how far away from the
initial point, while the second equation states that the total variance due to
the noisy estimates (which are multiplied by the step sizes) should be finite.
%
When such a step size sequence is applied to gradient descent or ascent, which
corresponds to finding roots of the gradient, with an
unbiased estimator of the gradients, then the algorithm will converge almost
surely to a local optimum.
%
This means that maximizing the likelihood, or maximizing the evidence
lower bound in variational inference, can be done efficiently by performing
gradient ascent using small sub-samples (mini-batches) of the data set instead
of iterating over the entire data set.
%
Stochastic gradient descent on a loss function using mini-batch gradient
estimates is valid when the mini-batch estimates are unbiased.
%
Using mini-batches like this can greatly reduce the convergence time of the
algorithms.


Unfortunately naively using stochastic likelihood estimates in HMC violates the
basic assumptions of the theory: 
%
If the potential energy function changes over time (whether stochastically or
deterministically) then the Hamiltonian is no longer conserved.
%
This can result in samples from a distribution which very different to the
Boltzmann distribution.
%
See \citet{sghmc} for an example of this.
%
In order to introduce stochastic gradients in HMC, we must first consider how
stochastic forces influence a classical physical system.

Complementing the usual deterministic forces in a classical system with
stochastic forces results in what is known as Langevin dynamics
\citep[chapter~9]{stochastic-phys-chem}.
% 
The most fundamental and simplest such system is the Brownian particle.  The
$d$-dimensional Brownian motion, or Wiener process
\citep[chapter~2]{numeric-sde}, is a time-homogeneous Markov process with
finite-time transition kernel
%
\begin{equation}
  p(x_{t+\tau}|x_{t};t,\tau) = \frac{1}{\left(\sqrt{2\pi \tau}\right)^d}
  \exp\left(-\frac{(x_{t+\tau} - x_{t})^2}{2\tau}\right),
\end{equation}
%
which is the solution to an isotropic, translation invariant diffusion
equation
%
\begin{equation}
  \frac{\partial p}{\partial t} = \frac{1}{2} \nabla^2 p
\end{equation}
%
with initial condition $p(x'|x;t,\tau=0) = \delta(x' - x)$.
%
The diffusion equation is again a kind of continuity equation with probability
current $j = -\frac{1}{2}\nabla p$.
%
This continuity equation arises because Brownian motion is almost surely
continuous.
%
This current can be interpreted with the following argument.
Imagine an infinitesimal (hyper-)cube.
%
Particles flow isotropically in every direction, and therefore any particle on
the surface of the cube has exactly one half probability of entering the cube,
and one half probability of leaving the cube.
%
The net flow of particles through the surface will be one half times the
difference in number of particles on the outside surface and the number of
particles on the inside surface.
%
By taking the volume to be infinitesimally small, the net density of particles
flowing through the volume will tend to exactly half of the gradient of the
particle density, in the direction of steepest descent.

If the motion is not isotropic or homogeneous, then we may locally transform to
a coordinate system in which the motion is isotropic, calculate the current in
that coordinate system and then transform back.
%
By doing the reverse transformation, the current picks up two Jacobian factors,
one for the density and one for the gradient, the outer product of which is a
symmetric positive definite matrix which becomes the \emph{diffusion matrix}.
%
Letting the diffusion matrix absorb the factor $\frac{1}{2}$, the resulting
probability current is $j = -\nabla (p(x)\,D(x))$, where it is implied that the
differential operator matrix multiplies on the right with the diffusion matrix.
%
In other words, the components of the current are $j_i =
-\sum_{k}\frac{\partial}{\partial x_k} p(x)\,D_{i,k}(x)$.
%
This is the Fokker--Planck diffusion probability current and takes the
interpretation that that the inhomogeneity affects the process locally in a
certain way.
%
This is in contrast to Fick's first law \citep{nonlinear-diffusion} for
diffusion in inhomogeneous media which gives a probability current
$j = -D(x)\nabla p(x)$.
%
Which probability current to use depends on the microscopic details of the
diffusion.

The Wiener process is ubiquitous in science and has many interesting properties.
%
For example, paths which are realizations of the Wiener process are everywhere continuous
with probability one, but they are nowhere differentiable.
%
As such, the derivative of a Wiener process, which is continuous-time white
noise, is not well defined in the sense of functions; however, much like the
derivative of the Dirac delta, it can be defined as a linear functional and treated
as a density due to the Riesz--Markov--Kakutani representation
theorem \cite[chapters 2 and 6]{rudin-analysis}.
%
The Wiener process is also self-similar, and exhibits the scaling law $W(\alpha
t) \cong \sqrt{\alpha}\,W(t)$.\footnote{The congruency here means that this
transformation is an isomorphism; in this case isomorphism would mean
distribution preserving.}
%
Scaling laws of this form are often studied in
statistical physics and tend to only exist for simple functions which are power
laws or for very complex non-differentiable functions such as the Wiener process
or the Weierstrass function.
%
This kind of self-similarity gives rise to fractal behaviour and has interesting
implications in the study of complex systems, particularly near phase changes.

Langevin dynamics%
%
\footnote{%
  Here we describe Langevin dynamics with a potential energy. Sometimes the term
  `Langevin dynamics' is used to refer to the case where the potential is zero,
  and it is usually implied to be the first order variant when not otherwise
  specified.
}
%
describe classical particles in a medium by extending the
forces found in Hamiltonian dynamics to include a dissipative friction
force, due to the medium, and a stochastic force, which represents the
individual molecules in the medium bumping the larger particle.
%
The stochastic force is assumed to be white noise because the time-scales of the
movement of the particle are typically much larger than the time-scales of its
interaction with the molecules in the medium.
%
In the informal notation used by physicists, we may write the second order Langevin
equation with unit mass as
%
\begin{equation}
  \frac{d^2x}{dt^2} = 
  \overbrace{-\;\nabla U(x)}^{\smbox{conservative force}}
  \underbrace{-\; \gamma(x) \frac{dx}{dt}}_{\smbox{friction}} + 
  \overbrace{\sqrt{2D(x)}\,\xi(t)}^{\smbox{stochastic force}},
\end{equation}
%
where $\gamma(x)$ is the friction coefficient which is either a positive scalar
or a positive semi-definite matrix, and $D(x)$ is a symmetric positive semi-definite
diffusion matrix; the square root is the local coordinate transformation whose
inverse diagonalizes the stochastic force. The random variable
$\xi(t)$ follows a white noise distribution
%
\begin{equation}
  \E{\xi(t)} = 0,
  \qquad
  \mbox{and}
  \qquad
  \E{\xi(t')\xi(t)} = \delta(t' - t).
\end{equation}
%
If the medium is homogeneous then $\gamma$ and $D$ will be constants and, unless
there is some external influence on the medium such as electromagnetic forces,
the system will be isotropic and so $\gamma$ and $D$ will be scalars.
%
If the system is over-damped ($\gamma\to\infty$), the effective dynamics reduces
to the first order Langevin equation
%
\begin{equation}
  \frac{dx}{dt} = -\nabla U(x) + \sqrt{2D(x)}\,\xi(t).
\end{equation}
%
The diffusion matrix here is not necessarily equal to the diffusion
matrix appearing in the second order equation.
%
The typical behaviour of these dynamics is to initially descend to the minimum
of the potential energy, and then slowly diffuse around near the minimum.

A more formal treatment relies on the theory of stochastic differential
equations~(SDEs). The Langevin equations can be written somewhat more formally
as SDEs in the It\^o%
%
\footnote{Unlike Riemann integration, stochastic integration depends strongly on
how the discretization is done. The two most prominent variants are the It\^o
and Stratonovich formulations \citep[chapters 7 and 8]{numeric-sde}}
%
interpretation where the driving stochastic process is a Wiener
process. Expectations of quantities depending on the realization of the SDE can
be written as Wiener integrals, in which case they are usually calculated
perturbatively.
%
As before with Liouville's equation and the diffusion equation, we can write
down a partial differential equation for the probability density based on the
probability current.
%
For an It\^o SDE driven by a Wiener process,
%
\begin{equation}
  \label{eq:sde}
  dx =
  \underbrace{\mu(x, t)\,dt}_{\smbox{drift}}
  +
  \underbrace{\sigma(x, t)\,dW(t)}_{\smbox{diffusion}},
\end{equation}
%
the probability density $p(x;t)$ evolves according to the Fokker--Planck equation
%
\begin{equation}
  \frac{\partial p}{\partial t} =
  \underbrace{- \nabla\cdot(p\,\mu)}_{\smbox{drift}}
  +
  \overbrace{\sum_{i,j} \frac{\partial^2}{\partial x_i\partial x_j}
  \left(p\,D_{i,j}\right)}^{\smbox{diffusion}},
\end{equation}
%
where the diffusion matrix is $D = \frac{1}{2} \sigma\sigma^T$.
%
The Fokker--Planck equation is also sometimes called the (forward) Kolmogorov
equation, although the Kolmogorov equation may also contain terms corresponding
to jump processes.
%
If the driving stochastic process is not Gaussian, then there exist
generalizations to the above equation involving higher order derivatives.
%
Note how the drift term in the Fokker--Planck equation corresponds to
Liouville's equation for the ODE that results in Equation~\ref{eq:sde} by
setting $\sigma$ to zero, and the diffusion term corresponds to the diffusion
equation which results when there is no drift velocity.
%
The probability current can be written exactly as
the sum of the drift current and the (not necessarily homogeneous or isotropic)
diffusion current $j = j_{\,\smbox{drift}} + j_{\,\smbox{diff}}$.

For homogeneous and isotropic diffusion ($D$ is a scalar constant), first order
Langevin dynamics in a potential written as an It\^o SDE is
%
\begin{equation}
  dx = -\nabla U(x) dt + \sqrt{2D}\,dW(t),
\end{equation}
%
with the corresponding Fokker--Planck equation
%
\begin{equation}
  \frac{\partial p}{\partial t} = \nabla\cdot(p\,\nabla U(x)) + D\nabla^2 p.
\end{equation}
%
The Fokker--Planck equation for first order Langevin dynamics admits a unique
stationary solution
%
\begin{equation}
  p(x) = \frac{1}{Z} e^{-\beta U(x)},
\end{equation}
%
where the temperature is given by the diffusion constant $\beta = 1/D$.
%
This stationary distribution is exactly the position marginal of the canonical
ensemble.
%
One interesting consequence of this is that the temperature of the particles can
be controlled by increasing or decreasing the magnitude of the stochastic force.
At equilibrium, the temperature of the medium is the same as the temperature of
the Brownian particles, so the variance of the stochastic force can be directly
interpreted as the temperature of the medium.

For second order Langevin dynamics, we will assume that the diffusion matrix
does not depend on velocity, but may depend on position and that it is a
constant scalar multiple of the friction.
%
This assumption is for mathematical convenience.
%
This is trivially the case for the most common scenario where both the diffusion
and friction coefficients are constant and scalars.
%
The It\^o SDE is 
%
\begin{align}
  dx &= v \, dt, \\
  dv &= -\nabla U(x) \, dt - \gamma(x) v \, dt + \sqrt{2\gamma(x)T} \, dW(t),
\end{align}
%
where $T$ is the multiplicative constant.
The corresponding Fokker--Planck equation can be written compactly in matrix
form~\citep{sghmc}
%
\begin{equation}
  \frac{\partial p}{\partial t} =
  \nabla_{x,v}^T \left\{
    [A + B]p\,\nabla_{x,v}H
    +
    B\,T \nabla_{x,v}p
  \right\}
\end{equation}
%
where $H = U(x) + \frac{1}{2} v^2$, and $A$ and $B$ are the matrices
%
\begin{equation}
  A = \left[
    \begin{array}{cc}
      0 & -I \\ I & 0
    \end{array}
  \right]
  \qquad\mbox{and}\qquad
  B = \left[
    \begin{array}{cc}
      0 & 0 \\ 0 & \gamma(x)
    \end{array}
  \right].
\end{equation}
%
Noting that $\nabla_{x,v}^TA\nabla_{x,v}p = \nabla_x^T\nabla_vp -
\nabla_v^T\nabla_x p = 0$, the Fokker--Planck equation can be written
equivalently as
%
\begin{equation}
  \frac{\partial p}{\partial t} =
  \nabla_{x,v}^T [A + B]\left\{
    p\,\nabla_{x,v}H
    +
    T \nabla_{x,v}p
  \right\}.
\end{equation}
%
This equation again admits the Boltzmann distribution as the unique stationary
solution
%
\begin{equation}
  p(x,v) = \frac{1}{Z} e^{-\beta H(x,v)},
\end{equation}
%
where $\beta = 1/T = \gamma(x)/D(x)$.

Similarly to Hamiltonian dynamics, Langevin dynamics exhibits a kind of
reversibility. This reversibility is of the form
%
\begin{equation}
  p(x',v'|x,v; t) = p(x,-v|x',-v'; t).
\end{equation}
%
This can be shown by viewing the right hand side of the Fokker--Planck equation
as an operator acting on a Hilbert space and finding the adjoint
operator~\citep{sghmc}.
