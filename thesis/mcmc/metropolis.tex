\section{Metropolis--Hastings}

The basic principle of any Monte Carlo technique is to use random sampling
to estimate quantities of interest described as expectations.
%
Given any function $h(x)$ of any variable $x$, the most basic Monte Carlo
algorithm would estimate a quantity of interest
%
\begin{equation}
  H := \E[p(x)]{h(x)},
\end{equation}
%
by generating $M$ independent samples $x_i$ directly from the probability
distribution $p(x)$ and calculating the estimator
%
\begin{equation}
  \hat{H} := \frac{1}{M} \sum_{i=1}^M h(x_i),
  \qquad
  x_i \overset{\smbox{i.i.d.}}{\sim} p(x).
\end{equation}
%
Unfortunately, for many quantities of interest, the distribution $p$ is
difficult, or impossible to sample directly.
%
A common example occurring in Bayesian inference is the estimation of posterior
predictive distributions $p(y'|\data)$ by
sampling from the posterior $p(\theta|\data)$.
%
Since the posterior distribution of interesting models is usually quite complex,
generating samples $\theta_i$ directly from it is not feasible.
%
MCMC presents a practical solution to the problem of estimating expectations
over complex distributions by simulating an ergodic\footnote{The ergodicity is
required to guarantee convergence.} Markov chain which has $p(x)$ as its
stationary distribution~\citep[chapter~1]{mcmc-handbook}.
%
Assume a transition kernel $k(x'|x)$ leaves $p(x)$ invariant
%
\begin{equation}
  \int k(x'|x)p(x)\,dx = p(x'),
\end{equation}
%
then by the law of large numbers, averages taken over the realization of the
Markov chain will converge to the expectation under the stationary distribution;
%
\begin{equation}
  \frac{1}{M} \sum_{i=1}^M h(x_i) \quad \xrightarrow{M\to\infty} \quad
  \E[p(x)]{h(x)},
\end{equation}
%
when $x_i$ is sampled from $k$ based at $x_{i-1}$
%
\begin{equation}
  x_i \sim k(\,\cdot\,|x_{i-1}).
\end{equation}
% 
Many MCMC algorithms rely on detailed balance, $k(x'|x)p(x) = k(x|x')p(x')$, to
ensure that they have the desired stationary distribution. One simple way to
ensure this is to propose a new state $x'$ by sampling from an arbitrary
proposal distribution $q(x'|x)$ which has unbounded support, and accept the new
state with probability
%
\begin{equation}
  \label{eq:mh}
  \min\left\{1, \frac{p(x')q(x|x')}{p(x)q(x'|x)}\right\},
\end{equation}
%
and otherwise reject $x'$ and keep $x$ as the new state. This is called the
Metropolis--Hastings~(MH) rejection step.
%
If the proposal distribution is symmetric then the acceptance probability
reduces to $\min\{1, p(x')/p(x)\}$.
%
One benefit of MH based algorithms is that they do not require knowledge of the
normalization constant of the stationary distribution.
%
This allows these algorithms to be used for Bayesian inference by just
specifying the joint distribution $p(\data,\theta) = p(\data|\theta)p(\theta)$,
and for equilibrium physics simulations by using the Boltzmann
factor $e^{-H/k_BT}$.
%
One of the simplest MCMC algorithms based on this is random-walk
Metropolis--Hastings which uses a Gaussian proposal distribution centered around
the current point $x_{i-1}$.
%
Random walk Metropolis--Hastings as described in Algorithm~\ref{alg:rwmh}
is easy to implement correctly and does not require any other user
input so it can be useful for simple experiments.
%
\begin{algorithm}[htbp]
  \caption{Random Walk Metropolis--Hastings}
  \label{alg:rwmh}
  \textbf{Input:} stationary distribution $p(x)$, step size $\epsilon$, initial
  state $x_0$\\
  \textbf{Output:} samples $x_{1:M} := \{x_1,x_2,\ldots,x_M\}$
  \begin{algorithmic}[1]
    \For{$i = 1,\ldots,M$}
    \State sample $x' \sim \Norm(x_{i-1},\epsilon)$
      \State sample $u \sim U(0,1)$ \Comment $U$ is the uniform distribution
      \If{$u < \frac{p(x')}{p(x)}$}
        \Comment{The proposal distribution is symmetric}
        \State set $x_i \gets x'$
      \Else
        \State set $x_i \gets x_{i-1}$
      \EndIf
    \EndFor
  \end{algorithmic}
\end{algorithm}

