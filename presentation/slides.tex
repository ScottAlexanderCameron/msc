\documentclass[
%draft,
10pt
]{beamer}

%\usepackage{amsmath}
\usepackage{ifdraft}
\usetheme[progressbar=frametitle]{metropolis}
\newcommand{\data}{{\mathcal{D}}}
\newcommand{\Z}{{\mathcal{Z}}}
\newcommand{\M}{{\mathcal{M}}}
\newcommand{\bigO}[1]{{\mathcal{O}\left(#1\right)}}


\title{Stochastic Gradient Annealed Importance Sampling}
%\subtitle{NITheP Workshop}
\date{}
\author{Scott Cameron\\ Hans Eggers\\ Steve Kroon}
\institute{Stellenbosch University\\ NITheP}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}
\maketitle

\begin{frame}{Motivation}
  \begin{itemize}
    \item Recent developments in SG-MCMC and SVI have enabled large-scale
      Bayesian inference
      \vfill

    \item Despite this, accurate ML estimation on large data sets has still been
      largely unattainable
      \vfill

    \item Our goal is to provide an efficient method for estimating ML
      accurately on large data set
  \end{itemize}
\end{frame}

\begin{frame}[t]{Marginal Likelihood (Evidence)}
  Consider a Bayesian model 
  \begin{equation*}
    \data = \{y_n\}_{n=1}^N
    \qquad
    p(\data,\theta) = p(\theta) \prod_{n}p(y_n|\theta)
  \end{equation*}

  Posterior given by Bayes theorem
  \begin{equation*}
    p(\theta|\data) = \frac{p(\data|\theta) p(\theta)}{p(\data)}
  \end{equation*}
  
  Marginal likelihood
  \begin{equation*}
    \Z := p(\data) = \int p(\data|\theta) p(\theta)\,d\theta
  \end{equation*}

  Posterior predictive
  \begin{equation*}
    p(y'|\data) = \int p(y'|\theta)p(\theta|\data)\,d\theta
  \end{equation*}
\end{frame}

\begin{frame}[t]{Model Comparison/Combination}
  Posterior over models $\M_1,\M_2,\cdots$
  \begin{equation*}
    \frac{P(\M_1|\data)}{P(\M_2|\data)} =
    \frac{\Z_1}{\Z_2} \frac{p(\M_1)}{p(\M_2)}
  \end{equation*}
  \begin{center}
    $\M_1$ is a `better' model than $\M_2$ if $\Z_1 \gg \Z_2$
  \end{center}

  \ifoptionfinal{
    \pause
  }{}
  \vfill

  Combined predictions
  \begin{equation*}
    p(y'|\data) = \frac{\sum_i p(y'|\data,\M_i) \Z_i\,p(\M_i)}
    {\sum_i \Z_i\,p(\M_i)}
  \end{equation*}
  \begin{center}
    Weighs models proportionately to how well they describe data
  \end{center}
\end{frame}

\begin{frame}[t]{Why is this difficult?}
  Simple model
  \begin{equation*}
    \mu\sim\mathcal{N}(0,1) \qquad y_n\sim\mathcal{N}(\mu,1)
  \end{equation*}
  Naive estimator
  \begin{equation*}
    \hat\Z = \frac{1}{M} \sum_{i=1}^M p(\data|\mu_i)
    \qquad
    \mu_i \sim p(\mu)
  \end{equation*}

  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../thesis/images/ex-like-prior.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../thesis/images/hist-lw-estimator.png}
  \end{minipage}
\end{frame}

\begin{frame}[t]{Annealed Importance Sampling}
  Adiabatically decrease temperature: $0=\lambda_0<\cdots<\lambda_T=1$
  \begin{equation*}
    f_t(\theta) = p(\data|\theta)^{\lambda_t}p(\theta)
  \end{equation*}

  Update particles with HMC
  \begin{equation*}
    U_t(\theta) = -\lambda_t 
    \only<1>{\log{p}(\data|\theta)}
    \only<2>{\alert{\log{p}(\data|\theta)}}
    - \log{p}(\theta)
  \end{equation*}

  Iterated importance sampling
  \begin{equation*}
    w_i^{(t)} \gets w_i^{(t-1)}\,
    \only<1>{p(\data|\theta_i^{(t-1)})}
    \only<2>{\alert{p(\data|\theta_i^{(t-1)})}}
    ^{\lambda_t - \lambda_{t-1}}
  \end{equation*}

  Estimator
  \begin{equation*}
    \hat\Z = \frac{1}{M} \sum_{i=1}^M w_i^{(T)}
  \end{equation*}
\end{frame}

\begin{frame}{Problems with Scalability}
  Accurate estimates require $T \propto |\data|$
  \begin{enumerate}
      \vfill
    \item HMC needs likelihood gradients, $\bigO{|\data|}$
      \vfill
    \item Importance weights need likelihood, $\bigO{|\data|}$
      \vfill
  \end{enumerate}
  More or less $\bigO{|\data|^2}$ complexity
\end{frame}

\begin{frame}[t]{Stochastic Gradient HMC}
  Simulate Langevin dynamics
  \begin{equation*}
    \ddot\theta = -\nabla U(\theta) - \gamma \dot\theta + \sqrt{2\gamma}\,\xi
    \qquad
    \left\langle\xi(t)\xi(t')\right\rangle = \delta(t - t')
  \end{equation*}
  \vfill

  Fokker--Planck equation
  \begin{equation*}
    \frac{\partial p}{\partial t} = \partial^T A \left\{
      p\,\partial H + \partial p
    \right\}
    \qquad
    A = \left(
      \begin{array}{cc}
        0 & -I \\ I & \gamma
      \end{array}
    \right)
  \end{equation*}
  \vfill

  Canonical ensemble
  \begin{equation*}
    p_\infty(\theta, \dot\theta) = \frac{1}{Z} e^{-H(\theta,\dot\theta)}
  \end{equation*}
\end{frame}

\begin{frame}[t]{Stochastic Gradient HMC}
  Euler--Maruyama discretization
  \begin{align*}
    \Delta \theta &= v \\
    \Delta v &= - \alpha\,v - \eta\nabla\hat{U}(\theta)
    + \mathcal{N}(0, 2(\alpha - \hat\beta)\eta)
  \end{align*}
  \vfill

  Mini-batch energy estimate
  \begin{equation*}
    \hat{U}(\theta) = -\frac{|\data|}{|B|}\sum_{y\in B} \log{p}(y|\theta) -
    \log{p}(\theta)
  \end{equation*}
  \vfill

  Time complexity $\bigO{|B|} \ll \bigO{|\data|}$\\
  \pause
  \alert{solves (1)}
\end{frame}

\begin{frame}[t]{Bayesian Updating}
  Predictive distributions
  \begin{equation*}
    \Z = \prod_{n} p(y_n|y_{<n})
  \end{equation*}
  \begin{equation*}
    p(y_n|y_{<n}) = \int p(y_n|\theta) p(\theta|y_{<n})
  \end{equation*}

  \vfill
  \pause

  Estimate products of predictives $p(y_n|y_{<n})$
  \begin{align*}
    \theta_i^{(n-1)},\: w_i^{(n-1)} &\gets \operatorname{\textsc{Ais}}(y_{<n})\\
    \theta_i^{(n)},\: \tilde w_i^{(n)} &\gets \operatorname{\textsc{Ais}}(y_n,
    \theta_i^{(n-1)})
  \end{align*}
  \pause
  \begin{equation*}
    \hat{p}(y_{\le n}) =
    \frac{1}{M} \sum_{i=1}^M \tilde w_i^{(n)} \: w_i^{(n-1)}
  \end{equation*}
  \pause
  \alert{solves (2)}
\end{frame}

\begin{frame}[t]{Stochastic Gradient Annealed Importance sampling}
  Equivalent to
  \begin{equation*}
    f_n^{(\lambda)}(\theta) = p(y_n|\theta)^\lambda
    \left[\prod_{k<n}p(y_k|\theta)\right] p(\theta)
  \end{equation*}

  Update particles with SGHMC
  \begin{equation*}
    \hat{U}_n^{(\lambda)}(\theta) = -\lambda \log{p}(y_n|\theta)
    - \frac{n-1}{|B|}\sum_{y\in B}\log{p}(y|\theta)
    - \log{p}(\theta)
  \end{equation*}

  Importance weights
  \begin{equation*}
    w_i^{(t)} \gets w_i^{(t-1)}\,
    p(y_n|\theta_i^{(t-1)})^{\lambda_t - \lambda_{t-1}}
  \end{equation*}

  ML estimator
  \begin{equation*}
    \hat\Z = \frac{1}{M} \sum_{i=1}^M w_i^{(T)}
  \end{equation*}
\end{frame}

\begin{frame}[t]{Results}
  Gaussian mixture model
  \begin{itemize}
    \item vs nested sampling
    \item vs annealed importance sampling
  \end{itemize}
  \vfill
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/gmm-logz.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/gmm-time.png}
  \end{minipage}
\end{frame}

\begin{frame}[t]{Parameter sensitivity}
  Adaptive annealing schedule
  \begin{itemize}
    \item Blue $\approx$ no annealing steps
  \end{itemize}
  \vfill
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/gmm-sensitivity-ESS-logz.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/gmm-sensitivity-ESS-time.png}
  \end{minipage}
\end{frame}

\begin{frame}[t]{Parameter sensitivity}
  Adaptive annealing schedule
  \begin{itemize}
    \item Blue $\approx$ no annealing steps
  \end{itemize}
  \vfill
  \begin{center}
    \includegraphics[width=0.69\textwidth]{../experiments/figs/gmm-sensitivity-ESS-steps.png}
  \end{center}
\end{frame}

\begin{frame}[t]{Distribution Shift}
  Data may change over time
  \vfill

  \begin{minipage}{0.49\textwidth}
    \centering
    {\small $1 \le n \le10^3$}

    \includegraphics[width=0.7\textwidth]{../experiments/figs/dist-shift-1k.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \centering
    {\small $10^3 < n \le 10^4$}

    \includegraphics[width=0.7\textwidth]{../experiments/figs/dist-shift-9k.png}
  \end{minipage}
  \vfill
  \begin{minipage}{0.49\textwidth}
    \centering
    {\small $10^4 < n \le 10^5$}

    \includegraphics[width=0.7\textwidth]{../experiments/figs/dist-shift-90k.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \centering
    {\small total}

    \includegraphics[width=0.7\textwidth]{../experiments/figs/dist-shift-100k.png}
  \end{minipage}
\end{frame}

\begin{frame}[t]{Distribution Shift}
  Dashed lines = shuffled data
  \vfill

  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/dist-shift.png}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../experiments/figs/dist-shift-steps.png}
  \end{minipage}
\end{frame}

\begin{frame}{Takeaways}
  \begin{itemize}
      \vfill
    \item SGIAS enables efficient large-scale ML estimation
      \vfill
    \item Naturally applicable to online estimation and streaming data
      \vfill
    \item Can be used to identify shifts in the underlying data generating
      distribution
      \vfill
  \end{itemize}
\end{frame}
\end{document}
