from models.logistic import LogReg
from exp import Experiment


class LogExp(Experiment):
    def __init__(self, *,
                 data_size: int = 1_000_000,
                 n_iter=None):
        super().__init__('log', data_size=data_size, n_iter=n_iter)

    def model(self, n_samples=1):
        return LogReg(10, 4, n_samples=n_samples)


def main():
    exp = LogExp()
    exp.run()
    exp.plot()


if __name__ == '__main__':
    main()
