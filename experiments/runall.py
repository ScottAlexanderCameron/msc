import gmm
import linear
import logreg
import sensitivitytest


def main():
    gmm.main()
    linear.main()
    logreg.main()
    sensitivitytest.main()


if __name__ == '__main__':
    main()
