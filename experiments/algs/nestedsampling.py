import numpy as np
import torch

from algs.sghmc import ConstrainedSGHMC
from models.model import Model
from typing import List


class Particle(object):
    def __init__(self, log_like=None):
        self._log_like = log_like

    @property
    def log_likelihood(self):
        if self._log_like is None:
            raise ValueError('log_likelihood is None')
        return self._log_like

    @log_likelihood.setter
    def log_likelihood(self, log_like):
        if log_like is None:
            raise ValueError('cannot assign None to log_likelihood')
        self._log_like = log_like

    def explore(self, log_like) -> None:
        raise NotImplementedError

    def assign_from(self, copy) -> None:
        raise NotImplementedError


class NestedSampler(object):
    def __init__(self, particles: List[Particle]):
        self.particles = particles
        self.logZ = -np.inf
        self.logL = -np.inf
        self.logLmax = -np.inf
        self.logW = np.log(1.0 - np.exp(-1.0 / len(particles)))
        self.h = 0.0

    def step(self, closure=None) -> None:
        i, worst, self.logL = min(
            ((i, p, p.log_likelihood)
             for i, p in enumerate(self.particles)),
            key=lambda tup: tup[2])

        self.logLmax = max(p.log_likelihood for p in self.particles)

        log_weight = self.logL + self.logW
        logZnew = np.logaddexp(self.logZ, log_weight)

        h = np.exp(log_weight - logZnew) * self.logL - logZnew
        if self.logZ != -np.inf:
            h += np.exp(self.logZ - logZnew) * (self.h + self.logZ)

        self.logZ = logZnew
        self.h = h

        if closure is not None:
            closure(worst, self.logL, self.logW, self.logZ)

        if len(self.particles) > 1:
            self.particles[-1], self.particles[i] = \
                self.particles[i], self.particles[-1]
            copy = np.random.choice(self.particles[:-1])
            worst.assign_from(copy)

        worst.explore(self.logL)
        self.logW -= 1.0/len(self.particles)

    @property
    def uncertainty(self):
        return np.sqrt(self.h/len(self.particles))

    def run(self, precision: float = 1e-2, closure=None):
        steps = 0

        while float(self.logLmax + self.logW) >= float(self.logZ + np.log(precision)):
            self.step(closure)
            steps += 1

        return steps


class SGHMCParticle(Particle):
    def __init__(self, data, model: Model, *,
                 n_steps: int = 100,
                 lr: float = 1e-3,
                 alpha: float = 0.1):
        super().__init__()

        self.data = data
        self.model = model
        self.n_steps = n_steps
        self.sghmc = ConstrainedSGHMC(self.model.parameters(), lr=lr, alpha=alpha)

        with torch.no_grad():
            self.log_likelihood = self.log_like()

    def log_like(self):
        return self.model.log_p(self.data).sum()

    def energy(self):
        return -self.model.log_prior()

    def step_sampler(self, log_like):
        self.sghmc.step(self.energy, lambda: self.log_like() - log_like)

    def explore(self, log_like) -> None:
        self.sghmc.resample_momentum()

        for i in range(self.n_steps):
            self.step_sampler(log_like)

        with torch.no_grad():
            self.log_likelihood = self.log_like()

    def assign_from(self, copy) -> None:
        with torch.no_grad():
            for p, q in zip(self.model.parameters(), copy.model.parameters()):
                p.copy_(q)

