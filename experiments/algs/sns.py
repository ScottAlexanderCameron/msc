
import numpy as np
import torch.utils.data as dutils

from algs.nestedsampling import NestedSampler, SGHMCParticle
from models.model import Model
from typing import List


class SequentialSGHMCParticle(SGHMCParticle):
    def __init__(self, prev_data, next_data, model: Model, *,
                 batch_size: int = 500,
                 n_steps: int = 100,
                 lr: float = 1e-3,
                 alpha: float = 0.1):
        super().__init__(next_data, model, n_steps=n_steps,
                         lr=lr, alpha=alpha)
        self.batch_size = batch_size
        self.data_size = len(prev_data)
        self.prev_data = prev_data
        self.next_data = self.data  # SGHMCParticle.data

    def set_data(self, prev_data, next_data):
        self.data_size = len(prev_data)
        self.prev_data = prev_data
        self.next_data = self.data = next_data  # SGHMCParticle.data

    def step_sampler(self, log_like):
        loss_multiplier = self.data_size if self.data_size > 0 else 1

        self.sghmc.step(self.energy, lambda: self.log_like() - log_like,
                        loss_multiplier=loss_multiplier)

    def energy(self):
        if self.data_size == 0:
            return super().energy()

        batch = self.prev_data[
            np.random.choice(self.data_size, self.batch_size)]

        return -self.model.log_p(batch).mean() \
            - self.model.log_prior()/self.data_size

    def get_state(self):
        return self.model.state_dict()

    def set_state(self, state_dict):
        self.model.load_state_dict(state_dict)


class SequentialNestedSampler(NestedSampler):
    def __init__(self, data, particles: List[Model], *,
                 batch_size: int = 500,
                 n_steps: int = 100,
                 lr: float = 1e-3,
                 alpha: float = 0.1):
        self.data = data
        self.current_index = 0
        self.batch_size = batch_size
        self.reservoir = []
        self.prev_logZ = 0

        prev_data, next_data = self.prev_and_next_data()

        particles = [
            SequentialSGHMCParticle(
                prev_data, next_data, model,
                batch_size=batch_size,
                n_steps=n_steps,
                lr=lr,
                alpha=alpha
            ) for model in particles
        ]

        super().__init__(particles)

    def prev_and_next_data(self):
        prev_data = self.data[:self.current_index]
        next_data = self.data[self.current_index:
                              self.current_index + self.batch_size]

        if type(prev_data) is tuple:
            # for slicing
            prev_data = dutils.TensorDataset(*prev_data)
        return prev_data, next_data

    def step(self, closure=None):
        def wrapper(worst, logL, logW, logZ):
            """
                implements reservoir sampling so that particles can be sampled
                from the approximate posterior for the next run of nested
                sampling
            """
            if len(self.reservoir) < len(self.particles):
                self.reservoir.append(worst.get_state())
            else:
                p = (logW - logZ).exp()
                if np.random.uniform() < p:
                    replace = np.random.randint(len(self.reservoir))
                    self.reservoir[replace] = worst.get_state()
            if closure is not None:
                closure(worst, logL, logW, logZ)

        steps = 0
        # TODO where to put precision ?
        precision = 1e-2

        self.logZ = -np.inf
        self.logZ = -np.inf
        self.logL = -np.inf
        self.logLmax = -np.inf
        self.logW = np.log(1.0 - np.exp(-1.0 / len(self.particles)))
        self.h = 0.0

        while float(self.logLmax + self.logW) >= float(self.logZ + np.log(precision)):
            super().step(wrapper)
            # print(steps, f'logmax:{self.logLmax}, logw:{self.logW}, logz:{self.logZ}')
            steps += 1

        self.prev_logZ += self.logZ
        self.current_index += self.batch_size

        for r, p in zip(self.reservoir, self.particles):
            p.set_state(r)
            prev_data, next_data = self.prev_and_next_data()
            p.set_data(prev_data, next_data)

        return steps

    def run(self, closure=None, precision=1e-2):
        while self.current_index < len(self.data):
            n_steps = self.step(closure)
            yield (self.current_index, float(self.prev_logZ), n_steps)

