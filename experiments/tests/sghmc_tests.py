import matplotlib.pyplot as plt
import numpy as np
import torch

from algs.sghmc import SGHMC, ConstrainedSGHMC
from scipy.stats import norm


class Sampler(object):
    def __init__(self, loss_multiplier=1):
        self.x = torch.tensor(self.init_x(),
                              dtype=torch.float32,
                              requires_grad=True)
        self.sampler = SGHMC([self.x], lr=1e-3)
        self.loss_multiplier = loss_multiplier

    def init_x(self):
        return np.random.randn(1)

    def energy(self):
        raise NotImplementedError

    def pdf(self, x):
        raise NotImplementedError

    def step(self):
        self.sampler.step(self.energy, self.loss_multiplier)

    def plot(self, xs):
        plt.hist(xs, density=True, bins=1000)
        x = np.linspace(xs.min(), xs.max(), 1000)
        plt.plot(x, self.pdf(x))
        return x

    def run(self, show=True):
        xs = np.empty(100_000)

        for i in range(100_000):
            for _ in range(5):
                self.step()
            xs[i] = self.x.detach().numpy()

        self.plot(xs)
        if show:
            plt.show()


class ConstrainedSampler(Sampler):
    def __init__(self, *args):
        super().__init__(*args)
        self.sampler = ConstrainedSGHMC([self.x], lr=1e-3)

    def constraint(self):
        raise NotImplementedError

    def plot_constraint(self, x):
        pass

    def plot(self, xs):
        x = super().plot(xs)
        self.plot_constraint(x)

    def step(self):
        self.sampler.step(self.energy, self.constraint, self.loss_multiplier)


def norm_loss(x):
    return 0.5 * x * x


class LossMultiplierGaussian(Sampler):

    def energy(self):
        return norm_loss(self.x) / self.loss_multiplier

    def pdf(self, x):
        return norm.pdf(x)


def test1():
    for mul in (1, 10, 0.1):
        LossMultiplierGaussian(mul).run(show=False)
    plt.show()


class ConstrainedGaussian(ConstrainedSampler):
    def __init__(self):
        super().__init__()

    def init_x(self):
        return 0.5

    def energy(self):
        return norm_loss(self.x)

    def constraint(self):
        return self.x * (5 - self.x)

    def pdf(self, x):
        return 2 * norm.pdf(x)

    def plot_constraint(self, x):
        plt.plot([0, 0], [0, 2/np.sqrt(2 * np.pi)], 'r--')


def test2():
    ConstrainedGaussian().run()


class Triangle(ConstrainedSampler):
    def __init__(self):
        super().__init__()

    def init_x(self):
        return 0.5

    def energy(self):
        return -self.x.log()

    def constraint(self):
        return self.x * (2 - self.x)

    def pdf(self, x):
        return 0.5 * x * (x >= 0) * (x <= 2)

    def plot_constraint(self, x):
        plt.plot([0, 0], [0, 1], 'r--')
        plt.plot([2, 2], [0, 1], 'r--')


def test3():
    Triangle().run()


if __name__ == '__main__':
    # test1()
    # test2()
    test3()
