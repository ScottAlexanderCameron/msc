import numpy.random as rng
import torch
import torch.nn as nn

from models.model import Model
import torch.distributions as td


class GMM(Model):
    def __init__(self, *,
                 n_components: int,
                 dimensions: int,
                 n_samples: int = 1):
        super().__init__()

        self.n_components = n_components
        self.dimensions = dimensions
        self.stick_break = td.StickBreakingTransform()

        self.logits_prior = td.TransformedDistribution(
            td.Dirichlet(torch.ones(n_components, dtype=torch.double)),
            self.stick_break.inv)

        self.vars_prior = td.TransformedDistribution(
            td.Gamma(1, 1),
            td.ComposeTransform([
                td.ExpTransform().inv,
                td.AffineTransform(0, -1)]))

        with torch.no_grad():
            self.logits = nn.Parameter(self.logits_prior.sample((n_samples,)))
            self.log_vars = nn.Parameter(self.vars_prior.sample(
                (n_samples, n_components, dimensions)).double())
            self.means = nn.Parameter(torch.normal(0.0, 2 * (0.5*self.log_vars).exp()))

    def forward(self, x):
        sigmas = (0.5 * self.log_vars).exp()
        betas = self.stick_break(self.logits)

        x = x[:, None, None, :]

        log_ps = td.Normal(self.means, sigmas).log_prob(x).sum(dim=-1)
        log_ps = torch.logsumexp(log_ps + betas.log(), dim=2)

        return log_ps

    def log_prior(self):
        p_logits = self.logits_prior.log_prob(self.logits)
        p_sigma = self.vars_prior.log_prob(self.log_vars).sum(dim=-1).sum(dim=-1)
        means_prior = td.Normal(0.0, 2 * (0.5*self.log_vars).exp())
        p_means = means_prior.log_prob(self.means).sum(dim=-1).sum(dim=-1)

        return p_logits + p_sigma + p_means

    def sample_data(self, n_samples: int = 1000):
        with torch.no_grad():
            sigmas = (0.5 * self.log_vars[0]).exp()
            betas = self.stick_break(self.logits[0])
            mus = self.means[0]

            zs = rng.choice(self.n_components, p=betas.numpy(), size=n_samples)
            xs = torch.normal(mus[zs], sigmas[zs])

        return xs

