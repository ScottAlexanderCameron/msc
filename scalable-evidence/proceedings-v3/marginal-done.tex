%=================================================================
\documentclass[proceedings,article,accept,moreauthors,pdftex]{Definitions/mdpi} 

%=================================================================
\firstpage{1} 
\makeatletter 
\setcounter{page}{\@firstpage} 
\makeatother
\pubvolume{xx}
\issuenum{1}
\articlenumber{5}
\pubyear{2019}
\copyrightyear{2019}
%\externaleditor{Academic Editor: name}
\history{Received: date; Accepted: date; Published: date}
\updates{yes} % If there is an update available, un-comment this line

%% MDPI internal command: uncomment if new journal that already uses continuous page numbers 
%\continuouspages{yes}

%------------------------------------------------------------------
% The following line should be uncommented if the LaTeX file is uploaded to arXiv.org
%\pdfoutput=1

%=================================================================
% Add packages and commands here. The following packages are loaded in our class file: fontenc, calc, indentfirst, fancyhdr, graphicx, lastpage, ifthen, lineno, float, amsmath, setspace, enumitem, mathpazo, booktabs, titlesec, etoolbox, amsthm, hyphenat, natbib, hyperref, footmisc, geometry, caption, url, mdframed, tabto, soul, multirow, microtype, tikz
\usepackage{amssymb, bm}
\usepackage{subcaption}

%\input{defs}
\newcommand{\ubw}{{\mathbf{w}}}
\newcommand{\uby}{{\mathbf{y}}}
\newcommand{\ubx}{{\mathbf{x}}}
\newcommand{\ubb}{{\mathbf{b}}}
\newcommand{\ubv}{{\mathbf{v}}}
\newcommand{\ubn}{{\mathbf{n}}}
\newcommand{\bmtheta}{{\bm{\theta}}}
\newcommand{\data}{{\mathcal{D}}}
\newcommand{\ev}{{\mathcal{Z}}}
\newcommand{\E}{{\mathbb{E}}}
%=================================================================
% Full title of the paper (Capitalized)
\Title{
  A Sequential Marginal Likelihood Approximation
  Using Stochastic Gradients
}

% Author Orchid ID: enter ID or remove command
\newcommand{\orcidauthorA}{0000-0003-3830-6673}
\newcommand{\orcidauthorB}{0000-0002-9982-4223}
\newcommand{\orcidauthorC}{0000-0001-5625-8623}

% Authors, for the paper (add full first names)
\Author{Scott A. Cameron $^{1,2,}$*\orcidA{}, Hans C. Eggers $^{1,2}$\orcidB{} and Steve Kroon $^{3}$\orcidC{}}%NOTE: Please carefully check the accuracy of names and affiliations. 

% Authors, for metadata in PDF
\AuthorNames{Scott A. Cameron, Hans C. Eggers and Steve Kroon}

% Affiliations / Addresses (Add [1] after \address if there is only one affiliation.)
\address{%
$^{1}$ \quad Department of Physics, Stellenbosch University, South Africa\\ %please add email of H.C.E.
$^{2}$ \quad National Institute for Theoretical Physics, Stellenbosch, South
Africa\\
$^{3}$ \quad Computer Science Division, Stellenbosch University, South Africa} %please add email of S.K.
%NOTE: Please add the city and post code. (or zip code in the US).

% Contact information of the corresponding author
\corres{Correspondence: scott.a.cameron@live.co.uk}
\firstnote{Presented at the 39th International Workshop on Bayesian Inference and Maximum Entropy Methods in Science and Engineering, Garching, Germany, 30 June--5 July 2019.}

% Current address and/or shared authorship
%\firstnote{Current address: Affiliation 3} 
%\secondnote{These authors contributed equally to this work.}
% The commands \thirdnote{} till \eighthnote{} are available for further notes

%\simplesumm{} % Simple summary

%\conference{} % An extended version of a conference paper

% Abstract (Do not insert blank lines, i.e. \\) 
%\input{abstract}
\abstract{
Existing algorithms like nested sampling and annealed importance sampling are
able to produce accurate estimates of the marginal likelihood of a model, but
tend to scale poorly to large data sets. This is because these algorithms
need to recalculate the log-likelihood for each
iteration by summing over the whole data set.  Efficient
scaling to large data sets requires that algorithms only visit small subsets
(mini-batches) of data on each iteration. To this end, we estimate the marginal
likelihood via a sequential decomposition into a product of predictive
distributions $p(\uby_n|\uby_{<n})$.  Predictive distributions can be
approximated efficiently through Bayesian updating using stochastic gradient
Hamiltonian Monte Carlo, which approximates likelihood gradients using
mini-batches. Since each data point typically contains little information
compared to the whole data set, the convergence to each successive posterior
only requires a short burn-in phase.  This approach can be viewed as a special
case of sequential Monte Carlo (SMC) with a single particle, but differs from
typical SMC methods in that it uses stochastic gradients. We illustrate how this
approach scales favourably to large data sets with some simple models.}
% Keywords
\keyword{marginal likelihood; Monte Carlo; stochastic gradients}

% The fields PACS, MSC, and JEL may be left empty or commented out if not applicable
%\PACS{J0101}
%\MSC{}
%\JEL{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\input{body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

Marginal likelihood (ML), sometimes call evidence, is a quantitative measure of
how well a model can describe a particular data set; it is the probability that
the data set occurred within that model.  Consider a Bayesian model with
parameters $\bmtheta$ for a data set $\data = \{\uby_n\}_{n=1}^{N}$.  The~ML is the~integral

\begin{equation*}
  \ev := p(\data) = \int p(\data|\bmtheta) p(\bmtheta) \, d\bmtheta,
\end{equation*}
%
where $p(\data|\bmtheta)$ is the likelihood and $p(\bmtheta)$ is the prior. In~this paper we consider the case where the data are conditionally independent
given the parameters
\begin{equation}
  \label{eq:cond-ind}
  p(\data|\bmtheta) = \prod_{n} p(\uby_n|\bmtheta),
\end{equation}
%
as is common in many parametric models.
%
The posterior distribution over a set of models is proportional to their
ML and so approximations to it are sometimes used for model comparison, and~weighted model averaging~\cite{bayesian-reasoning} (chapter 12).
%NOTE: Please consider this suggested change.
This integral is typically analytically intractable for any but the
simplest models, so one must resort to numerical approximation methods.
%
Nested sampling~(NS)~\cite{nested-sampling} and annealed importance
sampling~(AIS)~\cite{ais} are two algorithms able to produce accurate estimates
of the ML. NS accomplishes this by sampling from the prior under constraints of
increasing likelihood, and~AIS by sampling from a temperature annealed
distribution $\propto p(\data|\bmtheta)^{\beta}p(\bmtheta)$ and averaging over
samples with appropriately calculated importance weights.
%
Although NS and AIS produce accurate estimates of the ML, they
tend to scale poorly to large data sets due the fact that they need to
repeatedly calculate the likelihood function: for NS this is to ensure staying
within the constrained likelihood contour; for AIS the likelihood must be
calculated both to sample from the annealed distributions using some Markov
chain Monte Carlo (MCMC) method, as~well as to calculate the importance
weights.
%
Calculation of the likelihood is computationally expensive on large data sets.
To combat this problem, various optimization and sampling algorithms rather make
use of stochastic approximations of the likelihood by sub-sampling the data set
into mini-batches $B \subseteq \data$~\cite{sgld}. The~stochastic
log-likelihood approximation~is

\begin{equation*}
  \log{p(\data|\bmtheta)} \approx \frac{|\data|}{|B|} \sum_{\uby \in B}
  \log{p(\uby|\bmtheta)},
\end{equation*}
%
with each iteration of these algorithms generally using a different mini-batch.
%
Unfortunately NS and AIS cannot trivially use mini-batching to improve
scalability. Using stochastic likelihood approximations changes the statistics
of the likelihood contours in NS, allowing particles to occasionally move to
lower likelihood instead of higher, violating the basic assumptions of the
algorithm. AIS could benefit from using stochastic likelihood gradients during
the MCMC steps, but~it is not obvious how one would calculate the
importance weights in this setting.
%
This work presents an approach for large-scale ML approximations using
mini-batches. This is done using a sequential decomposition of the ML into
predictive distributions, which can each be approximated using stochastic
gradient MCMC methods. The~particles sampled from each previous posterior
distribution can be reused for efficiency since they will typically be close to
the next posterior.  This can be viewed as a special case of sequential Monte
Carlo with Bayesian updating~\cite{smc}.

We illustrate our approach by calculating ML estimates on three simple models.
For these models, we obtain roughly an order of magnitude speedup over nested
sampling on data sets with one million observations with negligible loss in
accuracy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sequential Marginal Likelihood~Estimation}
\label{sec:seq}

The ML can be decomposed, through the product rule, into~a
product of predictive distributions of the following~form
 
\begin{equation*}
  \ev = \prod_{n} p(\uby_n|\uby_{<n}),
\end{equation*}
% 
where, from~Equation~(\ref{eq:cond-ind}),
\begin{equation}
  \label{eq:pred}
  p(\uby_n|\uby_{<n}) = \int p(\uby_n|\bmtheta) p(\bmtheta|\uby_{<n}) \,
  d\bmtheta.
\end{equation}
% 

Assuming one is able to produce accurate estimates $\hat{p}(\uby_n|\uby_{<n})$
of the predictive probabilities, the~log-ML can be approximated by
\begin{equation}
  \label{eq:seq}
  \log\hat\ev = \sum_n \log\hat{p}(\uby_n|\uby_{<n}).
\end{equation}
% 
In this way the difficult problem of estimating an integral of an extremely
peaked function, $p(\data|\bmtheta)$, reduces to the easier problem of
estimating many integrals of smoother functions $p(\uby_n|\bmtheta)$.
%
Note that this approach can more generally be applied using any other sequential
decomposition of the data set. We only present derivations using the approach
above for notational clarity but we decompose the data into varying-sized chunks
of observations during our experiments as discussed in
Section~\ref{sec:experiments}.

Typical sequential Monte Carlo (SMC) methods use a similar approach, and~calculate predictive estimates using a combination of importance resampling and
MCMC mutation steps~\cite{smc}. Generic examples of such algorithms are the
bootstrap particle filter~\cite{bootstrap-filter}, which is often used for
posterior inference in hidden Markov models and other latent variable sequence
models~\cite{smc}, and~the ``left-to-right'' algorithm which is used
in~\cite{topic-models} to evaluate topic~models.

The computational efficiency of using this approach depends on the method of
approximating Equation~(\ref{eq:pred}). One such estimator is
\begin{equation}
  \label{eq:p-lower}
  \hat{p}(\uby_n|\uby_{<n}) = \frac{1}{M}
  \sum_{i=1}^M p(\uby_n|\bmtheta_i),
\end{equation}
% 
where each $\bmtheta_i$ is drawn from the posterior distribution
$p(\bmtheta|\uby_{<n})$ using MCMC methods. Again, one might use a chunk of
observations for this predictive estimate rather than just one.  As~described
in~\cite{sandwich}, estimators of this form tend to underestimate the ML, but~still converge to the exact ML in the limit $M \to \infty$.
%
Since samples from the previous posterior, $p(\bmtheta|\uby_{<n-1})$, would
generally be available at each step, we expect only a small number of steps will
be needed to accurately sample from the next posterior distribution,
$p(\bmtheta|\uby_{<n})$.
%
Metropolis-Hastings based MCMC algorithms would have to iterate over the
previous $n-1$ data points in order to calculate the acceptance probability
% TODO maybe elaborate
for each Markov transition, and~so using them to estimate $\log\ev$ in this
sequential manner would scale at least quadratically in $N$.
%
The key computational improvement in our approach is instead using stochastic
gradient based MCMC algorithms such as stochastic gradient Hamiltonian Monte
Carlo~\cite{sghmc}. SGHMC utilizes mini-batching, allowing one to efficiently
draw samples from the posterior distribution $p(\bmtheta|\uby_{<n})$ even when
$n$ is large. We now describe how one can use SGHMC to sample from posterior
distributions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Stochastic Gradient Hamiltonian Monte~Carlo}

SGHMC~\cite{sghmc} simulates a Brownian particle in a potential, by~numerically integrating the Langevin~equation
 
\begin{equation*}
  \begin{array}{ll}
    d\bmtheta &= \ubv~dt\\
    d\ubv &= - \nabla U(\bmtheta) dt - \gamma \ubv dt + \sqrt{2\gamma}dW,
  \end{array}
\end{equation*}
%
where $U(\bmtheta)$ is the potential energy, $\gamma$ is the friction
coefficient and $W$ is the standard Wiener process~\cite{stochastic-calculus}.
That is, each increment $\Delta W$ is independently normally distributed with
mean zero and variance $\Delta t$.
%
It can be shown through the use of a Fokker-Planck equation~\cite{gardiner}, that
the above dynamics converge to the stationary~distribution

\begin{equation*}
  p(\bmtheta, \ubv) \propto \exp\left(- U(\bmtheta) - \frac{\ubv^2}{2}\right).
\end{equation*}
%

We can use this to sample from the full data posterior by using a potential
energy equal to the negative log-joint $U(\bmtheta) := -\log{p}(\data,
\bmtheta)$.
%
The numeric integration is typically discretized~\cite{sghmc, cyclic-sghmc%
%TODO  , bohamiann
} as follows
\begin{equation}
  \label{eq:sghmc}
  \begin{array}{ll}
    \Delta \bmtheta &= \ubv \\
    \Delta \ubv &= - \eta \nabla \hat{U}(\bmtheta) - \alpha \ubv +
    \bm{\epsilon} \sqrt{2(\alpha - \hat{\beta})\eta},
  \end{array}
\end{equation}
% 
where $\eta$ is called the learning rate, $1 - \alpha$ is the momentum
decay, $\bm{\epsilon}$ is a standard Gaussian random vector,
$\hat\beta$ is an optional parameter to offset the variance of the stochastic
gradient term~and
 
\begin{equation*}
  \hat{U}(\bmtheta) = - \frac{|\data|}{|B|} \sum_{\uby \in B}
  \log{p}(\uby|\bmtheta) - \log{p}(\bmtheta)
\end{equation*}
is an unbiased estimate of $U(\bmtheta)$.
% 
A new mini-batch $B$ is sampled for each iteration of Equation~(\ref{eq:sghmc}).
Since $U(\bmtheta)$ grows with the size of the data set, a~small learning rate
$\eta \sim \mathcal{O}(\frac{1}{|\data|})$ is required to minimize the
discretization  error.
%
The variance of the stochastic gradient term is proportional to $\eta^2$ while
the variance of the injected noise is proportional to $\eta$, so in the limit
$\eta \to 0$, stochasticity in the gradient estimates becomes negligible and the
correct continuum dynamics are recovered, even if one ignores the errors from
the stochastic gradient noise and $\hat{\beta} = 0$ is used. We refer the reader
to~\cite{sgrhmc,bohamiann,sghmc} for an in-depth analysis of the algorithm
parameters.

For the purposes of Bayesian updating, we use SGHMC to sample from the
$n$\textsuperscript{th} posterior distribution, $p(\bmtheta|\uby_{\le n})$,
using the stochastic potential~energy

\begin{equation*}
  \hat{U}_n(\bmtheta) =
  -\frac{n-1}{|B|}\sum_{\uby \in B} \log{p}(\uby|\bmtheta)
  -\log{p}(\uby_n|\bmtheta)
  -\log{p(\bmtheta)},
\end{equation*}
%
where the mini-batch is drawn i.i.d. with replacement from the set of all
previous data points \mbox{i.e.,~$B \subset \{\uby_k|k < n\}$.} The~extra term here is
to ensure that the previously unseen data point is always taken into account.
If the extra term was not included, there would be some chance that the new data
point does not get taken into account during the SGHMC steps.  With~this
potential energy SGHMC still converges to the required posterior distribution,
since it is still an unbiased estimator of the negative~log-joint.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments}
\label{sec:experiments}

We use mini-batches of size 500, with~the following SGHMC parameters: $\eta =
0.1/n$, $\alpha = 0.2$ and $\hat{\beta} = 0$. Predictive distributions are
approximated using $M = 10$ samples and 20 burn-in steps for each new posterior.
As mentioned in Section~\ref{sec:seq}, rather than Bayesian updating by adding a
single observation at a time, we add chunks of data at a time. In~the following
experiments we use chunks of 20 data points when $n \le 80$, chunks of size
$\left\lfloor\frac{n}{4}\right\rfloor$ when $80 < n < 2000$, and~chunks of size
500 thereafter.
%
Our motivation for this is because we expect that smaller chunk sizes will give
a lower variance in the estimator Equation~(\ref{eq:p-lower}) when $n$ is small and so
the posterior is less peaked, but~for large $n$ using larger chunk sizes is more
efficient.

We use NS as our reference standard of accuracy. We implement NS with 20 SGHMC
steps to sample from the constrained prior. For~SGHMC used with NS we used
parameters $\eta = 10^{-3}$, $\alpha = 0.1$ and $\hat{\beta} = 0$
because there is no gradient noise when sampling from the prior. %NOTE:footnote is put within  parenthesis, because it should not be allowed in this journal
 Results reported are for two particles; more behave similarly but are slower.  We allow
NS to run until the additive terms are less than 1\% of the current $\hat\ev$
estimate. This is a popular stopping criterion and is also used
in~\cite{sandwich}. For~more information on the constrained sampler and NS see
Appendix~\ref{ap:ns}.  Our experiments were run on a laptop with an Intel i7
CPU. For~fair comparison, all code is single threaded. Multithreading gives a
considerable speedup when calculating the likelihood on large data sets but can
introduce subtle complexities that are difficult to control for in tests of
runtime~performance.

We evaluate our approach on the following three models using simulated data sets:

\subsection{Linear~Regression}

The data set consists of pairs $(\ubx, y)$ related~by

\begin{equation*}
  y = \ubw^T\ubx + b + \epsilon,
\end{equation*}
%
where $\epsilon$ is zero mean Gaussian distributed with known variance
$\sigma^2$. We do not assume any distribution over $\ubx$ as it always appears
on the right hand side of the~conditional.

\begin{equation*}
  p(y|\ubx,\ubw,b) = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(y -
      \ubw^T\ubx -
  b)^2}{2\sigma^2}\right)
\end{equation*}
%
Parameters are $\ubw$ and $b$, with~standard Gaussian priors. ML can be
calculated analytically for this model. We used 5 dimensional vectors $\ubx$;
this model has 6~parameters.

\subsection{Logistic~Regression}

The data set consists of pairs $(\ubx, y)$, where $\ubx$ is an observation
vector which is assigned a class label $y \in \{1,\ldots,K\}$.  The~labels have a
discrete distribution with probabilities given~by

\begin{equation*}
  p(y|\ubx, \bmtheta) = \frac{\exp(\ubw_y^T\ubx + b_y)}{\sum_k \exp(\ubw_k^T\ubx +
  b_k)}.
\end{equation*}
%
Parameters are $\bmtheta = (\ubw_{1:K}, b_{1:K})$, with~standard Gaussian priors.  Again we do not
assume any distribution over $\ubx$ as it always appears on the right hand side
of the conditional.
%
We used 10 dimensional vectors $\ubx$ with 4 classes; this model has 44
parameters.
% I had previously said 14; I added when I should have multiplied...

\subsection{Gaussian Mixture~Model}

The data are modeled by a mixture of multivariate Gaussian distributions with
diagonal covariance matrices. Mixture weights, means and variances are treated
as parameters. This type of model is often treated as a latent variable model,
where the mixture component assignments of each data point are the latent
variables. Here we marginalize out the latent variables to obtain the following
conditional distribution:

\begin{equation*}
  p(\uby|\bmtheta) = \sum_{k=1}^K \beta_k\prod_{j=1}^d
  \frac{1}{\sqrt{2\pi\sigma_{k,j}^2}}
  \exp\left(-\frac{(y_j - \mu_{k,j})^2}{2\sigma_{k,j}^2}\right),
\end{equation*}

\begin{equation*}
  \bmtheta = (\beta_{1:K},\mu_{1:K,1:d},\sigma_{1:K,1:d}^2).
\end{equation*}
%

Mixture weights $\beta_{1:K}$ are modeled by a Dirichlet prior with $\bm{\alpha}
= 1$; means $\mu_{k,j}$ are modeled by Gaussian priors, centered around zero and
with variance $4\sigma_{k,j}^2$; variances $\sigma_{k,j}^2$ are modeled by
inverse gamma priors with shape and scale parameters equal to 1.
%
We used 5 Gaussian components and observations were 2 dimensional; this model
has 25 parameters with 24 degrees of~freedom.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and~Discussion}

The log-ML typically grows linearly in the number of data points.
For this reason, it is natural to measure errors in $\frac{\log\ev}{N}$ rather
than $\log\ev$.  In~\cite{sandwich}, the~authors suggest that errors in
$\frac{\log\ev}{N}$ of $0.1$ are acceptable. For~each model, we measured the
runtime performance of nested sampling and our sequential estimator for various
data set sizes up to one million data points. 
%
Due to computational constraints we run NS only for data set sizes at
logarithmically increasing intervals, while the sequential sampler naturally
produces many more intermediate results in a single run.
%
In some of the figures below, the~sequential sampler initially underestimates the
log-ML.  We believe this is due to higher variance of Equation~(\ref{eq:p-lower}) when
$n$ is small, and~so we also give a hybrid result which replaces the initial
terms in Equation~(\ref{eq:seq}) with a NS estimate of the ML for the first 100 data
points.
 
\subsection{Linear~Regression}
For the linear regression model, the~exact ML is available analytically and is
shown in Figure~\ref{fig1}a for comparison. Both algorithms are able to
produce accurate results for this model for all data set sizes.  The~final error
of the sequential sampler on one million data points is only about $10^{-4}N$
(roughly 0.01\%).  For~this model, our method was faster than NS by about a
factor of 3 on one million~observations.

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/lin-logz.png}
    \caption{}
%    \label{fig:lin-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/lin-time.png}
    \caption{}
%    \label{fig:lin-time}
  \end{subfigure}
  \caption{%
    Linear regression model. (\textbf{a}) shows the accuracy of the
    sequential ML estimator compared to nested sampling and the exact ML  and
    (\textbf{b}) shows the run time of both methods.
  }
\label{fig1}
\end{figure}
\unskip

\subsection{Logistic~Regression}
For the largest data set, NS and our sequential sampler produced estimates which
differed by $3\times 10^{-4}N$ (roughly 0.7\%), which is negligible.
Our sequential sampler was almost a factor 17 faster than the nested sampler on
one million observations for this~model.




\subsection{Gaussian Mixture~Model}
The posterior distribution for this model is multimodal. Some modes are due to
permutation symmetries; these modes do not have to be  explored since each one
contains the same information. There are also some local modes which do not
necessarily capture meaningful information about the data; for example, fitting
a single Gaussian to the whole data set may be a poor local optimum of the
likelihood function. If~an MCMC walker finds one of these modes it can get
trapped. However, we find that by Bayesian updating, the~MCMC walkers tend to
leave the poor local modes early on, before~they become extremely peaked. This
is similar to how annealing can help prevent MCMC and optimization algorithms
from getting trapped in poor local optima. The~estimates produced by NS and our
sequential sampler differed on the largest data set by $2\times 10^{-3}N$
(roughly 0.06\%). For~this model our sequential sampler was about a factor 11
faster than the nested sampler on one million~observations.



In all the experiments our sequential sampler seems to converge to the same
result as NS within a negligible error for large $n$.
%
The initial disagreement between NS and our sequential sampler on the first few
thousand data points, seen in Figure~\ref{fig2}a and Figure~\ref{fig3}a, seems as if it can be attributed
to the initial terms in Equation~(\ref{eq:seq}), since the proposed hybrid approach,
replacing early terms in the sequential sampler by estimates based on NS,
matches NS closely for all data set~sizes.
\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/log-logz.png}
    \caption{}
%    \label{fig:log-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/log-time.png}
    \caption{}
%    \label{fig:log-time}
  \end{subfigure}
  \caption{%
    Logistic regression model. (\textbf{a}) shows the sequential
    ML estimator compared to nested sampling and
    (\textbf{b}) shows the run time of both methods.
  }
  \label{fig2}
\end{figure}

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-logz.png}
    \caption{}
%    \label{fig:gmm-logz}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-time.png}
    \caption{}
%    \label{fig:gmm-time}
  \end{subfigure}
  \caption{%
    Gaussian mixture model. (\textbf{a}) shows the sequential ML
    estimator compared to nested sampling and (\textbf{b}) shows the
    run time of both methods.
  }
 \label{fig3}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Materials and~Methods}

Code for this work was implemented using
pytorch~\cite{pytorch}. The~code for our experiments is
available at \url{https://gitlab.com/pleased/sequential-evidence}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}

We found that our sequential sampler using stochastic gradients was able to
produce accurate ML estimates with a speedup over NS. Furthermore, since the
marginal cost of updating the ML estimates when new data arrives does not depend
on the number of previous data points, this approach may be effective for
weighted model averaging in a setting where one periodically gets access to new
data, such as in streaming applications.
%
Potential future work may involve a more in-depth analysis of the algorithm
parameters, for~example automatic tuning of the number of samples, $M$, used for
approximating predictive probabilities. One can imagine further exploring the
effects of stochastic gradients for ML calculation in the full SMC setting,
including latent variable models and resampling steps, with~more general
stochastic gradient MCMC algorithms such as those in~\cite{sgrhmc}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{6pt} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% optional
%\supplementary{The following are available online at \linksupplementary{s1}, Figure S1: title, Table S1: title, Video S1: title.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\authorcontributions{
%For research articles with several authors, a short
%paragraph specifying their individual contributions must be provided. The
%following statements should be used ``
%
Conceptualization,
methodology,
software,
formal analysis,
investigation,
visualization,
writing--original draft preparation: S.A.C.;
validation,
writing--review and editing: S.A.C., H.C.E. and S.K.;
resources: S.K.;
project administration,
supervision: H.C.E. and S.K.;
funding acquisition: H.C.E.
%
%'', please turn to the
%\href{http://img.mdpi.org/data/contributor-role-instruction.pdf}{CRediT
%taxonomy} for the term explanation. Authorship must be limited to those who have
%contributed substantially to the work reported.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\funding{S. Cameron received bursary support from the South African National
Institute of Theoretical Physics, as~well as financial support from both the
organizers of MaxEnt 2019 and the National Institute of Theoretical Physics in
attending the~conference.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\acknowledgments{We wish to thank the organizers of MaxEnt 2019 and the South
African National Institute of Theoretical Physics for financial support in
attending the~conference.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\conflictsofinterest{The authors declare no conflict of~interest.} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% optional
\abbreviations{The following abbreviations are used in this manuscript:\\

\noindent 
\begin{tabular}{@{}ll}
AIS & Annealed importance sampling\\
MCMC & Markov chain Monte Carlo\\
ML & Marginal likelihood\\
NS & Nested sampling\\
SGHMC & Stochastic gradient Hamiltonian Monte Carlo
\end{tabular}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% optional
\appendixtitles{no} %Leave argument "no" if all appendix headings stay EMPTY (then no dot is printed after "Appendix A"). If~the appendix sections contain a heading then change the argument to "yes".
\appendix
%\input{appendix}
\section{}
\label{ap:ns}
\unskip
Nested sampling requires one to sample from the prior under increasing
likelihood constraints. Sampling under constraints is, in~general, a~difficult
problem. This sampling can be accomplished by repeatedly sampling from the prior
until the constraint is satisfied. This method of rejection sampling scales
extremely poorly with both the size of the data set and the dimension of the
parameter space, and~is not feasible on any but the smallest problems. However
NS using this rejection sampling method is a theoretically perfect
implementation of NS, as~it satisfies the assumptions of perfect i.i.d. sampling
upon which NS is based. Further more it is extremely easy to implement correctly
and so is a useful tool for testing the correctness of other NS~implementation.

Our implementation of NS is based on SGHMC, simply because the code for SGHMC
was already written. In~order to sample under constraints we use a similar
strategy to Galilean Monte Carlo as described in~\cite{gmc-ns}, where the
particle reflects off of the boundary of the likelihood contour by updating the
momentum as follows:

\begin{equation*}
  \Delta\ubv = -2 \ubn (\ubv\cdot\ubn).
\end{equation*}
%
Here $\ubn$ is a unit vector parallel to the likelihood gradient
$\nabla_{\bmtheta} p(\data|\bmtheta)$. While it is not the focus of our paper,
we note that we have not previously encountered the idea of applying SGHMC to
sampling under constraints; our implementation allows a fairly direct approach
to implementing NS in a variety of contexts, and~the technique may also
potentially be of value in other constrained sampling~contexts.

We found that, due to discretization error, the~constrained sampler tends to
undersample slightly at the boundaries of the constrained region; however the
undersampled volume is of the order of the learning rate $\eta$ and so can be
neglected if a small enough learning rate is~used.

We tested the correctness of our constrained sampler against NS with rejection
sampling for up to 250 observations. We found that the slight undersampling at
the constraint boundaries tends to make the NS estimate slightly higher that
that of the rejection sampler---see Figure~\ref{fig:ns-vs-reject}---but the
difference was within the acceptable range of $0.1N$, and~appears to be
decreasing with the number of~observations.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{experiments/figs/ns-vs-reject.png}
  \caption{%
    Comparison of NS with SGHMC to NS with rejection sampling for a
    1-dimensional Gaussian mixture model with 9 parameters (8 degrees of
    freedom).  The~size of the shaded band is $\pm0.1N$ around the rejection
    sampling implementation.
  }
  \label{fig:ns-vs-reject}
\end{figure}

%=====================================
% References, variant B: external bibliography
%=====================================
\reftitle{References}
%\externalbibliography{yes}
%\bibliography{ref}
\begin{thebibliography}{999}
\providecommand{\natexlab}[1]{#1}

\bibitem[Barber(2012)]{bayesian-reasoning}
Barber, D.
\newblock {\em Bayesian Reasoning and Machine Learning}; Cambridge University
  Press: New York, NY, USA,  2012.

\bibitem[Skilling(2006)]{nested-sampling}
Skilling, J.
\newblock Nested sampling for general {Bayesian} computation.
\newblock {\em Bayesian Anal.} {\bf 2006}, {\em 1},~833--859, doi:10.1214/06-BA127.

\bibitem[{Neal}(1998)]{ais}
{Neal}, R.M.
\newblock {Annealed Importance Sampling}.
\newblock {\em arXiv} {\bf 1998}, arXiv:physics/9803008.

\bibitem[Welling and Teh(2011)]{sgld}
Welling, M.; Teh, Y.W.
\newblock Bayesian Learning via Stochastic Gradient Langevin Dynamics.
\newblock  In Proceedings of the 28th International Conference on Machine
  Learning, {ICML} 2011, Bellevue, WA, USA, 28 June--2 July 2011;
  Getoor, L., Scheffer, T., Eds.; Omnipress: Madison, WI, USA,  2011; pp. 681--688.

\bibitem[{Naesseth} \em{et~al.}(2019){Naesseth}, {Lindsten}, and
  {Sch{\"o}n}]{smc}
{Naesseth}, C.A.; {Lindsten}, F.; {Sch{\"o}n}, T.B.
\newblock {Elements of Sequential Monte Carlo}.
\newblock {\em arXiv} {\bf 2019}, arXiv:1903.04797.

\bibitem[{Gordon} \em{et~al.}(1993){Gordon}, {Salmond}, and
  {Smith}]{bootstrap-filter}
{Gordon}, N.J.; {Salmond}, D.J.; {Smith}, A.F.M.
\newblock Novel approach to nonlinear/non-Gaussian Bayesian state estimation.
\newblock {\em IEE Proc. F - Radar Signal Process.} {\bf 1993},
  {\em 140},~107--113, doi:10.1049/ip-f-2.1993.0015.

\bibitem[Wallach \em{et~al.}(2009)Wallach, Murray, Salakhutdinov, and
  Mimno]{topic-models}
Wallach, H.M.; Murray, I.; Salakhutdinov, R.; Mimno, D.
\newblock Evaluation Methods for Topic Models.
\newblock  In Proceedings of the 26th Annual International Conference on Machine
  Learning, Montreal, QC, Canada,  14--18 June 2009; ACM: New York, NY, USA, 2009; pp. 1105--1112,
\newblock
  doi:{\changeurlcolor{black}\href{https://doi.org/10.1145/1553374.1553515}{\detokenize{10.1145/1553374.1553515}}}.

\bibitem[{Grosse} \em{et~al.}(2015){Grosse}, {Ghahramani}, and
  {Adams}]{sandwich}
{Grosse}, R.B.; {Ghahramani}, Z.; {Adams}, R.P.
\newblock {Sandwiching the marginal likelihood using bidirectional Monte
  Carlo}.
\newblock {\em arXiv} {\bf 2015}, arXiv:1511.02543.

\bibitem[Chen \em{et~al.}(2014)Chen, B.~Fox, and Guestrin]{sghmc}
Chen, T.; Fox, E.; Guestrin, C.
\newblock Stochastic Gradient {Hamiltonian} {Monte} {Carlo}.
\newblock In Proceedings of the {31st International Conference on Machine Learning,} Beijing, China,  21--26 June
  {2014}; Volume {5}.

\bibitem[Durrett(1996)]{stochastic-calculus}
Durrett, R.
\newblock {\em Stochastic Calculus: A Practical Introduction}; Probability and
  Stochastics Series; CRC Press: Boca Raton, Florida, United States, 1996,
  pp. 177--207

\bibitem[Gardiner(2004)]{gardiner}
Gardiner, C.W.
\newblock {\em Handbook of Stochastic Methods for Physics, Chemistry and the
  Natural Sciences}, 3rd ed.; Volume~13, {Springer Series in Synergetics};
  Springer: Berlin, Germany,  2004; p. xviii+415.

\bibitem[{Zhang} \em{et~al.}(2019){Zhang}, {Li}, {Zhang}, {Chen}, and
  {Wilson}]{cyclic-sghmc}
{Zhang}, R.; {Li}, C.; {Zhang}, J.; {Chen}, C.; {Wilson}, A.G.
\newblock {Cyclical Stochastic Gradient MCMC for Bayesian Deep Learning}.
\newblock {\em arXiv} {\bf 2019}, arXiv:1902.03932.

\bibitem[{Ma} \em{et~al.}(2015){Ma}, {Chen}, and {Fox}]{sgrhmc}
{Ma}, Y.A.; {Chen}, T.; {Fox}, E.B.
\newblock {A Complete Recipe for Stochastic Gradient MCMC}.
\newblock {\em arXiv} {\bf 2015},  arXiv:1506.04696.

\bibitem[Springenberg \em{et~al.}(2016)Springenberg, Klein, Falkner, and
  Hutter]{bohamiann}
Springenberg, J.T.; Klein, A.; Falkner, S.; Hutter, F.
\newblock Bayesian Optimization with Robust Bayesian Neural Networks. In {\em
  Advances in Neural Information Processing Systems 29}; Lee, D.D., Sugiyama,
  M., Luxburg, U.V., Guyon, I., Garnett, R., Eds.; Curran Associates, Inc.: Barcelona, Spain
  2016; pp. 4134--4142.

\bibitem[Paszke \em{et~al.}(2017)Paszke, Gross, Chintala, Chanan, Yang, DeVito,
  Lin, Desmaison, Antiga, and Lerer]{pytorch}
Paszke, A.; Gross, S.; Chintala, S.; Chanan, G.; Yang, E.; DeVito, Z.; Lin, Z.;
  Desmaison, A.; Antiga, L.; Lerer,~A.
\newblock Automatic Differentiation in PyTorch. {Available online:
https://openreview.net/pdJsrmfCZ (accessed on 24 June 2019)}

\bibitem[Skilling(2012)]{gmc-ns}
Skilling, J.
\newblock Bayesian Computation in Big Spaces-Nested Sampling and Galilean Monte
  Carlo. \emph{AIP Conf. Proc.} {\bf 2012}, %NOTE: newly added information, please confirm.
\newblock {\em 1443},~145--156, doi:10.1063/1.3703630.

\end{thebibliography}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}

