\documentclass[20pt,margin=1in,innermargin=-4.2in]{tikzposter}
\geometry{paperwidth=42in,paperheight=30in}
\usepackage[utf8]{inputenc}
\usepackage{adjustbox}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{graphicx}
%\usepackage[hidelinks]{hyperref}
\usepackage{mathrsfs}
\usepackage{subcaption}
\usepackage{url}
\usepackage[sort&compress,sectionbib]{natbib}
\usepackage{relsize}

\def\@externalbibliography{}
\newcommand{\externalbibliography}[1]{\gdef\@externalbibliography{#1}}
\bibliographystyle{plain}
%\bibpunct{[}{]}{,}{n}{}{}
\newcommand{\mlarge}[1]{\mathlarger{\mathlarger{\mathlarger{\mathlarger{#1}}}}}


\input{defs}

\tikzposterlatexaffectionproofoff
\usetheme{Simple}
%\usetitlestyle{Default}
\usecolorstyle[%
%colorPalette=PurpleGrayBlue,
colorOne=teal,
%colorTwo=black,
%colorThree=green
]{Denmark}

\makeatletter
\renewcommand\TP@maketitle{%
  \centering
  \color{titlefgcolor}
  {\bfseries \Huge \sc \@title \par} \vspace*{1em}
  {\huge \@author \par} \vspace*{1em}
  {\LARGE \@institute}
  \titlegraphic{\includegraphics[scale=2]{USlogo.pdf}}
  \tikz[remember picture,overlay]
  \node[scale=0.8,anchor=east,xshift=1.35\linewidth,yshift=5.0cm,inner sep=0pt]
  {\@titlegraphic};
  \titlegraphic{\includegraphics[scale=2]{logo-nithep.jpg}}
  \tikz[remember picture,overlay]
  \node[scale=0.8,anchor=west,xshift=-2.05\linewidth,yshift=5.0cm,inner sep=0pt]
  {\@titlegraphic};
}
\makeatother

\title{
  Sequential Evidence Estimation
  Using Stochastic Gradients
}
\author{Scott A. Cameron $^{1,2}$, Hans C. Eggers $^{1,2}$ and Steve Kroon
$^{3}$}
\institute{
$^{1}$ \quad Department of Physics, Stellenbosch University\\
$^{2}$ \quad National Institute for Theoretical Physics, Stellenbosch\\
$^{3}$ \quad Computer Science Division, Stellenbosch University
}

% begin document
\begin{document}
\maketitle
\centering
\begin{columns}
  \column{0.32}
  \block{Contributions}{
    \huge
    \begin{itemize}
      \item We propose a method of estimating marginal likelihood using
        stochastic gradients, which gives a considerable speedup on large data
        sets over traditional methods.
        %Our approach is especially beneficial for
        %applications where one wants to update ML estimates as new data arrives.
      \item We implement a constrained SGHMC sampler for use in nested sampling.
    \end{itemize}
  }

  \block{Stochastic Gradient Hamiltonian Monte Carlo}{
    SGHMC~\cite{sghmc} simulates a Brownian particle in a potential:
    \begin{equation*}
      \begin{array}{ll}
        d\bmtheta &= \ubv dt\\
        d\ubv &= - \nabla U(\bmtheta) dt - \gamma \ubv dt + \sqrt{2\gamma}dW,
      \end{array}
    \end{equation*}
    %
    where $W$ is the standard Wiener process.
    The above dynamics converge to the stationary distribution
    \begin{equation*}
      p(\bmtheta, \ubv) \propto \exp\left(- U(\bmtheta) - \frac{\ubv^2}{2}\right).
    \end{equation*}
    %
    We can use this to sample from the posterior by using a potential
    energy equal to the negative log-joint $U(\bmtheta) := -\log{p}(\data,
    \bmtheta)$. SGHMC is implemented numerically as follows
    \begin{align*}
      \Delta \bmtheta &= \ubv \\
      \Delta \ubv &= - \eta \nabla \hat{U}(\bmtheta) - \alpha \ubv +
      \bm{\epsilon} \sqrt{2(\alpha - \hat{\beta})\eta}, \\
    \end{align*}
    where $\hat{U}$ is an unbiased estimator of the potential
    and $\hat{\beta}$ is a parameter to offset
    the gradient noise. For Bayesian updating we use the following potential to
    sample from the $n$\textsuperscript{th} posterior
    $p(\bmtheta|\uby_{\le n})$:
    \begin{equation*}
      \hat{U}_n(\bmtheta) =
      -\frac{n-1}{|B|}\sum_{\uby \in B} \log{p}(\uby|\bmtheta)
      -\log{p}(\uby_n|\bmtheta)
      - \log{p(\bmtheta)}.
    \end{equation*}
    $B$ is a random mini-batch of previously seen data;
    $B\subset\{\uby_{k}|k<n\}$.
    Mini-batching allows efficient sampling from the posterior even when $n$ is
    large.
  }

  \block{}{
    \small
      email: scott.a.cameron@live.co.uk\\
      code: \url{https://gitlab.com/pleased/sequential-evidence}
  }

  \column{0.36}
  \block{Experiments}{
    We use nested sampling~\cite{nested-sampling} as our reference standard of
    accuracy.

    \subsection*{Linear Regression}
    \begin{minipage}{0.49\linewidth}
      \centering{\Huge 3 $\times$ Speedup}
      \vspace{1cm}
      \begin{align*}
        y &\sim \mathcal{N}(\ubw^T\ubx + b, \sigma^2)\\
        \ubw &\sim \mathcal{N}(0, 1) \\
        b &\sim \mathcal{N}(0, 1)
      \end{align*}
    \end{minipage}
    \begin{minipage}{0.49\linewidth}
      \includegraphics[width=\textwidth]{experiments/figs/lin-logz.png}
    \end{minipage}

    \subsection*{Logistic Regression}
    \begin{minipage}{0.49\linewidth}
      \includegraphics[width=\textwidth]{experiments/figs/log-logz.png}
    \end{minipage}
    \begin{minipage}{0.49\linewidth}
      \centering{\Huge 17 $\times$ Speedup}
      \vspace{1cm}
      \begin{align*}
        p(y|\ubx, \ubw_{1:K}, b_{1:K}) &= \frac{\exp(\ubw_y^T\ubx + b_y)}{\sum_k
        \exp(\ubw_k^T\ubx + b_k)} \\
        \ubw_k &\sim \mathcal{N}(0, 1)\\
        b_k &\sim \mathcal{N}(0, 1)
      \end{align*}
    \end{minipage}

    \subsection*{Gaussian Mixture Model}
    \begin{minipage}{0.49\linewidth}
      \centering{\Huge 11 $\times$ Speedup}
      \vspace{1cm}
      \begin{equation*}
        \uby \sim \sum_{k=1}^K \beta_k
        \mathcal{N}(\mu_k,\sigma_k^2)
      \end{equation*}
      \centering{With conjugate priors}
    \end{minipage}
    \begin{minipage}{0.49\linewidth}
      \includegraphics[width=\textwidth]{experiments/figs/gmm-logz.png}
    \end{minipage}
  }

  \column{0.32}
  \block{Sequential ML Estimation}{
    Sequential ML decomposition:
    \begin{equation*}
      \ev = \prod_{n} p(\uby_n|\uby_{<n}),
    \end{equation*}
    where,
    \begin{equation*}
      p(\uby_n|\uby_{<n}) = \int p(\uby_n|\bmtheta) p(\bmtheta|\uby_{<n}) \,
      d\bmtheta.
    \end{equation*}
    We can estimate predictive probabilities by
    \begin{equation*}
      \hat{p}(\uby_n|\uby_{<n}) = \frac{1}{M}
      \sum_{i=1}^M p(\uby_n|\bmtheta_i) \qquad
      \bmtheta_i \sim p(\bmtheta|\uby_{<n}) \mbox{ using MCMC },
    \end{equation*}
    %
    and log-ML by
    \begin{equation*}
      \log\hat\ev = \sum_n \log\hat{p}(\uby_n|\uby_{<n}).
    \end{equation*}
    %
    This type of ML estimator is a form of sequential Monte Carlo~\cite{smc}.
  }

  \block{Future Work}{
    \begin{itemize}
      \item Automatic tuning of algorithm parameters.
      \item Exploring the effects of stochastic gradients in the full SMC
        setting, with more general stochastic gradient MCMC algorithms.
    \end{itemize}
  }

  \block{Acknowledgements}{
    We wish to thank the organizers of MaxEnt 2019 and the National Institute of
    Theoretical Physics for financial support in attending the conference.
  }

  \block{References}{
    \vspace{-1em}
    \begin{footnotesize}
      \renewcommand{\refname}{}
      \externalbibliography{yes}
      \bibliography{ref}
    \end{footnotesize}
  }
\end{columns}
\end{document}
