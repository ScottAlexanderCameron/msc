\documentclass[a4paper,12pt]{article}

\usepackage{amsmath, amssymb, bm}
\usepackage{cite}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{url}

\textwidth16cm
\textheight23cm
\topmargin-2.0cm
\oddsidemargin0cm
\parindent0pt
\pagestyle{empty}

\input{defs}

\title{
  A Sequential Marginal Likelihood Approximation
  Using Stochastic Gradients
}

\author{
  \underline{Scott A. Cameron}$^{1,2}$, Hans Eggers$^{1,2}$, Steve Kroon$^{3}$\\
  \small
  (1) Department of Physics, Stellenbosch University, Matieland 7602,
  South Africa\\
  \small
  (2) National Institute for Theoretical Physics, Matieland 7602,
  South Africa\\
  \small
  (3) Department of Computer Science, Stellenbosch University, Matieland 7602,
  South Africa\\
  \small
  email: scott.a.cameron@live.co.uk
}
\date{\today}

\begin{document}
\maketitle

\begin{abstract}
\noindent
Existing algorithms like nested sampling and annealed importance sampling are
able to produce accurate estimates of the marginal likelihood of a model, but
tend to scale poorly to large data sets. This is because these algorithms
need to recalculate the log-likelihood for each
iteration by summing over the whole data set.  Efficient
scaling to large data sets requires that algorithms only visit small subsets
(mini-batches) of data on each iteration. To this end, we estimate the marginal
likelihood via a sequential decomposition into a product of predictive
distributions $p(\uby_n|\uby_{<n})$.  Predictive distributions can be
approximated efficiently through Bayesian updating using stochastic gradient
Hamiltonian Monte Carlo, which approximates likelihood gradients using
mini-batches. Since each data point typically contains little information
compared to the whole data set, the convergence to each successive posterior
only requires a short burn-in phase.  This approach can be viewed as a special
case of sequential Monte Carlo (SMC) with a single particle, but differs from
typical SMC methods in that it uses stochastic gradients. We illustrate how this
approach scales favourably to large data sets with some simple models.
\end{abstract}

\section{Introduction}

\begin{itemize}
  \item Marginal likelihood is a quantitative measure of how well a model can
    describe a particular dataset; it is the probability that the data set
    occurred within that model.

  \item Consider a Bayesian model with parameters $\bmtheta$ for a data set $\data =
    \{\uby_n\}_{n=1}^{N}$.
    The marginal likelihood is the integral
    \begin{equation}
      \ev = p(\data) = \int p(\data|\bmtheta) p(\bmtheta) \, d\bmtheta,
    \end{equation}
    where $p(\data|\bmtheta)$ is the likelihood and $p(\bmtheta)$ is the prior.
    In this paper we assume for simplicity that the data are
    conditionally independent given the parameters
    \begin{equation}
      \label{eq:cond-ind}
      p(\data|\bmtheta) = \prod_{n} p(\uby_n|\bmtheta),
    \end{equation}
    as is the common scenario in many parametric models.

  \item The posterior distribution over a set of models is proportional to the
    marginal likelihood and so it is sometimes used for model comparison, and
    weighted model averaging.

  \item This integral is typically analytically intractable for any but the
    simplest models, so one must resort to numerical approximation methods.

  \item Nested sampling (NS) \cite{nested-sampling} and annealed importance
    sampling (AIS) \cite{ais} are able to produce accurate estimates of the
    marginal likelihood. NS accomplishes this by sampling from the prior under
    constraints of increasing likelihood, and AIS by sampling from a temperature
    annealed distribution $\propto p(\data|\bmtheta)^{\beta}p(\bmtheta)$ and
    averaging over samples with appropriately calculated importance weights.

  \item Although NS and AIS produce accurate estimates of the marginal
    likelihood, they tend to scale poorly to large data sets due the fact that
    they need to calculate the likelihood function each iteration: for NS this
    is to ensure staying within the likelihood contour; for AIS the likelihood
    must be calculated both to sample from the annealed distributions using some
    Markov chain Monte Carlo (MCMC) method, as well as the calculate the importance
    weights.

  \item Calculation of the likelihood can be computationally expensive on large
    data set. To combat this problem, many optimization and sampling algorithms
    exist which use stochastic approximations of the likelihood by sub-sampling
    the data set into mini-batches $B \subseteq \data$. The stochastic
    log likelihood approximation is
    \begin{equation}
      \log{p(\data|\bmtheta)} \approx \frac{|\data|}{|B|} \sum_{\uby \in B}
      \log{p(\uby|\bmtheta)},
    \end{equation}
    Where each iteration of these algorithms uses a different mini-batch.

  \item Unfortunately NS and AIS cannot trivially use mini-batching to improve
    scalability. Using stochastic likelihood approximations changes the statics
    of the likelihood contours in NS, allowing particles to occasionally move to
    lower likelihood instead of higher. AIS could benefit from using stochastic
    likelihood gradients during the MCMC steps but it is not obvious how one
    would deal with calculation of the importance weights in this setting.
    
  \item In order to efficiently approximate marginal likelihood on large data
    sets, we propose using a sequential decomposition into predictive
    distributions, which can be approximated using stochastic gradient MCMC
    methods. This can be viewed as a special case of sequential Monte Carlo.
\end{itemize}

\section{Sequential Marginal Likelihood Estimation}

\begin{itemize}
  \item Marginal likelihood can be decomposed, through the product rule, into a
    product of predictive distributions of the following form
    \begin{equation}
      \ev = \prod_{n} p(\uby_n|\uby_{<n}),
    \end{equation}
    where, from Eq~\ref{eq:cond-ind},
    \begin{equation}
      p(\uby_n|\uby_{<n}) = \int p(\uby_n|\bmtheta) p(\bmtheta|\uby_{<n}) \,
      d\bmtheta
    \end{equation}

  \item The difficult problem of estimating an integral of an extremely peaked
    function reduces to the easier problem of estimating many integrals of
    smoother functions.

  \item Although multiplication compounds errors, applications are typically
    interested in the log of the marginal
    likelihood\cite{nested-sampling,sandwich} for which the errors become
    additive.

  \item Assuming one is able to produce accurate estimates $\hat{p}(\uby_n|\uby_{<n})$
    of the predictive distributions, the sequential marginal likelihood
    estimator is
    \begin{equation}
      \label{eq:seq}
      \log\hat\ev = \sum_n \log\hat{p}(\uby_n|\uby_{<n})
    \end{equation}

  \item In typical sequential Monte Carlo methods, this approach is used, and
    the predictive estimates $\hat{p}$ are calculated using a combination of
    importance resampling and MCMC mutation steps.

  \item The computational efficiency of such an algorithm is determined by the method of
    approximating $\hat{p}$. One such estimator is
    \begin{equation}
      \label{eq:p-lower}
      \hat{p}(\uby_n|\uby_{<n}) = \frac{1}{M} \sum_{i=1}^M p(\uby_n|\bmtheta_i),
    \end{equation}
    where each $\bmtheta_i$ is drawn from the posterior distribution
    $p(\bmtheta|\uby_{<n})$ using MCMC methods. Metropolis-Hastings based MCMC
    algorithms have to iterate over the entire dataset for each Markov
    transition, and so using them to estimating $\log\ev$ would have at least
    polynomial time complexity.

  \item We propose using stochastic gradient based MCMC algorithms such as
    stochastic gradient Hamiltonian Monte Carlo\cite{sghmc}. SGHMC utilizes
    mini-batching allowing one to efficiently draw samples from the posterior
    distribution $p(\bmtheta|\uby_{<n})$ even when $n$ is large.

  \item Note that $\uby_n$ does not actually have to be an individual data
    point, instead the data can be coarse-grained into chunks of some
    user-specified size. Smaller chuck sizes typically provide better
    approximations since $p(\uby_n|\bmtheta)$ would be less peaked but tend to
    make the algorithm slower due to the larger number of terms in the sum
    Eq~\ref{eq:seq}.
\end{itemize}

\section{Stochastic Gradient HMC}

\begin{itemize}
  \item SGHMC\cite{sghmc} simulates a Brownian particle in a potential, by
    numerically integrating the Langevin equation
    \begin{equation}
      \begin{array}{ll}
        d\bmtheta &= \ubv dt\\
        d\ubv &= - \nabla U(\bmtheta) dt - \gamma \ubv dt + \sqrt{2\gamma}dW,
      \end{array}
    \end{equation}
    Where $U(\bmtheta)$ is the potential energy, $\gamma$ is the friction
    coefficient and $W$ is the standard Wiener process. That is, each $\Delta W$
    is independently normally distributed with mean zero and variance 
    $\Delta t$.

  \item It can be shown through the use of a Fokker-Planck equation, that the
    above dynamics converges to the stationary distribution
    \begin{equation}
      p(\bmtheta, \ubv) \propto \exp\left(- U(\bmtheta) - \frac{\ubv^2}{2}\right).
    \end{equation}

  \item By choosing the potential to be the negative log joint $U(\bmtheta) =
    -\log{p}(\data, \bmtheta)$, the dynamics produces samples from the posterior
    distribution.

  \item The numeric integration is typically discretized as follows
    \begin{equation}
      \label{eq:sghmc}
      \begin{array}{ll}
        \Delta \bmtheta &= \ubv \\
        \Delta \ubv &= - \eta \nabla \hat{U}(\bmtheta) - \alpha \ubv +
        \bm{\epsilon} \sqrt{2(\alpha - \hat{\beta})\eta},
      \end{array}
    \end{equation}
    Where $\eta$ is called the learning rate, $1 - \alpha$ is the momentum
    decay, $\bm{\epsilon}$ is a standard Gaussian random vector,
    $\hat\beta$ is a parameter to offset the variance of the stochastic
    gradient term and
    \begin{equation}
      \hat{U}(\bmtheta) = - \frac{|\data|}{|B|} \sum_{\uby \in B}
      \log{p}(\uby|\bmtheta) - \log{p}(\bmtheta).
    \end{equation}
  
  \item A new mini-batch $B$ is sampled for each iteration of Eq~\ref{eq:sghmc}.

  \item The variance of the stochastic gradient term is proportional to $\eta^2$
    while the variance of the injected noise is proportional to $\eta$, so in
    the limit $\eta \to 0$, only the injected noise contributes and the correct
    continuum dynamics are recovered, even if one ignores the errors from the
    stochastic gradient noise and $\hat{\beta} = 0$ is used.
\end{itemize}

\section{Sandwiching The Marginal Likelihood}

\begin{itemize}
  \item Sandwiching is proposed in \cite{sandwich}. The authors define
    stochastic upper and lower bounds, a lower bound is extremely unlikely to
    over estimate the marginal likelihood and an upper bound is extremely
    unlikely to under estimate it. Given upper and lower bounds, one can be
    fairly confident that the real marginal likelihood lies between them.

  \item The estimator $\hat{p}(\uby_n|\uby_{<n})$ in Eq~\ref{eq:p-lower} is
    unbiased and so by Jensen's inequality
    \begin{equation}
      \E[\log\hat{p}(\uby_n|\uby_{<n})] \le \log{p}(\uby_n|\uby_{<n}).
    \end{equation}
    Furthermore, since $\hat{p}$ is a positive estimator for a positive
    quantity, Markov's inequality implies
    \begin{equation}
      \mathrm{Pr}(\log\hat{p}(\uby_n|\uby_{<n}) > \log{p}(\uby_n|\uby_{<n}) + \delta) <
      e^{-\delta}.
    \end{equation}
    In this sense, it is a stochastic lower bound.

  \item Similarly the harmonic mean
    \begin{equation}
      \hat{q}(\uby_n|\uby_{<n}) = \left(
        \frac{1}{M} \sum_{i=1}^M \frac{1}{p(\uby_n|\bmtheta)}
      \right)^{-1},
    \end{equation}
    where $\bmtheta_i$ is sampled from the next posterior
    $p(\bmtheta|\uby_{\le n})$, is a stochastic upper bound by a similar
    calculation to the previous point.

  \item With high probability
    \begin{equation}
      \log\hat{p}(\uby_n|\uby_{<n}) < \log{p}(\uby_n|\uby_{<n}) < \log\hat{q}(\uby_n|\uby_{<n})
    \end{equation}

  \item The harmonic mean estimator has been criticized as the ``worst Monte
    Carlo method ever''\cite{hme-worst}. It is essentially an importance
    sampling algorithm where the sampling distribution (the posterior) is much
    more peaked than the distribution over which the expectation is taken (the
    prior). Since the posterior is largely insensitive to the prior, marginal
    likelihood approximations using harmonic mean can differ from the true value
    by an arbitrary multiplicative constant.

  \item When used for estimating predictive distribution, the flaws of the
    harmonic mean typically aren't as extreme, since for Bayesian updating, one
    posterior and the next are typically very similar. Furthermore, we only use
    harmonic mean as an optional upper bound, since it is virtually free to
    calculate using the posterior samples which are already being used for the
    estimator in Eq~\ref{eq:p-lower}.
\end{itemize}


\section{Experiments}

\begin{itemize}
  \item In our experiments we run SGHMC to sample from the
    $n$\textsuperscript{th} posterior distribution using the stochastic
    potential energy
    \begin{equation}
      \hat{U}_n(\bmtheta) = -\frac{n}{|B| + 1}\left(
        \log{p}(\uby_n|\bmtheta) + \sum_{\uby \in B} \log{p}(\uby|\bmtheta)
      \right) - \log{p(\bmtheta)}
    \end{equation}
    Where the mini-batch is drawn i.i.d. with replacement from the set of all
    previous data points $B \subset \{\uby_k|k<n\}$. This is to ensure that the
    $n$\textsuperscript{th} data point is always taken into account. With this
    potential energy SGHMC still converges to the correct posterior distribution

  \item We implement two variants of nested sampling as a baseline with which to
    compare. The first implementation samples from the prior using a similar
    Langevin simulation to SGHMC, and reflects off of the likelihood contours
    using the classical energy preserving momentum reflection
    \begin{equation}
      \Delta\ubv = - 2 (\mathbf{n}\cdot\ubv) \mathbf{n}
    \end{equation}
    where $\mathbf{n}$ is the normal vector to the likelihood contour.
    The second implementation samples from the prior under constraints using
    simple rejections sampling. The second implementation is a theoretically
    perfect nested sampler in that the samples are drawn exactly and
    independently from the prior. The rejection sampler is used to tune the
    parameters of the first nested sampler.

\end{itemize}

\subsection{Linear Regression}

\begin{itemize}
  \item The dataset consists of pairs of vectors $(\uby, \ubx)$ related by
    \begin{equation}
      \uby_n = W\ubx + \ubb + \bm{\epsilon},
    \end{equation}
    where $\bm{\epsilon}$ is zero centered Gaussian distributed with known variance
    $\sigma^2$.

  \item Parameters are $W$ and $\ubb$, with standard Gaussian priors.

  \item Marginal likelihood can be calculated analytically for this model.
\end{itemize}

\subsection{Logistic Regression}

\begin{itemize}
  \item The dataset consists of pairs $(y, \ubx)$, where $y$ is an integer
    representing the class to which the vector $\ubx$ belongs.

  \item The labels have a discrete distribution with probabilities
    \begin{equation}
      p(y|\ubx, W, \ubb) = \frac{\exp([W\ubx + \ubb]_y)}{\sum_k \exp([W\ubx + \ubb]_k)}
    \end{equation}

  \item Parameters are $W$ and $\ubb$, with standard Gaussian priors.
\end{itemize}

\subsection{Gaussian Mixture Model}

\begin{itemize}
  \item The data are modeled by a mixture of Gaussians, with mixture weights, means
    and variance treated as parameters
    \begin{equation}
      p(y|\bmtheta) = \sum_{k=1}^K \frac{\beta_k}{\sqrt{2\pi\sigma_k^2}}
      \exp\left(-\frac{(y - \mu_k)^2}{2\sigma_k^2}\right),
    \end{equation}
    \begin{equation}
      \bmtheta = (\beta_{1:K},\mu_{1:K},\sigma_{1:K}).
    \end{equation}

  \item Mixture weights $\beta_{1:K}$ are modeled by a Dirichlet prior with
    $\alpha = 1$.

  \item Means $\mu_k$ are modeled by Gaussian priors, centered around zero and
    with variance $4\sigma_k^2$.

  \item Variances $\sigma_k^2$ are modeled by inverse gamma priors with shape
    and scale parameters equal to 1.

  \item The posterior distribution for this model is multimodal. Some modes are
    due to permutation symmetries, these modes do not have to be  explored since
    each one contains the same information. There are also some local modes
    which do not necessarily capture meaningful information about the data; for
    example, fitting a single Gaussian to the whole dataset may be a poor local
    optimum of the likelihood function. If an MCMC walker finds one of these
    modes it can get trapped.
\end{itemize}

\section{Results and Discussion}
In \cite{sandwich}, the authors suggest that for a dataset of size $N$,
errors in $\log\ev$ of $0.1N$ are acceptable.
\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/lin-logz.png}
    \caption{Marginal likelihood accuracy}
  \end{subfigure}
  ~
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/lin-time.png}
    \caption{Time complexity}
  \end{subfigure}
  \caption{Linear regression model}
\end{figure}

\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/log-logz.png}
  \end{subfigure}
  ~
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/log-time.png}
  \end{subfigure}
\end{figure}

\begin{figure}[htbp]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-logz.png}
  \end{subfigure}
  ~
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-time.png}
  \end{subfigure}
\end{figure}

\section{Conclusion}

\bibliographystyle{plain}
\bibliography{ref}
\end{document}

