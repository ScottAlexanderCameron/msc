\section{Correctness of Nested Sampler and Annealed Importance Sampler}
\label{ap:ns}
\unskip
\ifdraft{}{
\subsection{Nested Sampler Implementation}

Nested sampling requires one to sample from the prior under increasing
likelihood constraints. Sampling under constraints is, in general, a difficult
problem. This sampling can be accomplished by repeatedly sampling from the prior
until the constraint is satisfied. This method of rejection sampling scales
extremely poorly with both the size of the data set and the dimension of the
parameter space, and is not feasible on any but the smallest problems.
%
Instead the constrained sampling is typically implemented by duplicating a live
particle, which is already within the constrained likelihood contour, and
applying a MCMC kernel to the particle which leaves the prior invariant.
%
While moving the particle with MCMC, care must be taken to avoid leaving the
constrained region.

Our implementation of NS is based on SGHMC, simply because the code for SGHMC
was already written. In order to sample under constraints we use a similar
strategy to Galilean Monte Carlo as described in~\cite{gmc-ns}, where the
particle reflects off of the boundary of the likelihood contour by updating the
momentum as follows:

\begin{equation*}
  \Delta\ubv = -2 (\ubv\cdot\ubn) \ubn.
\end{equation*}
%
Here $\ubn$ is a unit vector parallel to the likelihood gradient
$\nabla_{\bmtheta} p(\data|\bmtheta)$. While it is not the focus of our paper,
we note that we have not previously encountered the idea of applying SGHMC to
sampling under constraints; our implementation allows a fairly direct approach
to implementing NS in a variety of contexts, and the technique may also
potentially be of value in other constrained sampling contexts.

We found that, due to discretization error, the constrained sampler tends to
undersample slightly at the boundaries of the constrained region; however the
undersampled volume is of the order of the learning rate $\eta$ and so can be
neglected if a small enough learning rate is used.

\subsection{Test Results}

If either of our NS and AIS implementations were incorrect, we expect that their
ML estimates would disagree. 
%
If both implementations are incorrect, the probability that their errors would
cancel is close to zero since NS and AIS work in completely different ways.
%
Figure~\ref{fig:ns-ais} shows the result of 5 independent runs of NS and AIS for
data sets of varying sizes from 20 to 1000, in intervals of 20.
%
The shaded regions are the minimum and maximum log-ML estimates for each run;
these regions are the typical values produced by NS and AIS on these data sets.
%
Since the typical results of NS and AIS tend to agree, it is safe to assume that
they have been implemented correctly.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{experiments/figs/ns-ais.png}
  \caption{%
    Comparison of NS and AIS with SGHMC kernels for a
    1-dimensional Gaussian mixture model with 9 parameters (8 degrees of
    freedom). The shaded regions are the minimum and maximum of 5 independent
    runs of each algorithm.
  }
  \label{fig:ns-ais}
\end{figure}
}

\section{Model details}
\label{ap:models}
\ifdraft{}{
\comment{This section was moved here from Experiments
(Section~\ref{sec:experiments})}
\subsection{Linear Regression}

The data set consists of pairs $(\ubx, y)$ related by

\begin{equation*}
  y = \ubw^T\ubx + b + \epsilon,
\end{equation*}
%
where $\epsilon$ is zero mean Gaussian distributed with known variance
$\sigma^2$. We do not assume any distribution over $\ubx$ as it always appears
on the right hand side of the conditional. The single-observation likelihood is

\begin{equation*}
  p(y|\ubx,\ubw,b) = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(y -
      \ubw^T\ubx -
  b)^2}{2\sigma^2}\right).
\end{equation*}
%
Parameters are $\ubw$ and $b$, with standard Gaussian priors. ML can be
calculated analytically for this model. We used 5 dimensional vectors $\ubx$;
this model therefore has 6 parameters.

\subsection{Logistic Regression}

The data set consists of pairs $(\ubx, y)$, where $\ubx$ is an observation
vector which is assigned a class label $y \in \{1,\ldots,K\}$.  The labels have a
discrete distribution with probabilities given by \added{the softmax function of
an affine transform of the observations}

\begin{equation*}
  p(y|\ubx, \bmtheta) = \frac{\exp(\ubw_y^T\ubx + b_y)}{\sum_k \exp(\ubw_k^T\ubx +
  b_k)}.
\end{equation*}
%
Parameters are $\bmtheta = (\ubw_{1:K}, b_{1:K})$, with standard Gaussian priors.  Again we do not
assume any distribution over $\ubx$ as it always appears on the right hand side
of the conditional.
%
We used 10 dimensional vectors $\ubx$ with 4 classes; this model has 44
parameters.

\subsection{Gaussian Mixture Model}

The data are modeled by a mixture of $d$-dimensional multivariate Gaussian
distributions with diagonal covariance matrices. 
%
Mixture weights, means and variances are treated
as parameters. This type of model is often treated as a latent variable model,
where the mixture component assignments of each data point are the latent
variables. Here we marginalize out the latent variables to obtain the following
conditional distribution:

\begin{equation*}
  p(\uby|\bmtheta) = \sum_{k=1}^K \beta_k\prod_{j=1}^d
  \frac{1}{\sqrt{2\pi\sigma_{k,j}^2}}
  \exp\left(-\frac{(y_j - \mu_{k,j})^2}{2\sigma_{k,j}^2}\right),
\end{equation*}

\begin{equation*}
  \bmtheta = (\beta_{1:K},\mu_{1:K,1:d},\sigma_{1:K,1:d}^2).
\end{equation*}
%
Mixture weights $\beta_{1:K}$ are modeled by a Dirichlet prior with $\bm{\alpha}
= 1$; means $\mu_{k,j}$ are modeled conditionally given the variances
by Gaussian priors, centered around zero
with variance $4\sigma_{k,j}^2$; variances $\sigma_{k,j}^2$ are modeled by
inverse gamma priors with shape and scale parameters equal to 1.
%
We used 5 Gaussian components and observations were 2 dimensional; this model
has 25 parameters with 24 degrees of freedom.
%
We use this model for our sensitivity tests, since it has the most complex
structure.
}

\section{Supplementary Figures For Parameter Sensitivity Tests}
\label{ap:figs}
\ifdraft{}{
\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-M-logz.png}
    \caption{Dependence on $M$}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-ESS-logz.png}
    \caption{Dependence on $\ESS$}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-burnin-logz.png}
    \caption{Dependence on number of SGHMC steps}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-logz.png}
    \caption{Dependence on learning rate}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-burnin-logz.png}
    \caption{Dependence on learning rate with the product of learning rate and
    SGHMC steps constant}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-batch-size-logz.png}
    \caption{Dependence on mini-batch size}
  \end{subfigure}
  \caption{Log-ML sensitivity}
\end{figure}

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-M-time.png}
    \caption{Dependence on $M$}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-ESS-time.png}
    \caption{Dependence on $\ESS$}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-burnin-time.png}
    \caption{Dependence on number of SGHMC steps}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-time.png}
    \caption{Dependence on learning rate}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-burnin-time.png}
    \caption{Dependence on learning rate with the product of learning rate and
    SGHMC steps constant}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-batch-size-time.png}
    \caption{Dependence on mini-batch size}
  \end{subfigure}
  \caption{Running time sensitivity}
\end{figure}

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-M-steps.png}
    \caption{Dependence on $M$}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-ESS-steps.png}
    \caption{Dependence on $\ESS$}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-burnin-steps.png}
    \caption{Dependence on number of SGHMC steps}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-steps.png}
    \caption{Dependence on learning rate}
  \end{subfigure}
  \par\medskip
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-lr-burnin-steps.png}
    \caption{Dependence on learning rate with the product of learning rate and
    SGHMC steps constant}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{experiments/figs/gmm-sensitivity-batch-size-steps.png}
    \caption{Dependence on mini-batch size}
  \end{subfigure}
  \caption{Sensitivity of number of annealing steps}
\end{figure}
}
